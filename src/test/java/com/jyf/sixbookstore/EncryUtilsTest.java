package com.jyf.sixbookstore;

import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.common.utils.EncrypUtils;
import com.jyf.sixbookstore.common.utils.JsonUtils;
import org.junit.Test;

import java.io.UnsupportedEncodingException;

/**
 * @author Mr.贾
 * @time 2019/8/17 11:43
 * @describe:
 */
public class EncryUtilsTest {


    @Test
    public void test1(){
        Customer customer = new Customer();
        customer.setId(1l);
        customer.setRealName("大黄");
        customer.setGender(1);
        customer.setAge(2);
        customer.setPhoneNumber("11111111111");
        customer.setPassword("123456");
        customer.setDetailAddress("山东省");
        customer.setProfession("导盲犬");
        System.out.println(customer);
        String encry = null;
        String encry2 = null;
        encry = EncrypUtils.Base64Util.encry(customer.toString());
        encry2 = EncrypUtils.Base64Util.encry(JsonUtils.objectToString(customer));
        System.out.println(encry);
        //eyJhZGRyZXNzIjoi5bGx5Lic55yBIiwiYWdlIjoyLCJnZW5kZXIiOjEsImlkIjoxLCJuYW1lIjoi5aSn6buEIiwicGFzc3dvcmQiOiIxMjM0NTYiLCJwaG9uZU51bWJlciI6IjExMTExMTExMTExIiwicHJvZmVzc2lvbiI6IuWvvOebsueKrCJ9
        System.out.println(encry2);


        String decry1 = EncrypUtils.Base64Util.decry(encry);
        String decry = EncrypUtils.Base64Util.decry(encry2);
        System.out.println(decry1);
        //{"address":"山东省","age":2,"gender":1,"id":1,"realName":"大黄","password":"123456","phoneNumber":"11111111111","profession":"导盲犬"}
        System.out.println(decry);


    }

}
