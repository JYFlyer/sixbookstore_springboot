package com.jyf.sixbookstore;

import com.jyf.sixbookstore.bean.Account;
import com.jyf.sixbookstore.common.config.MyJedisPool;
import com.jyf.sixbookstore.common.utils.HttpContextUtils;
import com.jyf.sixbookstore.common.utils.JsonUtils;
import com.jyf.sixbookstore.common.utils.UUIDUtils;
import com.jyf.sixbookstore.mapper.BookMapper;
import com.jyf.sixbookstore.temp.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SixbookstoreSpringbootApplicationTests {

    @Autowired
    BookMapper bookMapper;




    @Test
    public void testSysContextUtils(){
        System.out.println(HttpContextUtils.getHostUrl());

    }

    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                //redis.clients.jedis.JedisPool@107a2a44
                //加锁后：redis.clients.jedis.JedisPool@4e704f06
                System.out.println(MyJedisPool.getJedisPool());
            }
        }).start();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                //redis.clients.jedis.JedisPool@176579a
                //加锁后：redis.clients.jedis.JedisPool@4e704f06
                System.out.println(MyJedisPool.getJedisPool());
            }
        });


        thread.start();
    }



    @Test
    public void testJedisPool(){


    }



    @Test
    public void resolveLog() throws IOException {
        /*File logFile = new File("sixBookStoreLogger/logger.log");
        FileInputStream fis=new FileInputStream(logFile);
        FileOutputStream fos = new FileOutputStream("sixBookStoreLogger/logger.log");
        BufferedInputStream bis = new BufferedInputStream(fis);*/
    }


    @Test
    public void testMyJsonUtil(){
        List<Account> accounts = new ArrayList<>();
        Account account = new Account();
        account.setId(1l);
        account.setCustomerId(1l);
        account.setWeixin(new BigDecimal("123"));
        account.setZhifubao(new BigDecimal("200"));
        Account account1 = new Account();
        account1.setId(2l);
        account1.setCustomerId(2l);
        account1.setWeixin(new BigDecimal("123"));
        account1.setZhifubao(new BigDecimal("200"));
        Account account2 = new Account();
        account2.setId(3l);
        account2.setCustomerId(3l);
        account2.setWeixin(new BigDecimal("123"));
        account2.setZhifubao(new BigDecimal("200"));
        accounts.add(account);
        accounts.add(account1);
        accounts.add(account2);
        //java对象 -》json字符串
        String string = JsonUtils.objectToString(account);
        System.out.println(string);
        //json字符串 -》java对象
        Account account10 = JsonUtils.stringToObject(string, Account.class);
        System.out.println(account1);

        String listStr = JsonUtils.objectToString(accounts);
        System.out.println(listStr);

        List<Account> list = JsonUtils.stringToObject(listStr, List.class);
        System.out.println(list);

    }


    @Test
    public void testUUID(){
        System.out.println(UUIDUtils.getUUIDByNumber(8));
    }


    @Test
    public void testColor(){
        System.out.println("\033[33;0m"+"异常为："+"\033[33;0m");
        System.out.println("\033[31;44;0m" + "我滴个颜什");
    }

    @Test
    public void testList(){
        List<Employee> employeeList = new ArrayList<>();

        Employee employee = new Employee();
        for (int i = 0; i< 5;i++){
            employee.setEmpName(UUIDUtils.getUUIDByNumber(8));
            employeeList.add(employee);
        }

        System.out.println(employeeList);

    }






}
