package com.jyf.sixbookstore;

import com.jyf.sixbookstore.common.utils.UUIDUtils;
import com.jyf.sixbookstore.mapper.TestMapper;
import com.jyf.sixbookstore.temp.Department;
import com.jyf.sixbookstore.temp.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Mr.贾
 * @time 2019/11/17 13:04
 * @describe:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BatchInsertTest {


    @Autowired
    TestMapper testMapper;


    @Test
    public void testBatchInsertEmp(){
        long startTime = System.currentTimeMillis();
        List<Employee> employeeList = new ArrayList<>(1000000);

        int cycle = 1000000;
        for (int i = 0; i< cycle;i++){
            Employee employee = new Employee();
            employee.setEmpName(UUIDUtils.getUUIDByNumber(20));
            employee.setAge(new Random().nextInt(100));
            employee.setGender(new Random().nextInt(2) + 1);
            employee.setSalary(Double.valueOf(String.format("%.2f",Math.random() * 10000)));
            employee.setDeptId(Long.parseLong(String.valueOf(new Random().nextInt(10000))));
            employeeList.add(employee);
        }

        int count = testMapper.batchInsertEmp(employeeList);
        System.out.println("插入的记录数："+count);
        System.out.println("花费时间(s):"+(System.currentTimeMillis() - startTime));



    }

    @Test
    public void testBatchInsertDept(){

        long startTime = System.currentTimeMillis();

        List<Department> departmentList = new ArrayList<>();

        for (int i = 0; i< 10000;i++){
            Department department = new Department();
            department.setDeptName(UUIDUtils.getUUIDByNumber(10));
            departmentList.add(department);
        }

        int count = testMapper.batchInsertDept(departmentList);
        System.out.println("插入的记录数："+count);
        System.out.println("花费时间(s):"+(System.currentTimeMillis() - startTime));


    }

}
