
function toShoppingCart(){
    $.ajax({
        type:"GET",
        url:"/book/beforeToShoppingCart",
        success:function(result){
            if(result.code === 400){
                layer.open({
                    type:0,
                    title:"系统提示",
                    content:"登录有就能有辆车了，现在要去登录吗?",
                    btn:["现在就去","给朕退下"],
                    btn1:function(){
                        //点击现在就去时去登陆页面
                        window.location.href="/frontSkip/toLogin";
                    }
                });
            }else{
                window.location.href="/frontSkip/toShoppingCart";
            }
        }
    });
}



var customerId;
var payMethod="未选择";
var orders = null;
$(function(){
    /*layui中加载layer模块*/
    layui.use('layer', function(){
        var layer = layui.layer;
    });

    /*选择支付方式*/
    var imgs=$(".payimg > img");
    var bs=$(".payimg > b");
    for(var i=0;i<imgs.length;i++){
        /*第一个img和第一个b需要有一个东西让他们同步起来*/
        imgs[i].index=i;
        imgs[i].onclick=function(){
            for(var j=0;j<imgs.length;j++){
                imgs[j].style.border="none";
            }
            for(var k=0;k<bs.length;k++){
                bs[k].style.display="none";
            }
            imgs[this.index].style.border="2px solid #e4393c";
            bs[this.index].style.display="block";
        }
    }

    /*编辑收货人信息*/
    $("#bianji").click(function(){
        layer.open({
            type: 2,
            title: '编辑收货人信息',
            shadeClose: false,
            shade: 0.4,
            area: ['800px', '500px'],
            content: '/frontSkip/toPayEdit'
        });

    });
    /*编辑收货人信息结束*/


    //若登录了，改变页头的customer信息
    $.ajax({
        type:"get",
        url:"/customer/getCustomer",
        success:function (result) {
            var customer = result.data;
            if (result.code === 200){
                //赋值customerId
                customerId = result.data.id;
                var $headerLeft = $("#header-left");
                $headerLeft.css("line-height","40px");
                $headerLeft.css("font-size","13px");
                $headerLeft.css("color","#666");
                $("span[class=customerName]").text(customer.realName);
                $("span[class=customerAddress]").text(customer.province+customer.city+customer.region+customer.detailAddress);
                $("span[class=customerPhone]").text(customer.phoneNumber);
                $(".jine > .p2 > span:eq(0)").text(customer.province+customer.city+customer.region+customer.detailAddress);
                $(".jine > .p2 > span:eq(1)").text(customer.realName);
                $(".jine > .p2 > span:eq(2)").text(customer.phoneNumber);
                if (customer.realName == null || customer.realName == ""){
                    $("#header-left").html("<span style='font:400 14px 微软雅黑;color: red;'>尊敬的客户："+customer.phoneNumber+",请尽快完善个人信息!</span> &nbsp;"
                        +"<a href='/customer/logout'>[退出]</a>"
                    );
                }else if (customer.gender === 1){
                    $("#header-left").html("欢迎您的到来：<span style='font:600 14px 微软雅黑; color:red;'>"+customer.realName+"</span> &nbsp;先生"
                        +"<a href='/customer/logout'>[退出]</a>"
                    );
                }else{
                    $("#header-left").html("欢迎您的到来：<span style='font:600 14px 微软雅黑; color:red;'>"+customer.realName+"</span> &nbsp;女士"
                        +"<a href='/customer/logout'>[退出]</a>"
                    );
                }
            }
        }
    });

    //获取订单和总价格
    $.ajax({
        type:"get",
        url:"/order/getOrder",
        success:function (result) {
            if (result.code === 200){
                //赋值orders
                orders = result.data.orders;
                var totalMoney = result.data.totalMoney;
                //转换成数值类型
                totalMoney = parseFloat(totalMoney);
                for(var i=0;i<orders.length;i++){
                    $("<li class='li1'></li>")
                        .append("<img src='"+orders[i].bookImgUrl+"'/>")
                        .append("<span title='"+orders[i].bookName+"'>"+orders[i].bookName+"</span>")
                        .append("<div class='price'>￥"+orders[i].bookPrice.toFixed(2)+"</div>")
                        .append("<div class='shuliang' title='购买的数量'>"+orders[i].bookNumber+"</div>")
                        .append("<div class='youhuo'>有货</div>")
                        .appendTo("#orderInfo");
                }
                $("#zongjiage").text("￥"+totalMoney.toFixed(2));

                //多于2个时出现滚动条
                if($("#orderInfo > .li1").length>2){
                    $("#orderInfo").css("height","160px");
                }else{
                    $("#orderInfo").css("height","auto");
                }
            }else{
                layer.msg(result.message,{icon:5,anim:6});
                setTimeout(function () {
                    //只要没有订单，就去首页
                    window.location.href="/frontSkip/toHome";
                },2000);
            }

        }
    });

});

//检查订单
function checkAccount() {
    //检查信息是否完善
    var cName =$(".wrapp > .customerName").text();
    var cAddress =$("#innerp > .customerAddress").text();
    var cPhone = $("#innerp > .customerPhone").text();
    /*
    * 在js中能作为if判断条件的有4种
    * 1、布尔:true/false
    * 2、null和undefined
    * 3、空串""
    * 4、0和NaN
    *
    * null、undefined、""、0就相当于false,可以直接取反来简化代码和提高性能
    * */
    if (!cName || !cAddress || !cPhone){
        layer.alert("请先完善个人信息!",{title:"系统提示"});
        return false;
    }
    var totalMoney=$("#zongjiage").text();
    if (!totalMoney){
        layer.msg("总金额不能为空!",{icon:5,anim:6});
        return false;
    }
    totalMoney=totalMoney.substring(1,totalMoney.length);
    //检查是否选择支付方式

    $.each($(".payimg > img"),function(index,object){
        if($(this).css("border")=="2px solid rgb(228, 57, 60)"){
            payMethod=index; //index为0或1
        }
    });
    if(payMethod=="未选择"){
        layer.msg("请先选择支付方式!",{icon:5,anim:6});
        return false;
    }
    //检查该支付方式账户钱是否足够
    $.ajax({
        type:"post",
        url:"/account/checkMoney",
        data:{customerId:customerId,totalMoney:totalMoney,payMethod:payMethod},
        success:function(result){
            if(result.code === 400){
                layer.msg(result.message,{icon:5,anim:6});
            }else{
                submitOrder();
            }
        }
    });
}

//提交订单
function submitOrder() {
    var totalMoney=$("#zongjiage").text();
    var strBookNos = "";
    for (var i=0;i<orders.length;i++){
        strBookNos +=orders[i].bookNo+",";
    }
    //去掉最后一个逗号
    strBookNos = strBookNos.substring(0,strBookNos.length-1);
    //去掉开头的符号
    totalMoney = totalMoney.replace("￥","");

    $.ajax({
        type:"post",
        url:"/order/submitOrder",
        data:{customerId:customerId,strBookNos:strBookNos,totalMoney:totalMoney,payMethod:payMethod},
        success:function(result){
            //===代表类型和内容都相等
            if(result.code === 400){
                layer.alert(result.message, {title:"系统提示",shade:false});
            }else{
                window.location.href="/frontSkip/toPaySuccess";
            }
        }
    });
}


function editCustomerInfo() {
    $.ajax({
        type:"get",
        url:"/customer/getCustomer",
        data:{pay:"pay"},
        success:function (result) {
            var customer = result.data;
            $("input[name=realName]").val(customer.realName);
            //设置单选按钮被选中的值
            $("input[name=gender][value="+customer.gender+"]").prop("checked","checked");
            $("input[name=age]").val(customer.age);
            $("input[name=address]").val(customer.province+customer.city+customer.region+customer.detailAddress);
            $("input[name=profession]").val(customer.profession);
        }
    });


}



//完善customer信息
function perfectInfo() {
    var realName = $("input[name=realName]").val();
    var gender = $("input[name=gender]:checked").val();
    var age = $("input[name=age]").val();
    var address = $("input[name=address]").val();
    var profession = $("input[name=profession]").val();

    //非 "" 校验
    var flag = true;
    console.log(realName,gender,age,address,profession);
    $.each($("#perfectForm input"),function () {
        if (!$(this).val()){ // ==> if($(this).val == null || $(this).val == "")
            flag = false;
            return;
        }
    });
    if (!flag){
        layer.msg('必填项不能为空!', {icon: 5});
        return false;
    }

    var numberRegExp = /^(?:[1-9][0-9]?|1[01][0-9]|120)$/;
    if (!numberRegExp.test(age)){
        layer.msg('年龄格式不对!', {icon: 5});
       return false;
    }
    if (age <0 || age > 200){
        layer.msg('年龄格式不对!', {icon: 5});
        return false;
    }

    $.ajax({
        async:false,
        type:"post",
        url:"/customer/perfectCustomerInfo",
        data:{
            realName:realName,
            gender:gender,
            age:age,
            address:address,
            profession:profession
        },
        success:function (result) {
            if (result.code === 200){
                //设置该layer持续时间和执行完毕后执行的函数
               layer.msg("保存成功!",{time:1000,icon:1},function () {
                   var index = parent.layer.getFrameIndex(window.name);
                   parent.layer.close(index);//关闭当前页
                   window.parent.location.reload();//刷新父页面
               });
            }else{
                if (result == "redirect"){
                   window.location.href="/frontSkip/toLogin";
                   return false;
                }
                layer.msg("输入的数据有误!",{icon:5,anim:6});
            }
        }

    });

}

//自定义让当前js程序睡眠的方法，单位毫秒
function sleep(n) {
    var start = new Date().getTime();
    //  console.log('休眠前：' + start);
    while (true) {
        if (new Date().getTime() - start > n) {
            break;
        }
    }
}