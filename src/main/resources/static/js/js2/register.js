window.onload=function(){
		var box=document.querySelector("#content_wrapper .login-box");
		box.style.marginLeft="402px";

		//更多资料显示与隐藏
		var gengduo=document.querySelector(".gengduoziliao");
		var selectShow=document.getElementById("selectShow");
		gengduo.onclick=function(){
			if(selectShow.style.display=="none"){
				selectShow.style.display="block";
				gengduo.innerHTML="收起资料";
			}
			else if(selectShow.style.display=="block"){
				selectShow.style.display="none";
				gengduo.innerHTML="更多资料";
			}
		};
				
//js创建验证码
var code="";
function createCode(){ //创建验证码函数
	  code = "";
	  var codeLength =5;//验证码的长度
	  var selectChar = [0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F','G','H'
		 ,'I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
		 //所有候选组成验证码的字符，当然也可以用中文的
	  for(var i=0;i<codeLength;i++){
		  var charIndex =Math.floor(Math.random()*36);
		  code +=selectChar[charIndex];
	   }
       $("#yangzhengmatext + span").text(code);     // 显示
	   }

		/*调用函数*/
		createCode();
		/*切换验证码*/
		$("#yangzhengmatext + span").click(function(){
			createCode();
		});
	};

//customer注册
function customerRegister(){
	var phoneNumber=$("#phoneNumber").val();
	var password1=$("#password1").val();
	var password2=$("#password2").val();
	var idNumber = $("#idNumber").val();
	var realName=$("#realName").val();
	var nickName=$("#nickName").val();
	var gender=$(":radio:checked").val();
	var address=$("#address").val();
	var profession=$("[name=profession] > option:selected").val();
	var yanzhengma=$("#yangzhengmatext").val();
	//验证手机号
	if(phoneNumber == ""){
		$("#errormessage").css("display","block");
		$("#errormessage em").text("请先输入正确的手机号！");
		return false;
	}else{
		var phoneNumberRegExp=/0?(13|14|15|18|17)[0-9]{9}/;
		if(!phoneNumberRegExp.test(phoneNumber)){
			$("#errormessage").css("display","block");
			$("#errormessage em").text("请先输入正确的手机号格式！");
			return false;
		}
	}
	//验证密码
	if(password1==""){
		$("#errormessage").css("display","block");
		$("#errormessage em").text("请先输入正确的密码！");
		return false;
	}else{
		var passwordRegExp=/[0-9a-zA-Z.]{6,}/;
		if(!passwordRegExp.test(password1)){
			$("#errormessage").css("display","block");
			$("#errormessage em").text("密码要求为6位以上的字数数字和任意字符！");
			return false;
		}
	}
	//确认密码
	if(password2==""){
		$("#errormessage").css("display","block");
		$("#errormessage em").text("请确认密码！");
		return false;
	}else{
		if(password1!=password2){
			$("#errormessage").css("display","block");
			$("#errormessage em").text("两次密码输入不一样！");
			return false;
		}
	}
	//身份证号
	if(idNumber == ""){
		$("#errormessage").css("display","block");
		$("#errormessage em").text("请输入您的有效身份证号！");
		return false;
	}else{
		var idNumberRegExp=/^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
		if(!idNumberRegExp.test(idNumber)){
			$("#errormessage").css("display","block");
			$("#errormessage em").text("确定身份证号格式正确吗?");
			return false;
		}
	}
	/*--------一下都是非必须填写的-----------*/
	//验证姓名
    if (realName != ""){
        var nameRegExp=/^([\u4e00-\u9fa5]){2,7}$/;
        if(!nameRegExp.test(realName)){
            $("#errormessage").css("display","block");
            $("#errormessage em").text("你的名字中存在不合法的字符！");
            return false;
        }
    }
    //验证姓名
    if (nickName != ""){
        var nickNameRegExp=/^([\u4e00-\u9fa5])[-_a-zA-Z0-9]{1,18}$/;
        if(!nickNameRegExp.test(nickName)){
            $("#errormessage").css("display","block");
            $("#errormessage em").text("昵称中存在不合法的字符！");
            return false;
        }
    }



	//验证性别
    if (realName != ""){
        if(gender==""){
            $("#errormessage").css("display","block");
            $("#errormessage em").text("请先选择性别！");
            return false;
        }
    }

	//验证地址
    if (realName != "" || gender != ""){
        if(address==""){
            $("#errormessage").css("display","block");
            $("#errormessage em").text("请先输入您的真实地址！");
            return false;
        }else{
            var addressRegExp=/^(?=.*?[\u4E00-\u9FA5])[\dA-Za-z\u4E00-\u9FA5]{6,}$/;
            if(!addressRegExp.test(address)){
                $("#errormessage").css("display","block");
                $("#errormessage em").text("确定您的地址对吗？");
                return false;
            }
        }
    }

	//验证职业
    if (realName != "" || gender != "" || address != ""){
        if(profession == ""){
            $("#errormessage").css("display","block");
            $("#errormessage em").text("请先选择您的职业");
            return false;
        }
    }

	//验证验证码是否正确
	if(yanzhengma==""){
		$("#errormessage").css("display","block");
		$("#errormessage em").text("请先输入正确的验证码！");
		return false;
	}else{
		var yanzhengmatxt=$("#yangzhengmatext + span").text();
		if(yanzhengma!=yanzhengmatxt){
			$("#errormessage").css("display","block");
			$("#errormessage em").text("验证码区分大小写哦！");
			return false;
		}
	}
	//注册customer
	$.ajax({
		type:"POST",
		url:"/customer/zhuCeCustomer",
		data:$("#zhuceform").serialize(),
		success:function(result){
			if(result.code === 400){
				//TODO 颤抖
				$("#errormessage").css("display","block");
				$("#errormessage em").text(result.message);
			}else{
				$("#errormessage").css("display","none");
                $("#errormessage em").text(result.message);
			}
		}
	});

}