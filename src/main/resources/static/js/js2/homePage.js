
function toShoppingCart(){
    $.ajax({
        type:"GET",
        url:"/book/beforeToShoppingCart",
        success:function(result){
            if(result.code == 400){
                layer.open({
                    type:0,
                    title:"系统提示",
                    content:"登录有就能有辆车了，现在要去登录吗?",
                    btn:["现在就去","给朕退下"],
                    btn1:function(){
                        //点击现在就去时去登陆页面
                        window.location.href="/frontSkip/toLogin";
                    }
                });
            }else{
                window.location.href="/frontSkip/toShoppingCart";
            }
        }
    });
}


$(function() {
    //若登录了，改变页头的customer信息
    $.ajax({
        type:"get",
        url:"/customer/getCustomer",
        success:function (result) {
            var customer = result.data;
            if (result.code === 200){
                var $headerLeft = $("#header-left");
                $headerLeft.css("line-height","40px");
                $headerLeft.css("font-size","13px");
                $headerLeft.css("color","#666");
                if (customer.realName == null || customer.realName == ""){
                    $("#header-left").html("<span style='font:400 14px 微软雅黑;color: red;'>尊敬的客户："+customer.phoneNumber+",请尽快完善个人信息!</span> &nbsp;"
                        +"<a href='/customer/logout'>[退出]</a>"
                    );
                }else if (customer.gender === 1){
                    $("#header-left").html("欢迎您的到来：<span style='font:600 14px 微软雅黑; color:red;'>"+customer.realName+"</span> &nbsp;先生"
                        +"<a href='/customer/logout'>[退出]</a>"
                    );
                }else{
                    $("#header-left").html("欢迎您的到来：<span style='font:600 14px 微软雅黑; color:red;'>"+customer.realName+"</span> &nbsp;女士"
                        +"<a href='/customer/logout'>[退出]</a>"
                    );
                }
            }
        }
    });

    //获取轮播图
    $.ajax({
        type:"get",
        url:"/carousel/getListCarousel",
        success:function (result) {
            if (result.code == 200){
                console.log(result.data);
               $.each($("#lunbo img"),function (index,object) {
                   if (result.data[index]){
                       $(this).parent("a").attr("href",result.data[index].contentUrl);
                       $(this).attr("src",result.data[index].imgUrl);
                       $(this).attr("title",result.data[index].title);
                   }
               });
            }
        }
    });





    //点击回顶部，隐藏右侧导航栏
    $("#HuiDingBu").click(function () {
		$("#right_nav").css("display","none");
	});

    //layui加载轮播图模块
    layui.use('carousel', function() {
        var carousel = layui.carousel;
        //建造实例
        carousel.render({
            elem: '#test1',
            height: '410px',
            width: '100%', //设置容器宽度
            arrow: 'hover', //始终显示箭头
            anim: 'fade', //切换动画方式
            indicator:'none'//指示器不显示
        });
    });





		
	
	//获取book的数据构建tab-biancheng页面
	$.ajax({
	    type:"GET",
	    url:"/book/getTabBook",
	    data:{"bookKind":1},
		success:function(result){
			if (result.code === 400){
                layer.msg(result.message, {icon: 2});//x号
			}else{
				gouJianTabBianCheng(result.data);
			}
		}
	});
	
	//获取book的数据构建新书排行榜
	$.ajax({
	   url:"/book/getNewBookList",
	   data:{"bookKind":4},
	   success:function(result){
		   if (result.code === 400){
               layer.msg(result.message, {icon: 2});//x号
		   }else{
               buildNewBookList(result.data);
		   }
	   }
	});
	//构建新书列表
	function buildNewBookList(result){
		for(var i=0;i<10;i++){
			$("<li></li>").append("<p><span>0"+(i+1)+"</span><img src="+result[i].bookImgUrl+" "+
				"title='"+result[i].bookName+"'/><a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' title='"+result[i].bookName+"'>"+result[i].bookName+"<br/><span title='' >￥"+result[i].bookPrice.toFixed(2)+"</span></a></p>")
				.appendTo("#newBookList");
			if(i<3){
				$("#newBookList >li > p > span").attr("class","hongse");
			}
		}
		$("#newBookList >li:nth-child(10) > p > span").text("10");
		$("#newBookList").append("<li><a href='javascript:;' class='list_bottom'>查看完整榜单>>></a></li>");


	}

	/*从这里开始滚动滑轮获取数据*/
	
	//右侧导航栏
	$("#right_nav").hide();
    //获取好处推荐四个字的偏移量
    var goodBook = $("#goodBook").offset().top;
	//获取到编程语言四个字的偏移量
	var programming = $("#bc").offset().top;
	//获取操作系统四个字的偏移量
	var os = $("#cz").offset().top;
	//获取数据库四个字的偏移量
	var dataBase = $("#sj").offset().top;
	//获取办公软件四个字的偏移量
	var office = $("#bg").offset().top;
	//获取图形处理/多媒体四个字的偏移量
	var tuxing = $("#tx").offset().top;
	//当前窗口可视区域的宽度
	var windowHeight = $(window).outerHeight();

	var goodBookFlag = true;
	var programmingFlag = true;
	var osFlag = true;
	var dataBaseFlag = true;
	var officeFlag = true;
	var tuxingFlag = true;
	//滚动到某个位置时加载数据，且只加载一次
	$(window).scroll(function() {
		var this_scrollTop = $(this).scrollTop();
		//this_scrollTop +=windowHeight/2;
		if (this_scrollTop > goodBook){
			if (goodBookFlag){
				goodBookFlag = false;
				//获取book的数据构建左右动
				$.ajax({
					async:false,
					url:"/book/getLeftAndRightBook",
					success:function(result){
						buildLeftAndRightBook(result.data);

					}
				});
				console.log("getLeftAndRightBook");
			}
		}
		if (this_scrollTop > programming) {
			if (programmingFlag){
				programmingFlag = false;
				//showBianchengBook
				$.ajax({
					url:"/book/getShowBook",
					data:{"bookKind":1},
					success:function(result){
						if (result.code == 400){
							alert(result.message);
						}else{
							goujianShowBianchengBook(result.data);
						}
					}
				});
				console.log("getShowBook1");
			}
			var widowScrollTop = $(window).scrollTop();
			widowScrollTop-=300;
			if(widowScrollTop > programming) {
				$("#right_nav").show(300);
			}else{
				$("#right_nav").hide(300);
			}
		}
		if (this_scrollTop > os){
			if (osFlag){
				osFlag = false;
				//showSystemBook
				$.ajax({
					url:"/book/getShowBook",
					data:{"bookKind":3},
					success:function(result){
						if (result.code == 400){
                            layer.msg(result.message, {icon: 2});//x号
						}else{
							goujianShowSystemBook(result.data);
						}
					}
				});
				console.log("getShowBook3");
			}
		}
		if (this_scrollTop > dataBase){
			if (dataBaseFlag){
				dataBaseFlag = false;
				//showDatabaseBook
				$.ajax({
					url:"/book/getShowBook",
					data:{"bookKind":2},
					success:function(result){
						if (result.code === 400){
                            layer.msg(result.message, {icon: 2});//x号
						}else{
							goujianShowDatabaseBook(result.data);
						}
					}
				});
				console.log("getShowBook2");
			}
		}
		if (this_scrollTop > office){
			if (officeFlag){
				officeFlag = false;
				//showOfficeBook
				$.ajax({
					url:"/book/getShowBook",
					data:{"bookKind":4},
					success:function(result){
						if (result.code === 400){
                            layer.msg(result.message, {icon: 2});//x号
						}else{
							goujianShowOfficeBook(result.data);
						}
					}
				});
				console.log("getShowBook4");
			}
		}
		if (this_scrollTop > tuxing){
			if (tuxingFlag){
				tuxingFlag = false;
				//showPsBook
				$.ajax({
					url:"/book/getShowBook",
					data:{"bookKind":5},
					success:function(result){
						if (result.code === 400){
                            layer.msg(result.message, {icon: 2});//x号
						}else{
							goujianShowPsBook(result.data);
						}
					}
				});
				console.log("getShowBook5");
			}
		}

	});

	
	/*tab栏*/
	var nodeA=document.querySelectorAll(".talleft_header_a");
	var divs=document.querySelectorAll("#talleft_content div");
	for(var i=0;i<nodeA.length;i++){
		nodeA[i].index=i;//用于关联下面的div
		nodeA[i].onclick=function(){
			for(var j=0;j<nodeA.length;j++){
				nodeA[j].style.color="black";
			}
			this.style.color="#ca1524";
			for(var k=0;k<divs.length;k++){
				divs[k].style.display="none";
			}
			divs[this.index].style.display="block";
			
			if(this.index==0){
			   $.ajax({
	               type:"GET",
	               url:"/book/getTabBook",
	               data:{"bookKind":1},
		           success:function(result){
					   if (result.code == 400){
                           layer.msg(result.message, {icon: 2});//x号
					   }else{
						   gouJianTabBianCheng(result.data);
					   }
		          }
	           });
			}else if(this.index==1){
			       $.ajax({
			          url:"/book/getTabBook",
			          data:{"bookKind":3},
			          success:function(result){
						  if (result.code == 400){
						      layer.msg(result.message, {icon: 2});//x号
						  }else{
							  gouJianTabSystem(result.data);
						  }
			          }
			       });
			}else if(this.index==2){
			        $.ajax({
			          url:"/book/getTabBook",
			          data:{"bookKind":2},
			          success:function(result){
						  if (result.code == 400){
                              layer.msg(result.message, {icon: 2});//x号
						  }else{
							  gouJianTabDatabase(result.data);
						  }
			          }
			       });
			}else if(this.index==3){
			       $.ajax({
			          url:"/book/getTabBook",
			          data:{"bookKind":4},
			          success:function(result){
						  if (result.code == 400){
                              layer.msg(result.message, {icon: 2});//x号
						  }else{
							  gouJianTabOffice(result.data);
						  }
			          }
			       });
			}else if(this.index==4){
			      $.ajax({
			          url:"/book/getTabBook",
			          data:{"bookKind":5},
			          success:function(result){
						  if (result.code == 400){
                              layer.msg(result.message, {icon: 2});//x号
						  }else{
							  gouJianTabPs(result.data);
						  }
			          }
			       });
			}
		}
	}
    //tabbiancheng的构建
    function gouJianTabBianCheng(result){
        $("div[class='biancheng'] > ul[class='firstUl']").empty();
        for(var i=0;i<5;i++){
            $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'><img src="+result[i].bookImgUrl+" title='"+result[i].bookName+"'/></a>" +
                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>")
                .appendTo("div[class='biancheng'] > ul[class='firstUl']");
        }

        $("div[class='biancheng'] > ul[class='secondeUl']").empty();
        for(var i=5;i<10;i++){
            $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'><img src="+result[i].bookImgUrl+" title='"+result[i].bookName+"'/></a>" +
                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>")
                .appendTo("div[class='biancheng'] > ul[class='secondeUl']");
        }
        $("#talleft_content .biancheng img").addClass("anim-opacity");
    }

    function gouJianTabSystem(result){
        $("div[class='system'] > ul[class='firstUl']").empty();
        for(var i=0;i<5;i++){
            $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'><img src="+result[i].bookImgUrl+" title='"+result[i].bookName+"'/></a>" +
                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>")
                .appendTo("div[class='system'] > ul[class='firstUl']");
        }

        $("div[class='system'] > ul[class='secondeUl']").empty();
        for(var i=5;i<10;i++){
            $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'><img src="+result[i].bookImgUrl+" title='"+result[i].bookName+"'/></a>" +
                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>")
                .appendTo("div[class='system'] > ul[class='secondeUl']");
        }
		$("#talleft_content .system img").addClass("anim-opacity");
    }

    function gouJianTabDatabase(result){
        $("div[class='database'] > ul[class='firstUl']").empty();
        for(var i=0;i<5;i++){
            $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'><img src="+result[i].bookImgUrl+" title='"+result[i].bookName+"'/></a>" +
                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>")
                .appendTo("div[class='database'] > ul[class='firstUl']");
        }

        $("div[class='database'] > ul[class='secondeUl']").empty();
        for(var i=5;i<10;i++){
            $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'><img src="+result[i].bookImgUrl+" title='"+result[i].bookName+"'/></a>" +
                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>")
                .appendTo("div[class='database'] > ul[class='secondeUl']");
        }
		$("#talleft_content .database img").addClass("anim-opacity");
    }

    function gouJianTabOffice(result){
        $("div[class='office'] > ul[class='firstUl']").empty();
        for(var i=0;i<5;i++){
            $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'><img src="+result[i].bookImgUrl+" title='"+result[i].bookName+"'/></a>" +
                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>")
                .appendTo("div[class='office'] > ul[class='firstUl']");
        }

        $("div[class='office'] > ul[class='secondeUl']").empty();
        for(var i=5;i<10;i++){
            $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'><img src="+result[i].bookImgUrl+" title='"+result[i].bookName+"'/></a>" +
                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>")
                .appendTo("div[class='office'] > ul[class='secondeUl']");
        }
		$("#talleft_content .office img").addClass("anim-opacity");
    }

    function gouJianTabPs(result){
        $("div[class='ps'] > ul[class='firstUl']").empty();
        for(var i=0;i<5;i++){
            $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'><img src="+result[i].bookImgUrl+" title='"+result[i].bookName+"'/></a>" +
                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>")
                .appendTo("div[class='ps'] > ul[class='firstUl']");
        }

        $("div[class='ps'] > ul[class='secondeUl']").empty();
        for(var i=5;i<10;i++){
            $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'><img src="+result[i].bookImgUrl+" title='"+result[i].bookName+"'/></a>" +
                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>")
                .appendTo("div[class='ps'] > ul[class='secondeUl']");
        }
		$("#talleft_content .ps img").addClass("anim-opacity");
    }


    //构建左右动
    function buildLeftAndRightBook(result){
        for(var i=0;i<result.length;i++){
            $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'><img src='"+result[i].bookImgUrl+"'/></a>")
                .appendTo("#LeftAndRight > ul");
        }
        $("#LeftAndRight img").addClass("anim-opacity");

        /*左右动*/
        var bloackAs=document.querySelectorAll("#LeftAndRight > a");
        var blockUL=document.querySelector("#LeftAndRight > ul");
        var lis=document.querySelectorAll("#LeftAndRight > ul > li");
        blockUL.style.width= (205) * lis.length+"px";
        var pianyiliang=0;
        for(var i=0;i<bloackAs.length;i++){
            bloackAs[i].index=i;
            bloackAs[i].onclick=function(){
                if(this.index==0){
                    pianyiliang-=205;
                    if(pianyiliang>=-1025){
                        blockUL.style.left=pianyiliang+"px";
                    }else{
                        pianyiliang=-1025;
                    }
                }
                if(this.index==1){
                    pianyiliang+=205;
                    if(pianyiliang<=0){
                        blockUL.style.left=pianyiliang+"px";
                    }else{
                        pianyiliang=0;
                    }
                }
            }
        }
    }


	//构建纯展示的book

function  goujianShowBianchengBook(result){
        for(var i=0;i<6;i++){
          $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'>" +
          		                " <img src="+result[i].bookImgUrl+" />" +
          		                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
          		                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>" +
          		                "</a>").appendTo("#showBook .biancheng ul[class='firstUl']");
        }
        for(var i=6;i<12;i++){
          $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'>" +
          		                " <img src="+result[i].bookImgUrl+" />" +
          		                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
          		                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>" +
          		                "</a>").appendTo("#showBook .biancheng ul[class='secondeUl']");
        }
        $("#showBook .biancheng img").addClass("anim-opacity");
}

function  goujianShowSystemBook(result){
        for(var i=0;i<6;i++){
          $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'>" +
          		                " <img src="+result[i].bookImgUrl+" />" +
          		                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
          		                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>" +
          		                "</a>").appendTo("#showBook .system ul[class='firstUl']");
        }
        for(var i=6;i<12;i++){
          $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'>" +
          		                " <img src="+result[i].bookImgUrl+" />" +
          		                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
          		                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>" +
          		                "</a>").appendTo("#showBook .system ul[class='secondeUl']");
        }
	$("#showBook .system img").addClass("anim-opacity");
}

function  goujianShowDatabaseBook(result){
        for(var i=0;i<6;i++){
          $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'>" +
          		                " <img src="+result[i].bookImgUrl+" />" +
          		                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
          		                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>" +
          		                "</a>").appendTo("#showBook .database ul[class='firstUl']");
        }
        for(var i=6;i<12;i++){
          $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'>" +
          		                " <img src="+result[i].bookImgUrl+" />" +
          		                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
          		                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>" +
          		                "</a>").appendTo("#showBook .database ul[class='secondeUl']");
        }
        $("#showBook .database img").addClass("anim-opacity");
}

function  goujianShowOfficeBook(result){
        for(var i=0;i<6;i++){
          $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'>" +
          		                " <img src="+result[i].bookImgUrl+" />" +
          		                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
          		                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>" +
          		                "</a>").appendTo("#showBook .office ul[class='firstUl']");
        }
        for(var i=6;i<12;i++){
          $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'>" +
          		                " <img src="+result[i].bookImgUrl+" />" +
          		                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
          		                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>" +
          		                "</a>").appendTo("#showBook .office ul[class='secondeUl']");
        }
	$("#showBook .office img").addClass("anim-opacity");
}

function  goujianShowPsBook(result){
        for(var i=0;i<6;i++){
          $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'>" +
          		                " <img src="+result[i].bookImgUrl+" />" +
          		                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
          		                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>" +
          		                "</a>").appendTo("#showBook .ps ul[class='firstUl']");
        }
        for(var i=6;i<12;i++){
          $("<li></li>").append("<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"'>" +
          		                " <img src="+result[i].bookImgUrl+" />" +
          		                "<a href='/book/showBookDetails?bookNo="+result[i].bookNo+"' class='jianjie' title='"+result[i].bookName+"'>"+result[i].bookName+"</a>" +
          		                "<h4>￥"+result[i].bookPrice.toFixed(2)+"</h4>" +
          		                "</a>").appendTo("#showBook .ps ul[class='secondeUl']");
        }
	$("#showBook .ps img").addClass("anim-opacity");
}
	
});

