package com.jyf.sixbookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jyf.sixbookstore.bean.ChatInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Mr.贾
 * @time 2020/1/11 22:41
 * @describe:
 */
@Repository
public interface WebChatMapper extends BaseMapper<ChatInfo> {


    List<ChatInfo> selectListByFromAndToId(@Param("fromCustomerId") Long fromCustomerId,@Param("toCustomerId") Long toCustomerId);
}
