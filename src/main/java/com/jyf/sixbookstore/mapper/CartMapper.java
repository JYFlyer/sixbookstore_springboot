package com.jyf.sixbookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jyf.sixbookstore.bean.Cart;
import com.jyf.sixbookstore.bean.Order;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface CartMapper extends BaseMapper<Cart> {

    //加入购物车时如果已经添加该书了，就发送修改请求
    void updateCartBookNumber(@Param("bookNumber") Integer bookNumber, @Param("customerId") Long customerId, @Param("bookNo") String bookNo);

    //查询当前用户购物车中书的所有种类数
    List<Integer> selectBookKindNumberByCustomerId(Long customerId);
    //查询当前用户购物车书的总数量
    Integer selectCartBookNumberByCustomerId(Long customerId);
	
	List<Cart> getCartBookByBookNo(@Param("customerId") Long customerId, @Param("bookNos") List<String> bookNos);

	//清空购物车
	void cleanCart(Long customerId);

	//删除选中的
	void deleteBookByChecked(@Param("customerId") Long customerId, @Param("list") List<String> list);

	//批量添加订单
	//int batchInsertOrder(@Param("orders") List<Order> orders);

    List<Cart> selectCartList(Long customerId);

    void updateBookNumberByCustomerIdAndBookNo(@Param("customerId") Long customerId,@Param("bookNosAndNumbers") List<Map<String,Object>> bookNosAndNumbers);
}