package com.jyf.sixbookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jyf.sixbookstore.bean.Account;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface AccountMapper extends BaseMapper<Account> {

	void updateWeiXinByCustomerId(@Param("customerId") Long customerId, @Param("totalMoney") BigDecimal totalMoney);
	void updateZhiFuBaoByCustomerId(@Param("customerId") Long customerId, @Param("totalMoney") BigDecimal totalMoney);


	BigDecimal getWeiXinByCustomerId(Long customerId);
	BigDecimal getZhiFuBaoByCustomerId(Long customerId);


}