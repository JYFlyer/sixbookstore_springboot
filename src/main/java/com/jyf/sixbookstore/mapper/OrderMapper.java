package com.jyf.sixbookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jyf.sixbookstore.bean.Order;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Mr.贾
 * @time 2019/6/22 13:43
 * @describe:
 */
@Repository
public interface OrderMapper extends BaseMapper<Order> {

      //得到总金额
      BigDecimal getTotalMoneyByOrderNo(String orderNo);

      List<Integer> getBookNumberByOrderNoAndBookNos(@Param("orderNo") String orderNo,@Param("bookNos") List<String> bookNos);

      //void updateBookStockAndStatusByOrderNoAndBookNos(@Param("orderNo")String orderNo,@Param("bookNosAndNumbers") List<Map<String, Object>> bookNosAndNumbers);

    void batchInsertOrders(@Param("orders") List<Order> orders);

    List<Order> selectOrderPage(Page<Order> page,@Param("paramMap") Map<String, Object> paramMap);
}
