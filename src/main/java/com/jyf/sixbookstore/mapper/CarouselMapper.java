package com.jyf.sixbookstore.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jyf.sixbookstore.bean.Carousel;
import org.springframework.stereotype.Repository;

/**
 * 
 * <pre>
 * 轮播图表
 * </pre>
 * <small> 2019-10-27 11:28:41 | jyf</small>
 */
@Repository
public interface CarouselMapper extends BaseMapper<Carousel> {

}
