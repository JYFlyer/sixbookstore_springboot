package com.jyf.sixbookstore.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.bean.GuessLike;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerMapper extends BaseMapper<Customer> {

	String findPasswordByPhoneNumber(String phoneNumber);

    void updatePasswordByPhoneNumber(@Param("phoneNumber") String phoneNumber,@Param("password") String password);

    void updateHeadImg(@Param("customerId") Long customerId,@Param("dbFilePath") String dbFilePath);
}