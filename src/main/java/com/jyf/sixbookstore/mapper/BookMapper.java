package com.jyf.sixbookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jyf.sixbookstore.bean.Book;
import com.jyf.sixbookstore.bean.Cart;
import com.jyf.sixbookstore.bean.GuessLike;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface BookMapper extends BaseMapper<Book> {
    
    List<Book> getNewBookList(Integer bookKind);

    List<Book> getLeftAndRightBook(@Param("lrBookIds") List<Long> lrBookIds);


    //加入购物车时根据书的编号判断是否存在该书，获取到书编号的集合
    List<String> getCartBookNosByCustomerId(Long customerId);

    //根据书的编号修改书的库存
    void updateBookStockByBookNos(@Param("bookNosAndNumbers") List<Map<String,Object>> bookNosAndNumbers);

    List<Long> getAllBookIds();


    List<Integer> getBookStocksByBookNos(@Param("bookNos") List<String> bookNos);

    //idea插件mybatisx的实验方法，Alt+B 即可跳转到对应的sql
    //List<Book> getBooks();
}