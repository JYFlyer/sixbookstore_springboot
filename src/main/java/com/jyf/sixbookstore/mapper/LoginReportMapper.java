package com.jyf.sixbookstore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jyf.sixbookstore.bean.LoginReport;
import org.springframework.stereotype.Repository;

/**
 * @author Mr.贾
 * @time 2019/11/1 21:08
 * @describe:
 */
@Repository
public interface LoginReportMapper extends BaseMapper<LoginReport> {

    //获取某个用户最新的登录记录
    LoginReport getNewestLoginReportByCustomer(String phoneNumber);
}
