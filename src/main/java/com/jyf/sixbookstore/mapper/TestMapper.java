package com.jyf.sixbookstore.mapper;

import com.jyf.sixbookstore.temp.Department;
import com.jyf.sixbookstore.temp.Employee;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Mr.贾
 * @time 2019/11/17 13:06
 * @describe:
 */
@Repository
public interface TestMapper {


    int batchInsertEmp(@Param("employeeList") List<Employee> employeeList);

    int batchInsertDept(@Param("departmentList") List<Department> departmentList);

}
