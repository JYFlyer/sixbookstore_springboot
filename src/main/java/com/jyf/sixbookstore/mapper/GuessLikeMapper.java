package com.jyf.sixbookstore.mapper;

import com.jyf.sixbookstore.bean.Book;
import com.jyf.sixbookstore.bean.GuessLike;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author Mr.贾
 * @time 2019/6/29 10:52
 * @describe:
 */
@Repository
public interface GuessLikeMapper {

    int createRecord(GuessLike guessLike);

    void updateRecordTimes(@Param("customerId") Long customerId,@Param("bookNo") String bookNo);

    GuessLike selectGKRecordByCIdAndBNo(@Param("customerId") Long customerId,@Param("bookNo") String bookNo);

    //查询猜你喜欢的书籍
    List<Book> getGuessLikeBook(@Param("customerId") Long customerId, @Param("divisionTime") Date divisionTime);

}
