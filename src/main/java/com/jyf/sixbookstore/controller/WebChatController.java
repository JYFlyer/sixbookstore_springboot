package com.jyf.sixbookstore.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jyf.sixbookstore.bean.ChatInfo;
import com.jyf.sixbookstore.common.controller.BaseController;
import com.jyf.sixbookstore.common.model.Result;
import com.jyf.sixbookstore.common.utils.TimeUtils;
import com.jyf.sixbookstore.service.WebChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Mr.贾
 * @time 2020/1/11 22:37
 * @describe:
 */
@RequestMapping("/webChat")
@RestController
public class WebChatController extends BaseController {

    @Autowired
    private WebChatService webChatService;


    /**
     * 保存聊天信息
     * @param chatInfo
     * @return
     */
    @PostMapping("/saveChatInfo")
    public Result saveChatInfo(@RequestBody ChatInfo chatInfo){

        chatInfo.setTime(TimeUtils.getFormatNow());
        return webChatService.save(chatInfo) ? Result.success("成功") : Result.fail("失败");
    }

    /**
     * 根据发送者ID和接受者ID查询聊天信息
     * @param fromCustomerId
     * @param toCustomerId
     * @return
     */
    @GetMapping("/getChatInfoListByFromAndToId")
    public Result getChatInfoListByFromAndToId(Long fromCustomerId,Long toCustomerId){
        //为0查询群聊的
        if ("0".equals(String.valueOf(toCustomerId))){
            List<ChatInfo> chatInfoList = webChatService.list(new QueryWrapper<ChatInfo>().eq("to_customer_id", 0));
            return chatInfoList.size() <= 0 ? Result.fail("没有聊天记录"):Result.success("有聊天记录",chatInfoList);
        }
        //查询私聊的
        List<ChatInfo> chatInfoList = webChatService.selectListByFromAndToId(fromCustomerId,toCustomerId);
        return chatInfoList.size() <= 0 ? Result.fail("没有聊天记录"):Result.success("有聊天记录",chatInfoList);
    }


}
