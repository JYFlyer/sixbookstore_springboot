package com.jyf.sixbookstore.controller;

import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.common.controller.BaseController;
import com.jyf.sixbookstore.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.Map;


/**
 * @author Mr.贾
 * @time 2019/6/7 17:12
 * @describe: 前台服务的跳转的控制器
 */
@RequestMapping("/frontSkip")
@Controller
public class FrontSkipController extends BaseController {


    @Autowired
    private CustomerService customerService;


    @GetMapping("/toLogin")
    public String toLogin() {
        return "frontend/login";
    }

    @GetMapping("/toRegister")
    public String toRegister(){
        return "frontend/register";
    }

    @GetMapping("/toIndexByYouKe")
    public String toIndexByYouKe(){
        return "frontend/homePage";
    }


    @RequestMapping("/toUpdatePassword")
    public String toFindPassword(){
        return "frontend/updatePasswordStart";
    }

    //TODO aop来实现拦截和跳转
    @RequestMapping("/toUpdatePasswordEnd")
    public String toUpdatePasswordEnd(boolean isStart){
        if (isStart){
            return "frontend/updatePasswordEnd";
        }
        //如果要是直接url跳转，重定向到登录页
        return "redirect:/frontSkip/toLogin";
    }

    //跳转到购物车界面
    @RequestMapping("/toShoppingCart")
    public String toShoppingCartPage() {
        return "frontend/shoppingCart";
    }

    @RequestMapping("/toPayPage")
    public String toPayPage() {
        return "frontend/pay";
    }

    @RequestMapping("/toPayEdit")
    public String toPayEdit(){
        return "frontend/payEdit";
    }

    @RequestMapping("/toPaySuccess")
    public String toPaySuccess(){
        return "frontend/paySuccess";
    }

    /* 个人中心页面 start */

    @RequestMapping("/personalCenter.html")
    public String toPersonalCenter(){
        return "frontend/personalCenter";
    }

    @RequestMapping("/toPersonalInfo")
    public String toPersonalInfo(Map<String,Object> map){
        map.put("customer",customerService.getCustomerById(getCustomerId()));
        return "frontend/pcenter/pInfo";
    }

    @RequestMapping("/toPersonalUpdatePassword")
    public String toPersonalUpdatePassword(){
        return "frontend/pcenter/updatepwd";
    }

    @RequestMapping("/toMyOrder")
    public String toMyOrder(){
        return "frontend/pcenter/myOrder";
    }

    @RequestMapping("/toShoppingReport")
    public String toShoppingReport(){
        return "frontend/pcenter/shoppingReport";
    }

    @RequestMapping("/toLoginReport")
    public String toLoginReport(){
        return "frontend/pcenter/loginReport";
    }

    /* 个人中心页面 end */

    /**
     * 去聊天室页面
     * @return
     */
    @RequestMapping("/toWebChat")
    public String toWebChat(){
        return "frontend/webChat";
    }





}
