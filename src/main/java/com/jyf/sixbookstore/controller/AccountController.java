package com.jyf.sixbookstore.controller;

import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.common.model.Result;
import com.jyf.sixbookstore.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Controller
@RequestMapping("/account")
public class AccountController {
	
	@Autowired
    AccountService accountService;

	
	@ResponseBody
	@RequestMapping(value = "/checkMoney",method = RequestMethod.POST)
	public Result checkMoney(Long customerId, Double totalMoney, Integer payMethod, HttpSession session) {
        Customer customer = (Customer)session.getAttribute("customer");
        if (customer == null || customer.getRealName() == null){
           return Result.fail("请先登录或者完善个人信息!");
        }
        BigDecimal accountMoney=accountService.getAccountMoney(customerId, payMethod);
		double doubleAccountMoney = accountMoney.setScale(2, RoundingMode.HALF_UP).doubleValue();
		if(totalMoney > doubleAccountMoney) {
          //总价格大于账户的钱，说明钱不够了。
          return Result.fail("该支付方式没钱了，请换一个吧！");
        }else {
          return Result.success("可以去付款!");
		}
	}
	


}
