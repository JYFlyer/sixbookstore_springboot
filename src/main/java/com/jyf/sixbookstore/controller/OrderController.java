package com.jyf.sixbookstore.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.bean.Order;
import com.jyf.sixbookstore.common.controller.BaseController;
import com.jyf.sixbookstore.common.constant.Constant;
import com.jyf.sixbookstore.common.model.MakeOrderNo;
import com.jyf.sixbookstore.common.model.Result;
import com.jyf.sixbookstore.common.utils.JsonUtils;
import com.jyf.sixbookstore.common.utils.RedisUtils;
import com.jyf.sixbookstore.service.AccountService;
import com.jyf.sixbookstore.service.BookService;
import com.jyf.sixbookstore.service.CartService;
import com.jyf.sixbookstore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @author Mr.贾
 * @time 2019/6/22 12:05
 * @describe:  订单前端控制器
 */
@Controller
@RequestMapping("/order")
public class OrderController extends BaseController {

    @Autowired
    private OrderService orderService;

    @Autowired
    CartService cartService;

    @Autowired
    AccountService accountService;

    @Autowired
    BookService bookService;

    /**
     * 查询当前会话中用户未支付的订单
     *
     * @return
     */
    @ResponseBody
    @RequestMapping("/getOrder")
    public Result getOrderByCustomerId(HttpSession session) {
        //根据订单编号从redis获取订单
        String strOrderNo = (String) session.getAttribute(Constant.sessionOderNoKey.getName());
        if (StringUtils.isEmpty(strOrderNo)){
            return Result.fail("暂时没有未支付的订单");
        }
        String strRedisOrder = RedisUtils.get(MakeOrderNo.getRedisOrderNo(strOrderNo));
        List<Order> orders = JsonUtils.stringToList(strRedisOrder, Order.class);

        //计算总金额
        BigDecimal totalMoney = new BigDecimal("0.00");
        for (Order order : orders) {
            BigDecimal bookPrice = order.getBookPrice();
            BigDecimal bookNumber = new BigDecimal(order.getBookNumber());
            BigDecimal multiply = bookPrice.multiply(bookNumber);
            totalMoney = totalMoney.add(multiply);
        }

        //利用DecimalFormat转化数字格式
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        String formatTotalMoney = decimalFormat.format(totalMoney);


        Map<String, Object> map = new HashMap<>();
        map.put("orders", orders);
        map.put("totalMoney", formatTotalMoney);
        return Result.success("查询成功", map);
    }

    /**
     * 提交订单
     * 1、修改book表bookStock，修改order表bookStock
     * 2、修改order表支付状态为1
     * 3、钱够了,减去钱，
     * 4、更新redis中订单的信息
     * @param customerId
     * @param totalMoney
     * @param payMethod
     * @return
     */
    @Transactional
    @ResponseBody
    @RequestMapping("/submitOrder")
    public Result submitOrder(Long customerId, String strBookNos, BigDecimal totalMoney, Integer payMethod, HttpSession session) {
        String orderNo = (String) session.getAttribute("orderNo");
        if (orderNo == null || "".equals(orderNo)) {
            return Result.fail("获取订单出现了一点问题,请重新选择商品!");
        }
        String[] split = strBookNos.split(",");
        List<String> bookNos = new ArrayList<>();
        //split数组的数据放到bookNos中
        Collections.addAll(bookNos, split);

        //根据bookNo的顺序得到对应的bookNumber
        //List<Integer> bookNumbers = orderService.getBookNumberByOrderNoAndBookNos(orderNo,bookNos);
        List<Integer> bookNumbers = new ArrayList<>();
        String strOrders = RedisUtils.get(MakeOrderNo.getRedisOrderNo(orderNo));
        List<Order> ordersList = JsonUtils.stringToList(strOrders, Order.class);

        //得到两个集合中一一对应关系的元素的索引位置一样
        for (Order order : ordersList) {
            for (int i = 0; i < bookNos.size(); i++) {
                if (order.getBookNo().equals(bookNos.get(i))) {
                    //在i的位置上添加元素，能保证两个集合中的元素的关系顺序一致
                    bookNumbers.add(i, order.getBookNumber());
                    break;
                }
            }
        }

        //构建一个bookNo对应一个bookNumber的map的list
        List<Map<String, Object>> bookNosAndNumbers = new ArrayList<>();
        for (int i = 0; i < bookNos.size(); i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("bookNo", bookNos.get(i));
            map.put("bookNumber", bookNumbers.get(i));
            bookNosAndNumbers.add(map);
        }
        //先检查库存是否够
        List<Integer> bookStocks = bookService.getBookStocksByBookNos(bookNos);
        for (int i = 0, k = bookStocks.size(); i < k; i++) {
            if (bookNumbers.get(i) > bookStocks.get(i)) {
                return Result.fail("买的数量太多，库存不足了");
            }
        }
        //库存足

        //修改购物车中书的数量
        cartService.updateBookNumberByCustomerIdAndBookNo(customerId, bookNosAndNumbers);

        //修改book表的bookStock
        bookService.updateBookStockByBookNos(bookNosAndNumbers);


        //redis取出来，修改库存和支付状态，保存到mysql，删除redis的
        String strUnPaidOrder = RedisUtils.get(MakeOrderNo.getRedisOrderNo(orderNo));
        if (strUnPaidOrder != null && !"".equals(strUnPaidOrder)) {
            List<Order> orders = JsonUtils.stringToList(strUnPaidOrder, Order.class);
            for (Order order : orders) {
                for (Map<String, Object> bookNoAndNumber : bookNosAndNumbers) {
                    if (order.getBookNo().equals(bookNoAndNumber.get("bookNo"))) {
                        order.setBookStock(order.getBookStock() - Integer.parseInt(String.valueOf(bookNoAndNumber.get("bookNumber"))));
                        order.setPayStatus(Constant.PAYED.getCode());
                        order.setCreateTime(new Date());
                        break;//跳出当前层的for循环
                    }
                }
            }

            orderService.batchInsertOrder(orders);
            //订单支付后，删除redis里面的那个订单
            String tempRedisOrderNo = MakeOrderNo.getRedisOrderNo(orderNo);
            RedisUtils.del(tempRedisOrderNo);
            //更新或者删除customer和order关联的key
            String tempKey = MakeOrderNo.getCustomerUnPaidOrderNoKey(customerId);
            List<String> lrangeOrderNos = RedisUtils.lrange(MakeOrderNo.getCustomerUnPaidOrderNoKey(customerId), 0, -1);
            if (lrangeOrderNos == null || lrangeOrderNos.size() <= 0){
               RedisUtils.del(tempKey);
            }else {
                for (String strOrderNo : lrangeOrderNos) {
                    if (strOrderNo.equals(tempRedisOrderNo)){
                        lrangeOrderNos.remove(strOrderNo);
                        break;
                    }
                }
                RedisUtils.del(tempKey);
                //更新list的值
                if (lrangeOrderNos.size() != 0){
                    for (String lrangeOrderNo : lrangeOrderNos) {
                        RedisUtils.lpush(tempKey,lrangeOrderNo);
                    }
                }
            }
        }

        //修改钱
        accountService.updateCountMoney(customerId, totalMoney, payMethod);
        return Result.success("订单提交成功!");
    }


    /**
     * 个人中心分页查询当前用户所有订单（已支付和未支付的）
     * @return
     */
    @RequestMapping("/getOrdersByCustomerId")
    @ResponseBody
    public Result getOrdersByCustomerId(Order order) {
        Customer customer = this.getCustomer();
        List<String> orderNos = RedisUtils.lrange(MakeOrderNo.getCustomerUnPaidOrderNoKey(customer.getId()),0,-1);
        //获取redis中当前用户未支付的订单
        List<Order> unpaidOrders = new ArrayList<>();
        if (orderNos != null && orderNos.size() > 0){
            for (String orderNo : orderNos) {
                String strRedisOrder = RedisUtils.get(orderNo);
                List<Order> tempList = JsonUtils.stringToList(strRedisOrder, Order.class);
                unpaidOrders.addAll(tempList);
            }
        }
        //设置customerName用于前端显示
        for (Order unpaidOrder : unpaidOrders) {
            unpaidOrder.setCustomerName(customer.getRealName());
        }

        //获取mysql中已支付的订单
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("customerId", customer.getId());
        paramMap.put("payStatus", Constant.PAYED.getCode());
        if (order != null){
            paramMap.put("bookName",order.getBookName());
        }

        Page<Order> page = orderService.orderPage(getPage(Order.class),paramMap);


        unpaidOrders.addAll(page.getRecords());
        page.setRecords(unpaidOrders);

        return Result.success("查询成功", page);

    }




}