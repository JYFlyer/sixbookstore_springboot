package com.jyf.sixbookstore.controller;

import com.jyf.sixbookstore.bean.Book;
import com.jyf.sixbookstore.bean.Cart;
import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.bean.GuessLike;
import com.jyf.sixbookstore.common.model.Result;
import com.jyf.sixbookstore.common.utils.JsonUtils;
import com.jyf.sixbookstore.common.utils.RedisUtils;
import com.jyf.sixbookstore.service.BookService;
import com.jyf.sixbookstore.service.CartService;
import com.jyf.sixbookstore.service.CustomerService;
import com.jyf.sixbookstore.service.GuessLikeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
@RequestMapping("/book")
public class BookController {

    private Logger logger = LoggerFactory.getLogger(BookController.class);
	
	@Autowired
    BookService bookService;

	@Autowired
	CartService cartService;

	@Autowired
	CustomerService customerService;

	@Autowired
	GuessLikeService guessLikeService;
	
	//跳转到购物车界面前检查是否登录
	@ResponseBody
	@RequestMapping("/beforeToShoppingCart")
	public Result toShoppingCart(HttpSession session){
		Customer customer=(Customer)session.getAttribute("customer");
		if(customer==null) {
			return Result.fail("没有登录!");
		}
		return Result.success("登录了!");
	}

	//点击购物车时检查是否登录
	@ResponseBody
	@RequestMapping("/beforeAddToCart")
	public Result beforeAddToCart(HttpSession session) {
		Customer customer=(Customer)session.getAttribute("customer");
		 if(customer == null) {
			 return Result.fail("customer没有登录");
		 }
		return Result.success("customer登录了",customer);
	}
	
	//加入购物车时判断当前用户的购物车是否满了
	@ResponseBody
	@RequestMapping("/getCartBookrows")
	public Result getCartBookRows(Long customerId) {
		   List<Cart> cartBooks=cartService.getCartBookByCustomerId(customerId);
		   if(cartBooks.size()>=10) {
			   return Result.fail("购物车已经满了");
		   }
		return Result.success("没满可以添加");
	}
	
	

	
	//猜你喜欢列表的数据返回
	@ResponseBody
	@RequestMapping("/getGuessLikeBook")
	public Result getGuessLikeBook(HttpSession session){
        Customer customer = (Customer)session.getAttribute("customer");
        List<Book> books = new ArrayList<>();
        //没登录就从数据库中随机4个
        if (customer == null) {
            List<Long> allBookIds = bookService.getAllBookIds();
            List<Long> fourBookIds = new ArrayList<>();
            //随机取出来四个bookId
            for (int i = 0; i < 4 ; i++) {
                //获取[0,allBookIds.size()) 之间的一个随机整数
                int random = new Random().nextInt(allBookIds.size());
                //根据下标得到集合中的元素
                Long aLong = allBookIds.get(random);
                //放到另一集合中
                fourBookIds.add(aLong);
            }
             books = bookService.getBooksByBookIds(fourBookIds);
        }else {
            Long customerId = customer.getId();
            //登录了就拿guessLike表的数据，有一条算一条，不够的在看差几个随机从book表取
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.MONTH,-1);//向前推一个月
            Date divisionTime = calendar.getTime();
            List<Book> guessLikeBook = guessLikeService.getGuessLikeBook(customerId,divisionTime);
            if (guessLikeBook != null && guessLikeBook.size() >= 4) {
                //截取list [0,4)
                books = guessLikeBook.subList(0, 4);
                //反转顺序
                Collections.reverse(books);
            }else {
                //guessLikeBook为空的情况,即登陆了，直接去页面
                if (guessLikeBook == null || guessLikeBook.size() == 0){
                    List<Long> allBookIds = bookService.getAllBookIds();
                    List<Long> fourBookIds = new ArrayList<>();
                    //随机取出来四个bookId
                    for (int i = 0; i < 4 ; i++) {
                        //获取[0,allBookIds.size()) 之间的一个随机整数
                        int random = new Random().nextInt(allBookIds.size());
                        //根据下标得到集合中的元素
                        Long aLong = allBookIds.get(random);
                        //放到另一集合中
                        fourBookIds.add(aLong);
                    }
                    books = bookService.getBooksByBookIds(fourBookIds);
                }else {
                    //guessLikeBook != null && guessLikeBook.size > 0
                    //把从guessLike表中查到的仅存的几个放进去
                    books.addAll(guessLikeBook);
                    //剩下的从book表中随意抽
                    int guessLikeBookSize = guessLikeBook.size();
                    int randomBookSize = 4 - guessLikeBookSize;
                    List<Long> allBookIds = bookService.getAllBookIds();
                    List<Long> randomBookIds = new ArrayList<>();
                    for (int i = 0; i < randomBookSize; i++) {
                        //[0,allBookIds.size())的随机下标
                        int randomNumber = new Random().nextInt(allBookIds.size());
                        randomBookIds.add(allBookIds.get(randomNumber));
                    }
                    //根据ids取出剩下的几本书
                    List<Book> booksByBookIds = bookService.getBooksByBookIds(randomBookIds);
                    //将booksByBookIds 全部数据放到 books 中
                    books.addAll(booksByBookIds);
                }
            }
        }

        return Result.success("查询成功",books);
	}

	
	//去书的详情页面展示数据，每次都增加一条记录
	@RequestMapping("/showBookDetails")
	public String showBookDetails(String bookNo, Map<String,Object> map,HttpSession session) {
         Customer customer = (Customer)session.getAttribute("customer");
         //如果登陆了，且有bookNo
         if (customer != null && bookNo != null && !"".equals(bookNo)){
             Long customerId = customer.getId();
             GuessLike gkByCIdAndBookNo =  guessLikeService.getGuessLikeRecordByCustomerIdAndBookNo(customerId,bookNo);
             if (gkByCIdAndBookNo == null) {
                 GuessLike guessLike = new GuessLike();
                 guessLike.setCustomerId(customerId);
                 guessLike.setBookNo(bookNo);
                 guessLike.setTimes(1);
                 guessLike.setCreateTime(new Date());
                 //添加一条记录
                 guessLikeService.createRecord(guessLike);
             }else {
                 //次数加一
                 guessLikeService.updateRecordTimes(customerId,bookNo);
             }
             Book book= bookService.findBookByBookNo(bookNo);
             map.put("book",book);
         }else {
             if (bookNo != null && !"".equals(bookNo)){
                 //没登录但是有bookNo
                 Book book= bookService.findBookByBookNo(bookNo);
                 map.put("book",book);
             }else {
                 //可能直接跳页面，没有BookNo,就随机一个
                 List<Long> allBookIds = bookService.getAllBookIds();
                 Long randomId = allBookIds.get(new Random().nextInt(allBookIds.size()));
                 Book book = bookService.findBookByBookId(randomId);
                 map.put("book",book);
             }
         }

		return "frontend/bookDetails";
	}


	//纯展示
	@ResponseBody
	@RequestMapping("/getShowBook")
	public Result getShowBook(Integer bookKind){
		if (bookKind == null) {
			return Result.fail("bookKind不能为空!");
		}
        List<Book> showBookByBookKind = null;
        String strShowBookList = null;
        try {
            strShowBookList = RedisUtils.get(String.valueOf(bookKind));
        }catch (Exception e){
             logger.error("redis获取showBook数据失败!");
        }
        if (strShowBookList != null && !"".equals(strShowBookList)){
            showBookByBookKind = JsonUtils.stringToList(strShowBookList,Book.class);
        }else {
            showBookByBookKind = bookService.getShowBookByBookKind(bookKind);
        }
		return Result.success("查询成功",showBookByBookKind);

	}
	
	
	@ResponseBody
	@RequestMapping("/getLeftAndRightBook")
	public Result getLeftAndRightBook(){
        List<Long> lrBookIds = new ArrayList<>();
	    List<Long> allBookIds = bookService.getAllBookIds();
        for (int i = 0,size = allBookIds.size(); i < 12 ; i++) {
            Random random = new Random();
            lrBookIds.add(allBookIds.get(random.nextInt(size)));
        }

        List<Book> books = bookService.getLeftAndRightBook(lrBookIds);
		return Result.success("查询成功",books);
		
	}

	//新书列表
	@ResponseBody
	@RequestMapping("/getNewBookList")
	public Result getNewBookList(Integer bookKind){
        List<Book> newBookList = null;
        String strNewBookList = null;
        try {
            strNewBookList = RedisUtils.get(String.valueOf(bookKind));
        }catch (Exception e){
            logger.error("redis获取NewBook数据失败!");
        }
        if (strNewBookList != null && !"".equals(strNewBookList)){
            newBookList = JsonUtils.stringToList(strNewBookList,Book.class);
        }else {
            newBookList = bookService.getNewBookList(bookKind);
        }

		return Result.success("查询成功",newBookList);
		
	}
	
	//tab
	@ResponseBody
	@RequestMapping("/getTabBook")
	public Result getTabBook(Integer bookKind){
		if (bookKind == null) {
			return Result.fail("bookKind不能为空!");
		}
        List<Book> tabBookByBookKind = null;
        String strTabBook = null;
        try {
            strTabBook = RedisUtils.get(String.valueOf(bookKind));
        }catch (Exception e){
            logger.error("redis获取TabBook数据失败!");
        }
        if (strTabBook != null && !"".equals(strTabBook)) {
            tabBookByBookKind = JsonUtils.stringToList(strTabBook,Book.class);
        }else {
            tabBookByBookKind = bookService.getTabBookByBookKind(bookKind);
        }

		return Result.success("查询成功!",tabBookByBookKind);
	}

}
