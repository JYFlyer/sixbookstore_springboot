package com.jyf.sixbookstore.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.jyf.sixbookstore.bean.Book;
import com.jyf.sixbookstore.bean.Cart;
import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.bean.Order;
import com.jyf.sixbookstore.common.constant.Constant;
import com.jyf.sixbookstore.common.model.MakeOrderNo;
import com.jyf.sixbookstore.common.model.Result;
import com.jyf.sixbookstore.common.utils.JsonUtils;
import com.jyf.sixbookstore.common.utils.RedisUtils;
import com.jyf.sixbookstore.service.BookService;
import com.jyf.sixbookstore.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
@RequestMapping("/cart")
public class CartController {
	
	@Autowired
    CartService cartService;
	@Autowired
	BookService bookService;


    //用户登录后添加购物车操作
    @Transactional
    @ResponseBody
    @RequestMapping("/addToCart")
    public Result addToCart(String bookNo,Integer bookNumber, HttpSession session){
        Book book=bookService.findBookByBookNo(bookNo);
        //从session中获取到customer，进而得到customerId，用来构建cart实例。
        Customer customer=(Customer)session.getAttribute("customer");
        Long customerId = null;
        if(customer!=null) {
            customerId=customer.getId();
        }
        Cart cart=new Cart();
        cart.setCustomerId(customerId);
        cart.setBookName(book.getBookName());
        cart.setBookNo(book.getBookNo());
        cart.setBookPrice(book.getBookPrice());
        cart.setBookNumber(bookNumber);
        cart.setBookStock(book.getBookStock());
        cart.setBookImgUrl(book.getBookImgUrl());
        cart.setBookKind(book.getBookKind());
        cart.setBookId(book.getId());
        //将构造好的cart 传到booService中进行逻辑操作
        bookService.addToCart(cart,bookNumber,customerId,bookNo);

        List<Cart> carts=cartService.getCartBookByCustomerId(customerId);
        List<Integer> bookKinds=cartService.selectBookKindNumberByCustomerId(customerId);
        Integer cartBookNumber=cartService.selectCartBookNumberByCustomerId(customerId);
        //创建一个集合将上面三个东西放进去返回
        Map<String,Object> map=new HashMap<>();
        map.put("allCartBook", carts);
        map.put("bookKinds",bookKinds);
        map.put("cartBookNumber", cartBookNumber);
        return Result.success("查询成功!",map);
    }


	
	//删除单个书
	@RequestMapping("/deleteByBookNo")
	@ResponseBody
	public Result deleteByBookNo(Long customerId, String bookNo){
		//先删除
		cartService.deleteByCustomerIdAndBookNo(customerId, bookNo);
		//在查询
		List<Cart> cartBooks=cartService.getCartBookByCustomerId(customerId);
		return Result.success("删除成功!",cartBooks);
	}
	//删除选中的书
	@ResponseBody
	@RequestMapping("/deleteBookByChecked")
	public Result deleteBookByChecked(Long customerId,String bookNos){
		    List<String> listOfBookNo=new ArrayList<>();
		     String[] bookNoItems=bookNos.split(",");
		     for(int i=0;i<bookNoItems.length;i++) {
                 listOfBookNo.add(bookNoItems[i]);
		     }
		     cartService.deleteBookByChecked(customerId, listOfBookNo);
		     
		 List<Cart> carts=cartService.getCartBookByCustomerId(customerId);
		     
		
		return Result.success("删除成功!",carts);
	}
	//清空购物车
	@ResponseBody
	@RequestMapping("/cleanCart")
	public List<Cart> cleanCart(@RequestParam("customerId") Long customerId){
		cartService.cleanCart(customerId);
		List<Cart> carts=cartService.getCartBookByCustomerId(customerId);
		return carts;
	}
	
	
	
	@ResponseBody
	@RequestMapping("/getCartBook")
	public Result getCartBook(Long customerId){
		return Result.success("查询成功!",cartService.getCartBookByCustomerId(customerId));
		
	}

	/**
	 * 将购物车中用户买的书添加订单
	 * @param customerId
	 * @param bookNos
	 * @param bookNumbers
	 * @return
	 */
    @Transactional
	@ResponseBody
	@RequestMapping("/toPay")
	public Result toPay(Long customerId, String bookNos,
                         String bookNumbers,HttpSession session) {
        List<String> listOfBookNo=new ArrayList<>();
        String[] bookNoItems=bookNos.split(",");

        //数组中的所有元素放到该集合中去
        Collections.addAll(listOfBookNo,bookNoItems);

        List<Integer> listOfBookNumber=new ArrayList<>();
        String[] strBookNumberItems=bookNumbers.split(",");
        for(int j=0;j<strBookNumberItems.length;j++) {
            listOfBookNumber.add(Integer.parseInt(strBookNumberItems[j]));
        }
        //将customer在购物车中选择的书，根据customerId和bookNos查出来，构造订单
        List<Cart> cartBooks=cartService.getCartBookByBookNo(customerId, listOfBookNo);
        //存放订单的list
        List<Order> orders=new ArrayList<>();
        String orderNo = MakeOrderNo.getOrderNo();
        //订单创建时间
        Date createTime = new Date();
        //构建order并放入orders中
        for(int i=0;i<cartBooks.size();i++) {
            Order order =new Order();
            //订单编号的格式为20190606+8位的UUID
            //同一个订单的多个商品的订单编号应该一样
            order.setOrderNo(orderNo);
            order.setCustomerId(cartBooks.get(i).getCustomerId());
            order.setBookId(cartBooks.get(i).getBookId());
            order.setPayStatus(Constant.UNPAID.getCode());
            order.setCreateTime(createTime);
            order.setBookName(cartBooks.get(i).getBookName());
            order.setBookNo(cartBooks.get(i).getBookNo());
            order.setBookPrice(cartBooks.get(i).getBookPrice());
            order.setBookNumber(listOfBookNumber.get(i));//设置bookNumber
            order.setBookStock(cartBooks.get(i).getBookStock());
            order.setBookImgUrl(cartBooks.get(i).getBookImgUrl());
            order.setBookKind(cartBooks.get(i).getBookKind());
            orders.add(order);
         }

        //将当前订单的list转成string
        String strOrders = JsonUtils.objectToString(orders);
        //未支付的订单先放到redis里面,并设置有效期为30天
        RedisUtils.set(MakeOrderNo.getRedisOrderNo(orderNo),strOrders);
        RedisUtils.expire(MakeOrderNo.getRedisOrderNo(orderNo),60*60*24*30);

        //将用户未支付的订单编号放到session中和redis中，session中是更新，redis是增加
        session.setAttribute(Constant.sessionOderNoKey.getName(),orderNo);
        String customerUnPaidOrderNoKey = MakeOrderNo.getCustomerUnPaidOrderNoKey(customerId);
        //放入订单编号,并设置有效期
        RedisUtils.lpush(customerUnPaidOrderNoKey,MakeOrderNo.getRedisOrderNo(orderNo));
        RedisUtils.expire(customerUnPaidOrderNoKey,60*60*24*30);

		return Result.success("成功!");
	}
	

	
	

}
