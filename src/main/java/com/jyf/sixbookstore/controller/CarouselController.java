package com.jyf.sixbookstore.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jyf.sixbookstore.bean.Carousel;
import com.jyf.sixbookstore.common.constant.Constant;
import com.jyf.sixbookstore.common.model.Result;
import com.jyf.sixbookstore.mapper.CarouselMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author Mr.贾
 * @time 2019/10/27 15:34
 * @describe:
 */
@Controller
@RequestMapping("/carousel")
public class CarouselController {

    @Autowired
    private CarouselMapper carouselMapper;



    @ResponseBody
    @RequestMapping("/getListCarousel")
    public Result getListCarousel(){
        QueryWrapper<Carousel> wrapper = new QueryWrapper<>();
        wrapper.eq("available",Constant.AVAILABLE.getCode());
        wrapper.eq("ascription_region",Constant.homePage.getCode());
        wrapper.orderByAsc("sequence");

        List<Carousel> carousels = carouselMapper.selectList(wrapper);
        if (carousels != null && carousels.size() > 0) {
            return Result.success("查询成功",carousels);
        }
        return Result.fail("暂时没有轮播图");
    }

}
