package com.jyf.sixbookstore.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jyf.sixbookstore.bean.LoginReport;
import com.jyf.sixbookstore.common.controller.BaseController;
import com.jyf.sixbookstore.common.model.Result;
import com.jyf.sixbookstore.mapper.LoginReportMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Mr.贾
 * @time 2019/11/1 21:07
 * @describe: 登录记录控制器
 */
@Controller
@RequestMapping("/loginReport")
public class LoginReportController extends BaseController {

    @Autowired
    private LoginReportMapper loginReportMapper;


    /**
     * 查询当前用户的登录记录
     * @return
     */
    @RequestMapping("/getLoginReportPage")
    @ResponseBody
    public Result getLoginReportPage(){
        QueryWrapper<LoginReport> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("create_time");
        wrapper.eq("phone_number",getCustomer().getPhoneNumber());
        IPage<LoginReport> page = loginReportMapper.selectPage(getPage(LoginReport.class), wrapper);
        return Result.success("查询成功",page);

    }








}
