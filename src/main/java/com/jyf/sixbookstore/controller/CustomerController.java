package com.jyf.sixbookstore.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.jyf.sixbookstore.bean.Account;
import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.bean.LoginReport;
import com.jyf.sixbookstore.common.controller.BaseController;
import com.jyf.sixbookstore.common.exception.MyException;
import com.jyf.sixbookstore.common.constant.Constant;
import com.jyf.sixbookstore.common.model.Result;
import com.jyf.sixbookstore.common.utils.*;
import com.jyf.sixbookstore.mapper.LoginReportMapper;
import com.jyf.sixbookstore.service.AccountService;
import com.jyf.sixbookstore.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * customer模块控制器
 */
@Controller
@RequestMapping("/customer")
public class CustomerController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
    CustomerService customerService;

	@Autowired
	AccountService accountService;

	@Autowired
	LoginReportMapper loginReportMapper;

	@Value("${filePath}")
	private String filePath;
    @Value("${DBFilePath}")
	private String DBFilePath;

	//根据手机号修改密码
    @ResponseBody
	@RequestMapping("/updatePasswordEnd")
	public Result updatePassword(String phoneNumber,String password) {
		 customerService.updatePassword(phoneNumber,password);
		 return Result.success("密码修改成功!");
	}

	//个人中心修改密码
	@ResponseBody
    @RequestMapping("/updatePassword2")
	public Result updatePassword2(String password,String newPassword,HttpServletRequest request,HttpServletResponse response){
        if (StringUtils.isEmpty(password) || StringUtils.isEmpty(newPassword)){
           return Result.fail("密码为空!");
        }
        Customer customer = (Customer)request.getSession().getAttribute(Constant.sessionCustomerKey.getName());
        Customer customerById = customerService.getCustomerById(customer.getId());
        if (!customerById.getPassword().equals(password)){
           return Result.fail("原来的密码不正确");
        }
        customerById.setPassword(newPassword);
        boolean updateResult = customerService.updateById(customerById);
        //更新session和cookie中的customer信息
        if (updateResult){
           customer.setPassword(newPassword);
           updateSessionCookieCustomer(request,response,customer);
           return Result.success("修改成功");
        }
        return Result.fail("修改失败");

    }


    /**
     * 注册customer
     * @param customer
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/zhuCeCustomer",method=RequestMethod.POST)
	public Result customerRegister(Customer customer) {
		if (customer == null || "".equals(customer.getPhoneNumber()) || "".equals(customer.getPassword())
		||"".equals(customer.getIdNumber())){
			return Result.fail("手机号、密码、身份证号不能为空!");
		}
		Customer customerByPhone=customerService.checkPhoneNumber(customer.getPhoneNumber());
        if (customerByPhone != null){
        	return Result.fail("该手机号已被注册!");
		}
		//设置默认的昵称
		if (customer.getNickName().equals("")){
		   customer.setNickName("隔壁老王"+UUIDUtils.getUUIDByNumber(8));
		}
		//注册用户
		customerService.zhuCeCustomer(customer);
        //用户和账户关联
        Account account = new Account();
        account.setCustomerId(customer.getId());
        accountService.insertAccount(account);
		return Result.success("注册成功!");
	}

    /**
     * 完善customer信息
     * @param customer
     * @param session
     * @return
     */
	@ResponseBody
	@RequestMapping("/perfectCustomerInfo")
	public Result perfectCustomerInfo(Customer customer,HttpSession session){
        Customer customer1 = (Customer) session.getAttribute(Constant.sessionCustomerKey.getName());
        Long customer1Id = customer1.getId();
        customer.setId(customer1Id);

        customerService.perfectCustomerInfo(customer);
        //完善信息后更新session中customer的信息
        Customer customerById = customerService.getCustomerById(customer1Id);
        session.setAttribute(Constant.sessionCustomerKey.getName(),customerById);
        return Result.success("保存成功!");
    }



	//cookie中得到customer
	//我这里是到前端的文本框中填充字符串，然后在点击登录。
	//自动登录在interceptor中做了
    //@WebLogger(content = "从cookie中获取用户信息")
	@ResponseBody
	@RequestMapping(value = "/getCookieCustomer",method = RequestMethod.POST)
	public Result getCookieCustomer(HttpServletRequest request){
        //请求中获取用户本地的cookie
		Cookie[] cookies = request.getCookies();
		for (Cookie cookie : cookies) {
			String name = cookie.getName();
			if (name.equals(Constant.cookieCustomerKey.getName())){
				String value = cookie.getValue();
                //对cookie中的value进行解密
                String decry = EncrypUtils.Base64Util.decry(value);
                //json字符串转成Customer
                Customer customer = JsonUtils.stringToObject(decry, Customer.class);
                if (customer != null){
                    return Result.success("cookie中有customer",customer);
                }
            }
		}
		return Result.fail("cookie中customer为空!");
	}


    /**
     * 根据手机号查询customer
     * @param phoneNumber
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/checkPhone",method = RequestMethod.POST)
	public Result getCustomerByPhoneNumber(String phoneNumber){
		Customer customer=customerService.checkPhoneNumber(phoneNumber);
		  if(customer==null){
			  return Result.fail("没有根据手机号找到该用户!");
		  }
		  return Result.success("该用户已经存在!");
	 }

    /**
     * 根据身份证号查询customer
     * @param idNumber
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/checkIdNumber",method = RequestMethod.POST)
	public Result getCustomerByIdNumber(String idNumber){
		Customer customer=customerService.checkIdNumber(idNumber);
		if(customer==null){
			return Result.fail("没有根据手机号找到该用户!");
		}
		return Result.success("该用户已经存在!");
	}

    /**
     * 根据身份证号和手机号查询customer
     * @param phoneNumber
     * @param idNumber
     * @param session
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/checkByPhoneNumberAndIdNumber",method = RequestMethod.POST)
	public Result checkByPhoneNumberAndIdNumber(String phoneNumber,String idNumber,HttpSession session){
		Customer customer=customerService.checkByPhoneNumberAndIdNumber(phoneNumber,idNumber);
		if(customer==null){
			return Result.fail("没有根据手机号和身份证号找到该用户!");
		}
		session.setAttribute("phoneNumber",phoneNumber);
		return Result.success("该用户已经存在!");
	}


    /**
     * 登录
     * @param phoneNumber
     * @param password
     * @param rememberMe
     * @return
     */
	@ResponseBody
	@RequestMapping(value="/login",method = RequestMethod.POST)
	public Result customerLogin(String phoneNumber,String password, boolean rememberMe) {
		if (StringUtils.isEmpty(phoneNumber) || StringUtils.isEmpty(password)) {
			return Result.fail("请输入正确的手机号或密码");
		}
		Customer customer2=customerService.checkLogin(phoneNumber,password);
		if (null == customer2.getPassword()){
            return Result.fail("请输入正确的手机号或密码!");
		}
		//登录成功时，将customer放到session中
        getRequest().getSession().setAttribute(Constant.sessionCustomerKey.getName(),customer2);
        //记住我时
        if (rememberMe){
            //将customer放到cookie中
            addCookie(Constant.cookieCustomerKey.getName(),customer2,"/",Constant.cookieTimeOut.getCode());
        }
        //改成在线状态
        Customer customer = new Customer();
        customer.setId(getCustomerId());
        customer.setOnlineStatus(String.valueOf(Constant.ONLINESTATUS.getCode()));
        customerService.updateById(customer);
        return Result.success("登录成功!");
	}

	//页头customer的信息从session中获取
	@ResponseBody
	@RequestMapping("/getCustomer")
	public Result getCustomer(HttpSession session){
        Customer customer = (Customer)session.getAttribute(Constant.sessionCustomerKey.getName());
        if (customer != null) {
            return Result.success("session中有customer登录",customer);
        }
        return Result.fail("session中没有customer登录!");
    }


    /**
     * 修改用户信息
     * @param customer
     * @return
     */
    @PostMapping("/updateCustomerInfo")
    @ResponseBody
    public Result updateCustomerInfo(Customer customer,HttpServletRequest request,HttpServletResponse response){
        boolean b = customerService.updateById(customer);
        //修改成功后更新session和cookie中的值
        if (b){
            updateSessionCookieCustomer(request,response,customer);
            return Result.success("修改成功");
        }
        return Result.fail("修改失败") ;
    }


    /**
     * 初始化简单在线人数统计
     * 实时更新改成定时异步任务+webSocket推送
     * @return
     */
    @ResponseBody
    @RequestMapping("/statisticsOnlineCustomer")
    public Result statisticsOnlineCustomer(){
        Map<String,Integer> countMap = new HashMap<>(2);
        int totalCustomer = customerService.count(null);
        int onlineCustomer = customerService.count(new QueryWrapper<Customer>().eq("online_status", Constant.ONLINESTATUS.getCode()));
        logger.info("当前总人数"+totalCustomer+",在线人数："+onlineCustomer + ",离线人数："+(totalCustomer - onlineCustomer));

        countMap.put("onlineCustomer",onlineCustomer);
        countMap.put("totalCustomer",totalCustomer);
        return Result.success("查询成功",countMap);
    }


    /**
     * 用户修改头像
     * @param uploadHeadImg
     * @return
     */
    @ResponseBody
    @RequestMapping("/uploadHeadImg")
    public Result uploadHeadImg(MultipartFile uploadHeadImg) throws IOException {
        if (uploadHeadImg == null) {
            return Result.fail("头像不能为空!");
        }
        //验证文件大小和格式
        if (!MyFileUtils.checkFile(uploadHeadImg)){
           return Result.fail("文件格式不正确或大小应小于50MB!");
        }
        String originalFilename = uploadHeadImg.getOriginalFilename();
        //重命名文件
        String renameFile = MyFileUtils.renameFile(originalFilename);
        //上传文件
        String DBFilePath2 = MyFileUtils.upload(uploadHeadImg.getBytes(), renameFile, filePath, DBFilePath);
        //如果上传成功了
        if (DBFilePath2 != null) {
            Customer customer = customerService.updateHeadImg(getCustomerId(),DBFilePath2);
            //更新session和cookie中的信息
            updateSessionCookieCustomer(getRequest(),getResponse(),customer);
            return Result.success("修改头像成功",DBFilePath2);
        }else {
            return Result.fail("修改头像失败!");
        }
    }


    /**
     * 查询个人中心中用户的一些信息
     * @return
     */
    @ResponseBody
    @RequestMapping("/getCustomerInfo")
    public Result getCustomerInfo(){
        Map<String,Object> info = new HashMap<>();
        Customer customer = customerService.getCustomerById(getCustomerId());
        if (customer !=null){
            info.put("headImgUrl",customer.getHeadImgUrl());
            info.put("nickName",customer.getNickName());
            LoginReport loginReport = loginReportMapper.getNewestLoginReportByCustomer(customer.getPhoneNumber());
            if (loginReport != null){
               info.put("newestLoginTime",TimeUtils.getFormatDate(loginReport.getCreateTime()));
            }
            return Result.success("有信息",info);
        }

        return Result.fail("没有该用户的一些信息!");
    }

    /**
     * 获取在线用户列表
     * @return
     */
    @ResponseBody
    @RequestMapping("/getOnlineCustomerList")
    public Result getOnlineCustomerList(){
        List<Customer> customerList = customerService.list(new QueryWrapper<Customer>().eq("online_status", Constant.ONLINESTATUS.getCode()));
        if (customerList.size() > 0){
           return Result.success("查询成功",customerList);
        }
        return Result.fail("没值");
    }


    /**
     * session没过期的情况下，用户关闭浏览器时，用户状态变成离线
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/updateCustomerOnlineStatus")
    public Result updateCustomerOnlineStatus(HttpSession session){
        Object object = session.getAttribute(Constant.sessionCustomerKey.getName());
        //如果不为空，就变成离线状态
        if (object != null){
            Customer customer = (Customer)object;
            Customer customer2 = new Customer();
            customer2.setId(customer.getId());
            customer2.setOnlineStatus(String.valueOf(Constant.OFFLINESTATUS.getCode()));
            customerService.updateById(customer2);

            //销毁服务器端该用户的session，防止老的session过期时，更改现有用户的在线状态
            session.invalidate();
            logger.info("用户："+customer.getRealName()+",关闭了浏览器,变成离线状态,该用户session被销毁");
            return Result.success("更新用户状态成功");
        }
        return Result.fail("该用户的session已过期，更新状态交由session的Listener执行");

    }


    /**
     * 用户登出
     * @param session
     * @return
     */
	@RequestMapping("/logout")
	public String logout(HttpSession session) {
        //更改状态为离线
        Customer customer = new Customer();
        customer.setId(getCustomerId());
        customer.setOnlineStatus(String.valueOf(Constant.OFFLINESTATUS.getCode()));
        customerService.updateById(customer);

        //清除cookie中customer的信息
        Cookie customerCookie = new Cookie(Constant.cookieCustomerKey.getName(),"要清除cookie中的customer信息");
        //一定要指定哪个域中的cookie，否则删不掉
        customerCookie.setPath("/");
        customerCookie.setMaxAge(0);
        getResponse().addCookie(customerCookie);

		//销毁session
		session.invalidate();
		return "redirect:/frontSkip/toLogin";
	}




}
