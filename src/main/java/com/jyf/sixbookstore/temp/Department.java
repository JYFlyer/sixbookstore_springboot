package com.jyf.sixbookstore.temp;

import lombok.Data;

/**
 * @author Mr.贾
 * @time 2019/11/17 13:10
 * @describe:
 */
@Data
public class Department {

   private Long id;

   private String deptName;


}
