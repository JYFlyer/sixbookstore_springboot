package com.jyf.sixbookstore.temp;

import lombok.Data;

/**
 * @author Mr.贾
 * @time 2019/11/17 13:08
 * @describe:
 */
@Data
public class Employee {


    private Long id;

    private String empName;

    private Integer gender;

    private Integer age;

    private Double salary;

    private Long deptId;
}
