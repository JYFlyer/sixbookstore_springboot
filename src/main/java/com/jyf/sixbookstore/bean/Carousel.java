package com.jyf.sixbookstore.bean;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Mr.贾
 * @time 2019/10/27 15:33
 * @describe:
 */
@Data
@TableName("t_carousel")
public class Carousel implements Serializable {

    private static final Long serializeID = 7L;

    @TableId
    private Long id;

    /** 轮播图标题 */
    private String title;

    /** 访问地址 */
    private String imgUrl;

    /** 存储地址 */
    private String imgPath;

    /** 对应超链接的url */
    private String contentUrl;

    /** 所属的区域，1代表首页 */
    private Integer ascriptionRegion;

    /** 排序，越小越靠前 */
    private Integer sequence;

    /** 1可用，0不可用 */
    private String available;

    /** 创建时间 */
    private Date createTime;

}
