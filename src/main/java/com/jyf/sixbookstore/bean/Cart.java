package com.jyf.sixbookstore.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@TableName("t_cart")
public class Cart implements Serializable {

    private static final long serialVersionUID = 4L;
    //指定主键为自增
    @TableId(type = IdType.AUTO)
    private Long id;

    private Long customerId;

    private Long bookId;

    private String bookNo;

    private Integer bookNumber;

    @TableField(exist = false)
    private String bookName;

    @TableField(exist = false)
    private BigDecimal bookPrice;

    @TableField(exist = false)
    private Integer bookStock;

    @TableField(exist = false)
    private String bookImgUrl;

    @TableField(exist = false)
    private Integer bookKind;

   
}