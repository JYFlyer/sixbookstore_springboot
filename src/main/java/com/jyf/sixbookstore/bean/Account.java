package com.jyf.sixbookstore.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@TableName("t_account")
public class Account implements Serializable {

    private static final long serialVersionUID = 2L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long customerId;

    private BigDecimal weixin;

    private BigDecimal zhifubao;



}