package com.jyf.sixbookstore.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("t_order")
public class Order implements Serializable {

	private static final long serialVersionUID = 5L;

	@TableId(type = IdType.AUTO)
	private Long id;
	
	private String orderNo;//编号

	private Long customerId;

	@TableField(exist = false)
	private String customerName;
	
	private Long bookId;

	private Integer payStatus;//支付状态0未支付，1已支付

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;//订单的创建时间
	
	private String bookName;

    private String bookNo;

    private BigDecimal bookPrice;

    private Integer bookNumber;
    
    private Integer bookStock;

    private String bookImgUrl;
    
    private Integer bookKind;
    


}
