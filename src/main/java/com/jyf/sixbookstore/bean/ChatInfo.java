package com.jyf.sixbookstore.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author Mr.贾
 * @time 2020/1/7 22:12
 * @describe: web聊天室的聊天消息对象
 */
@Data
@TableName("t_web_chat")
public class ChatInfo {

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 发送者的id
     */
    private Long fromCustomerId;

    /**
     * 接收者的id
     */
    private Long toCustomerId;

    /**
     * 发送者的sessionId
     */
    private String fromSessionId;

    /**
     * 发送者的昵称
     */
    private String fromNickName;

    /**
     * 接受者的sessionId
     */
    private String toSessionId;

    /**
     * 接受者的昵称
     */
    private String toNickName;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 发送时间
     */
    private String time;
}
