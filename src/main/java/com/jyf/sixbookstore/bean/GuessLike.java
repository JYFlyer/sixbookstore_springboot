package com.jyf.sixbookstore.bean;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Mr.贾
 * @time 2019/6/28 20:32
 * @describe: 猜你喜欢数据封装的实体类
 */
@Data
@TableName("t_guess_like")
public class GuessLike implements Serializable {

    private static final long serialVersionUID = 6L;

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    private Long customerId;

    private String bookNo;
    //@JSONField(serialzeFeatures = SerializerFeature.WriteMapNullValue)
    private Integer times;//次数

    /*@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")*/
    private Date createTime;//创建时间


}
