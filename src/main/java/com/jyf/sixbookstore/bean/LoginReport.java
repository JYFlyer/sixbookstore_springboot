package com.jyf.sixbookstore.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Mr.贾
 * @time 2019/11/1 21:00
 * @describe: 登录记录
 */
@Data
@TableName("t_login_report")
public class LoginReport implements Serializable {

    private static final Long serialVersionUID = 8L;

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 客户名
     */
    private String customerName;

    /**
     * 客户手机号
     */
    private String phoneNumber;

    /**
     * 登录设备的ip
     */
    private String loginIp;

    /**
     * 浏览器
     */
    private String browser;

    /**
     * 操作系统名称
     */
    private String os;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /**
     * 1 登录 0 登出
     */
    private Integer loginType;

}
