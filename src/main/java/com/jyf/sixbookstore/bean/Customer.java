package com.jyf.sixbookstore.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("t_customer")
public class Customer implements Serializable {

    private static final Long serializableId = 1L;

    //设置ID自动增长
    @TableId(type = IdType.AUTO)
    private Long id;

    //真实姓名
    private String realName;

    private Integer gender;

    private Integer age;

    private String phoneNumber;

    private String password;

    //身份证号
    private String idNumber;

    //省
    private String province;

    // 市
    private String city;

    //区
    private String region;

    //详细地址
    private String detailAddress;

    //职业
    private String profession;

    //头像地址
    private String headImgUrl;

    //昵称
    private String nickName;

    //在线状态 1在线 0离线
    private String onlineStatus;

    //聊天室用户的会话Id
    private String webChatSessionId;

}