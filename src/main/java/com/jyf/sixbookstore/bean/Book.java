package com.jyf.sixbookstore.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@TableName("t_book")
public class Book implements Serializable {

    private static final long serialVersionUID = 3L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private String bookName;

    private String bookAuthor;

    private String bookPress;

    private String bookNo;

    private String bookPublishDate;

    private Integer bookPage;

    private BigDecimal bookPrice;

    private Integer bookStock;

    private String bookImgUrl;

    private Integer bookKind;



   
}