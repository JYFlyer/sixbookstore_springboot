package com.jyf.sixbookstore.common.listener;

import com.jyf.sixbookstore.bean.Book;
import com.jyf.sixbookstore.common.constant.Constant;
import com.jyf.sixbookstore.common.utils.JsonUtils;
import com.jyf.sixbookstore.common.utils.RedisUtils;
import com.jyf.sixbookstore.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author Mr.贾
 * @time 2019/8/8 21:45
 * @describe: springBoot的ApplicationRunner接口的执行时机是IOC容器初始化完成后
 *               ,可用于项目启动时加载一些资源。
 *               多个实现类用@Order(x)表明优先级，越小优先级越高
 */
@Component
public class MyApplicationRunnerListener implements ApplicationRunner {

    @Autowired
    private BookService bookService;

    private Logger logger = LoggerFactory.getLogger(MyApplicationRunnerListener.class);


    @Override
    public void run(ApplicationArguments args) {
        initHomePageData();




    }


    private void initHomePageData(){
        try {
            //选择1号库,之后的方法都是对1号库的操作
            RedisUtils.getJedis(1);

            if (StringUtils.isEmpty(RedisUtils.get(String.valueOf(Constant.PROGRAMMING.getCode())))){
                //mysql中获取到数据
                List<Book> bookKindOfProgramming = bookService.getTabBookByBookKind(Constant.PROGRAMMING.getCode());
                //转成json字符串
                String strProgramming = JsonUtils.objectToString(bookKindOfProgramming);
                //存入redis，key为常量
                RedisUtils.set(String.valueOf(Constant.PROGRAMMING.getCode()),strProgramming);
            }

            if (StringUtils.isEmpty(RedisUtils.get(String.valueOf(Constant.DATABASE.getCode())))){
                //mysql中获取到数据
                List<Book> bookKindOfDatabase = bookService.getTabBookByBookKind(Constant.DATABASE.getCode());
                //转成json字符串
                String strDatabase = JsonUtils.objectToString(bookKindOfDatabase);
                //存入redis，key为常量
                RedisUtils.set(String.valueOf(Constant.DATABASE.getCode()),strDatabase);
            }

            if (StringUtils.isEmpty(RedisUtils.get(String.valueOf(Constant.OS.getCode())))){
                //mysql中获取到数据
                List<Book> bookKindOfOS = bookService.getTabBookByBookKind(Constant.OS.getCode());
                //转成json字符串
                String strOS = JsonUtils.objectToString(bookKindOfOS);
                //存入redis，key为常量
                RedisUtils.set(String.valueOf(Constant.OS.getCode()),strOS);
            }

            if (StringUtils.isEmpty(RedisUtils.get(String.valueOf(Constant.OFFICE.getCode())))){
                //mysql中获取到数据
                List<Book> strOffice = bookService.getTabBookByBookKind(Constant.OFFICE.getCode());
                //转成json字符串
                String strOS = JsonUtils.objectToString(strOffice);
                //存入redis，key为常量
                RedisUtils.set(String.valueOf(Constant.OFFICE.getCode()),strOS);
            }

            if (StringUtils.isEmpty(RedisUtils.get(String.valueOf(Constant.GRAPHICPROCESSING.getCode())))){
                //mysql中获取到数据
                List<Book> bookKindOfGraphicProcess = bookService.getTabBookByBookKind(Constant.GRAPHICPROCESSING.getCode());
                //转成json字符串
                String strGraphicProcess = JsonUtils.objectToString(bookKindOfGraphicProcess);
                //存入redis，key为常量
                RedisUtils.set(String.valueOf(Constant.GRAPHICPROCESSING.getCode()),strGraphicProcess);
            }

            logger.info("初始化同步数据到redis成功");
        }catch (Exception e){
            logger.error("初始化同步数据到redis失败");
        }
    }




}
