package com.jyf.sixbookstore.common.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author Mr.贾
 * @time 2019/6/9 10:59
 * @describe:  web应用的监听器，服务器启动时，数据源，controller等还没有被加载
 */
public class MyApplicationListener implements ServletContextListener {


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        /*ServletContext servletContext = servletContextEvent.getServletContext();
        //初始化在线人数为0，离线人数为0，总人数为0
        servletContext.setAttribute("onlineCustomer",0);
        servletContext.setAttribute("offlineCustomer",0);
        servletContext.setAttribute("totalCustomer",0);*/

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
