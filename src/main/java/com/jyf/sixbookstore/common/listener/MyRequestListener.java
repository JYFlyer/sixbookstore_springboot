package com.jyf.sixbookstore.common.listener;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Mr.贾
 * @time 2020/1/11 13:04
 * @describe: 为了在建立WebSocket请求时，获取到HttpSession
 */
@WebListener("request请求监听器")
public class MyRequestListener implements ServletRequestListener {


    @Override
    public void requestDestroyed(ServletRequestEvent sre) {

    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        //将所有request请求都携带上httpSession
        ((HttpServletRequest) sre.getServletRequest()).getSession();
    }
}
