package com.jyf.sixbookstore.common.listener;

import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.common.constant.Constant;
import com.jyf.sixbookstore.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author Mr.贾
 * @time 2019/10/26 10:08
 * @describe: 监听session的创建于销毁
 */
@WebListener("在线用户数量监听器")
public class MySessionListener implements HttpSessionListener {

    @Autowired
    private CustomerService customerService;


    private Logger logger = LoggerFactory.getLogger(MySessionListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent se) {

        /*ServletContext servletContext = se.getSession().getServletContext();
        int onlineCustomer = (int)servletContext.getAttribute("onlineCustomer");
        //获取离线人数
        int offlineCustomer = customerService.count(new QueryWrapper<Customer>().eq("online_status", 0));
        //当有会话建立时就+1
        onlineCustomer++;
        //计算总人数
        int totalCustomer = onlineCustomer + offlineCustomer;
        //推送消息
        WebSocketServer.sendInfo(String.valueOf(totalCustomer) + "," + String.valueOf(onlineCustomer));

        servletContext.setAttribute("onlineCustomer",onlineCustomer);
        servletContext.setAttribute("offlineCustomer",offlineCustomer);
        servletContext.setAttribute("totalCustomer",totalCustomer);
        logger.info("当前总人数"+totalCustomer+",在线人数："+onlineCustomer);*/
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        Object o = se.getSession().getAttribute(Constant.sessionCustomerKey.getName());
        if (o != null){
            Customer customer = (Customer) o;
            System.err.println(customer.getId());
            Customer customer2 = new Customer();
            customer2.setId(customer.getId());
            customer2.setOnlineStatus(String.valueOf(Constant.OFFLINESTATUS.getCode()));
            customerService.updateById(customer2);
            logger.info("用户："+customer.getRealName()+",登出了或session超时了");
        }

    }
}
