package com.jyf.sixbookstore.common.websocket;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jyf.sixbookstore.bean.ChatInfo;
import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.common.constant.Constant;
import com.jyf.sixbookstore.common.constant.WebChatInfoType;
import com.jyf.sixbookstore.common.utils.JsonUtils;
import com.jyf.sixbookstore.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Mr.贾
 * @time 2020/1/5 23:08
 * @describe: 在线聊天室的WebSocket服务端
 *  注：一个@ServerEndpoint标注的实例就代表一个服务端，可以有多个服务端
 *     每建立一个连接，就会创建一个WebChatServer对象，因此该服务端是多实例的。
 *     每个方法都可以传一个session对象
 */
@ServerEndpoint(value = "/webChatServer",configurator = HttpSessionConfigurator.class)
@Component
public class WebChatServer {

    //要用静态注入的方式，让属性属于当前类，而不是对象(因为对象会有多个，普通的注入会报错)
    private static CustomerService customerService;

    @Autowired
    public void setCustomerService(CustomerService customerService){
     WebChatServer.customerService = customerService;
    }

    private static Logger logger= LoggerFactory.getLogger(WebChatServer.class);
    //线程安全的map，key是sessionId，value是session会话
    private static Map<String,Session> sessionMap = new ConcurrentHashMap<>();
    //线程安全的计数器
    private static AtomicInteger integer = new AtomicInteger(0);

    //当前登录的customer
    private Customer currCustomer;



    /**
     * 建立连接
     * @param session
     */
    @OnOpen
    public void onOpen(Session session, EndpointConfig config){
        HttpSession httpSession = (HttpSession) config.getUserProperties().get(Constant.HTTPSESSION.getName());
        currCustomer = (Customer)httpSession.getAttribute(Constant.sessionCustomerKey.getName());
        logger.info("customer:-----------id:[{}]建立了连接",currCustomer.getId());
        //在线人数 +1
        int total = integer.incrementAndGet();
        logger.info("当前在线人数：[{}]",total);


        //更新当前用户webChatSessionId的值
        Customer customer1 = new Customer();
        customer1.setId(currCustomer.getId());
        customer1.setWebChatSessionId(session.getId());
        customerService.updateById(customer1);
        //将当前会话加入到Map中
        sessionMap.put(session.getId(),session);
        try {
            //新用户加入的系统消息
            Map<String,Object> map = new HashMap<>(3);
            map.put("nickName",currCustomer.getNickName());
            map.put("customerCounts",integer.get());
            map.put("isClose",0);
            String systemInfo = getInfoMapStr(WebChatInfoType.system,map);
            for (Session session1 : sessionMap.values()) {
                if (session1.isOpen()){
                    session1.getBasicRemote().sendText(systemInfo);
                }
            }

            //将发送方的sessionID写回给发送方
            String infoMapStr = getInfoMapStr(WebChatInfoType.sessionId, session.getId());
            session.getBasicRemote().sendText(infoMapStr);
            //将在线列表推送给每一个在线的人
            List<Customer> customerList = customerService.list(new QueryWrapper<Customer>().eq("online_status",Constant.ONLINESTATUS.getCode()));
            String customerStrs = getInfoMapStr(WebChatInfoType.customerList, customerList);
            for (Session session1 : sessionMap.values()) {
                if (session1.isOpen()){
                    session1.getAsyncRemote().sendText(customerStrs);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 收到客户端消息后调用的方法
     * @param session 发送消息方的会话
     * @param message 客户端发送过来的消息 必须要有的
     * */
    @OnMessage
    public void onMessage(Session session,String message) {
        ChatInfo chatInfo = JsonUtils.stringToObject(message, ChatInfo.class);
        logger.info("收到消息："+chatInfo.toString());
        if (chatInfo.getToSessionId().equals("")){
            //获取所有的session
            Collection<Session> sessionCollection = sessionMap.values();
            for (Session session1 : sessionCollection) {
                //刷新之后旧的session的state变成了close
                if (session1.isOpen()){
                    String messageAll = getInfoMapStr(WebChatInfoType.All, chatInfo);
                    session1.getAsyncRemote().sendText(messageAll);
                }
            }
        }else {
            //向自己和目标发送消息
            String toSessionId = chatInfo.getToSessionId();
            String fromSessionId = chatInfo.getFromSessionId();
            Session fromSession = sessionMap.get(fromSessionId);
            Session toSession = sessionMap.get(toSessionId);
            //注意此时可能session的状态为closed
            String messagePoint = getInfoMapStr(WebChatInfoType.point, chatInfo);
            if (fromSession != null && fromSession.isOpen()){
                fromSession.getAsyncRemote().sendText(messagePoint);
            }
            if (toSession != null && toSession.isOpen()){
                toSession.getAsyncRemote().sendText(messagePoint);
            }
        }
    }

    /**
     * 关闭连接
     */
    @OnClose
    public void onClose(Session session) throws IOException {
        logger.info("customer:-----------id:[{}]关闭了连接",currCustomer.getId());

        //总人数减一
        integer.decrementAndGet();

        //退出的系统消息提醒
        Map<String,Object> map = new HashMap<>(3);
        map.put("nickName",currCustomer.getNickName());
        map.put("customerCounts",integer.get());
        map.put("isClose",1);
        String systemInfo = getInfoMapStr(WebChatInfoType.system,map);
        for (Session session1 : sessionMap.values()) {
            if (session1.isOpen()){
                session1.getBasicRemote().sendText(systemInfo);
            }
        }

        //将最新的在线列表推送给每一在线的人
        List<Customer> customerList = customerService.list(new QueryWrapper<Customer>().eq("online_status",Constant.ONLINESTATUS.getCode()));
        String customerStrs = getInfoMapStr(WebChatInfoType.customerList, customerList);
        for (Session session1 : sessionMap.values()) {
            if (session1.isOpen()){
                session1.getAsyncRemote().sendText(customerStrs);
            }
        }

        //从map中移除这个session
        sessionMap.remove(session.getId());

        logger.info("一个聊天用户断开了连接");
    }

    /**
     * 返回消息的json字符串
     * @param infoType 消息类型
     * @param info 消息内容
     * @return
     */
    private static synchronized String getInfoMapStr(Integer infoType,Object info){
        Map<String,Object> infoMap = new HashMap<>();
        infoMap.put("type",infoType);
        infoMap.put("data",info);
        return JsonUtils.objectToString(infoMap);
    }

}
