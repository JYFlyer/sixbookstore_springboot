package com.jyf.sixbookstore.common.websocket;

import com.jyf.sixbookstore.common.constant.Constant;
import com.jyf.sixbookstore.common.exception.MyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSession;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

/**
 * @author Mr.贾
 * @time 2020/1/11 13:14
 * @describe:
 */
public class HttpSessionConfigurator extends ServerEndpointConfig.Configurator {

    private Logger logger = LoggerFactory.getLogger(WebSocketConfig.class);

    /**
     * 建立webSocket连接时获取HttpSession
     * @param sec
     * @param request
     * @param response
     */
    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        HttpSession httpSession = (HttpSession)request.getHttpSession();
        if (null == httpSession){
            logger.error("httpSession为空, header = [{}], 请登录!", request.getHeaders());
            throw new MyException("httpSession为空, 请登录!");
        }else {
            logger.info("webSocket握手, sessionId = [{}]", httpSession.getId());
            //将httpSession放到WebSocket的一个Map中
            sec.getUserProperties().put(Constant.HTTPSESSION.getName(), httpSession);
        }
    }
}
