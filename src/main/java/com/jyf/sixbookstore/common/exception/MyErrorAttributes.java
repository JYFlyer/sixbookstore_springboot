/*
package com.jyf.sixbookstore.common.exception;

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

*/
/*
 * @author Mr.贾
 * @time 2019/4/14 14:23
 * @describe:
 *//*


@Component
public class MyErrorAttributes extends DefaultErrorAttributes {

    //重写父类的返回错误信息的方法,覆盖掉默认的
    @Override
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
        //封装返回信息的容器
        Map<String,Object> errorMap = new HashMap<>();

        //得到系统默认的所有错误信息
        Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, includeStackTrace);
        String requestUrl = this.getRequestUrl(errorAttributes);
        //替换掉默认的path
        errorAttributes.put("path",requestUrl);

        //系统默认的错误信息返回
        errorMap.put("sysErrorAttr",errorAttributes);
        //自定义通用的错误信息返回
        errorMap.put("author","Mr.贾");
        Map<String,Object> extMap = (Map<String,Object>)webRequest.getAttribute("externalAttr", 0);
        //MyExceptionHandler中自定义的错误信息返回
        errorMap.put("extMap",extMap);
        errorMap.put("errorTime",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        return errorMap;
    }

    //获取请求的url
    private String getRequestUrl(Map<String, Object> errorAttributes){
        //得到request对象
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        //http://localhost:8081/error
        StringBuffer requestUrlBuff = request.getRequestURL();
        //http://localhost:8081/error/frontSkip/toLogin
        requestUrlBuff.append(errorAttributes.get("path"));
        String requestUrl = requestUrlBuff.toString();
        //去掉/error
        requestUrl = requestUrl.replace("/error/","/");
        return requestUrl;
    }

}
*/
