package com.jyf.sixbookstore.common.exception;

import com.jyf.sixbookstore.common.utils.ColorUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mr.贾
 * @time 2019/4/14 11:40
 * @describe:  全局异常处理器
 *             ControllerAdvice：即controller增强器，so只能处理controller中的异常；
 *             可以结合aop来处理其他的层的异常
 */

@ControllerAdvice
public class MyExceptionHandler {



    //专门处理自定义的异常
    @ExceptionHandler(MyException.class)
    public ModelAndView loginExpHandler1(MyException e, HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        handlerException(e,mv,request);
        mv.setViewName("error/5xx");
        return mv;
    }


    //处理各种java异常,包括所有的后代类
    @ExceptionHandler({Exception.class,Throwable.class})
    public ModelAndView ExpHandler1(Exception e, HttpServletRequest request){
        ColorUtils.plnRed("发生了异常!");
        ModelAndView mv = new ModelAndView();
        handlerException(e,mv,request);
        mv.setViewName("error/5xx");
        return mv;
    }

    //公共的方法
    private ModelAndView handlerException(Exception e,ModelAndView mv,HttpServletRequest request) {
        mv.addObject("path", request.getRequestURL());
        mv.addObject("msg", e.getMessage());
        mv.addObject("code", "400");
        mv.addObject("author", "Mr.贾");
        mv.addObject("errorTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        return mv;
    }
    //公共的方法
   /* private void handlerException(Exception e,HttpServletRequest request){
        Map<String,Object> map = new HashMap<>();
        //设置响应的状态码，根据这个去匹配对应的错误页面
        request.setAttribute("status",500);
        map.put("msg",e.getMessage());
        map.put("code","400");//自定义的400代表错误
        request.setAttribute("externalAttr",map);
    }*/


}

