package com.jyf.sixbookstore.common.exception;

/**
 * @author Mr.贾
 * @time 2019/6/6 21:44
 * @describe:  用于抛出异常对象，便于在全局异常处理器中接收信息，从而进一步返回到页面
 */
public class MyException extends RuntimeException {

    private String message;


    public MyException(String message){
        this.message = message;
    }

    //提供一个空参构造器
    public MyException() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
