package com.jyf.sixbookstore.common.constant;

/**
 * @author Mr.贾
 * @time 2020/1/11 15:40
 * @describe: 聊天室服务器端向客户端发送的消息类型
 */
public interface WebChatInfoType {

    /**
     * 所有人
     */
    int All = 0;

    /**
     * 点对点
     */
    int point = 1;

    /**
     * 系统消息
     */
    int system = 2;

    /**
     * 写回sessionId
     */
    int sessionId = 3;

    /**
     * 在线列表信息
     */
    int customerList = 4;
}
