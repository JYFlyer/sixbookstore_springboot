package com.jyf.sixbookstore.common.constant;

/**
 * @author Mr.贾
 * @time 2019/6/6 22:18
 * @describe:  一些常量的枚举类,枚举类相比于接口常量，类常量要跟好一些
 */
public enum Constant {
   //需要定义常量时，直接列举常量对象即可，格式XXX(数字码,注释),

    SUCCESS(200,"处理成功"),
    FAIL(400,"处理失败"),
    UNPAID(0,"未支付"),PAYED(1,"已支付"),
    weixin(0,"微信"),zhifubao(1,"支付宝"),
    MALE(1,"男"), FEMALE(0,"女"),

    PROGRAMMING(1,"编程语言"),
    DATABASE(2,"数据库"),
    OS(3,"操作系统"),
    OFFICE(4,"office"),
    GRAPHICPROCESSING(5,"图形处理"),


    //单位秒
    cookieTimeOut(3600 * 24 * 7 ,"cookie有效时长"),

    //session中customer信息的key
    sessionCustomerKey(1,"customer"),
    //cookie中customer信息的key
    cookieCustomerKey(2,"customerCookie"),
    //session中订单编号的key
    sessionOderNoKey(3,"orderNo"),

    ONLINESTATUS(1,"在线"),
    OFFLINESTATUS(0,"离线"),

    AVAILABLE(1,"可用"),
    UNAVAILABLE(0,"不可用"),

    homePage(1,"首页"),

    UNPAID_ORDER_RETENTION_TIME(60 * 60 * 24 * 30,"未支付订单的保留时长"),

    LOGIN(1,"登录"),
    LOGOUT(0,"登出"),
    //webSocket的Map中存储的sessionkey
    HTTPSESSION(1,"httpSession");






















    private final int code;

    private final String name;
    //默认为private
    Constant(int code,String name){
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Constant{" +
                "code=" + code +
                ", name='" + name + '\'' +
                '}';
    }
}
