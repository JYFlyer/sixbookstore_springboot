package com.jyf.sixbookstore.common.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.common.exception.MyException;
import com.jyf.sixbookstore.common.constant.Constant;
import com.jyf.sixbookstore.common.utils.EncrypUtils;
import com.jyf.sixbookstore.common.utils.HttpContextUtils;
import com.jyf.sixbookstore.common.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;

/**
 * @author Mr.贾
 * @time 2019/9/28 13:02
 * @describe: 基础controller，可以为所有controller的父类，封装一些公共的东西
 */
public class BaseController {


    private Logger logger = LoggerFactory.getLogger(BaseController.class);

    /**
     * 返回当前用户的ID
     * @return
     */
    public Long getCustomerId(){
        return getCustomer().getId();
    }

    /**
     * 返回当前用户
     * @return
     */
    public Customer getCustomer(){
        HttpSession session = HttpContextUtils.getHttpServletRequest().getSession();
        return (Customer)session.getAttribute(Constant.sessionCustomerKey.getName());
    }

    /**
     * 返回当前请求的request对象
     * @return
     */
    public HttpServletRequest getRequest(){
        return HttpContextUtils.getHttpServletRequest();
    }

    /**
     * 返回当前会话session
     * @return
     */
    public HttpSession getSession(){
        return this.getRequest().getSession();
    }

    /**
     * 返回当前请求的response对象
     * @return
     */
    public HttpServletResponse getResponse(){
        return HttpContextUtils.getHttpServletResponse();
    }



    /**
     * 自动获取分页参数，返回分页对象page
     * @param e 实体类型
     * @return page对象
     */
    public <E> Page<E> getPage(Class<E> e) {
        int pageNumber = getParamToInt("pageNumber", 1);
        int pageSize = getParamToInt("pageSize", 10);
        Page<E> page = new Page<>(pageNumber, pageSize);
        //支持sort、order参数
        String sortField = HttpContextUtils.getHttpServletRequest().getParameter("sort");
        String orderBy = HttpContextUtils.getHttpServletRequest().getParameter("order");
        if(!StringUtils.isEmpty(sortField)) {
            if (StringUtils.isEmpty(orderBy)){
                //默认是升序排列
               page.setAsc(sortField);
            }else {
               return "asc".equalsIgnoreCase(orderBy) ? page.setAsc(sortField) : page.setDesc(sortField);
            }
        }
        return page;
    }

    private int getParamToInt(String key, int defaultValue) {
        String pageNumber = HttpContextUtils.getHttpServletRequest().getParameter(key);
        if (StringUtils.isEmpty(pageNumber)) {
            return defaultValue;
        }
        return Integer.parseInt(pageNumber);
    }

    /**
     * 更新session和cookie中customer的信息
     * @param request
     * @param response
     * @param customer 最新的customer
     */
    public void updateSessionCookieCustomer(HttpServletRequest request, HttpServletResponse response, Customer customer){
        //session
        request.getSession().setAttribute(Constant.sessionCustomerKey.getName(),customer);
        //cookie
        String strCustomer = JsonUtils.objectToString(customer);
        String encryCustomer = null;
        encryCustomer = EncrypUtils.Base64Util.encry(strCustomer);
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (Constant.cookieCustomerKey.getName().equals(cookie.getName())){
                //更新cookie，name一样覆盖原来的
                Cookie newCookie = new Cookie(Constant.cookieCustomerKey.getName(),encryCustomer);
                cookie.setPath("/");//cookie有效范围为全局
                cookie.setMaxAge(Constant.cookieTimeOut.getCode());//磁盘上持久化cookie
                //cookie保存到客户端中
                response.addCookie(newCookie);
                break;
            }
        }
    }

    /**
     * 添加cookie到客户端
     * @param cookieName cookie名
     * @param cookieValue cookie值
     * @param path cookie的有效范围
     * @param maxAge 最大有效期 0表示删除该cookie
     */
    public void addCookie(String cookieName,Object cookieValue,String path,int maxAge){
        String strCustomer = JsonUtils.objectToString(cookieValue);
        //cookie的值中不能有符号、要对值进行安全加密
        String customerCookie = EncrypUtils.Base64Util.encry(strCustomer);
        Cookie cookie = new Cookie(cookieName,customerCookie);
        cookie.setPath(path);//cookie有效范围为全局
        cookie.setMaxAge(maxAge);//磁盘上持久化cookie
        //cookie保存到客户端中
        getResponse().addCookie(cookie);
    }




}
