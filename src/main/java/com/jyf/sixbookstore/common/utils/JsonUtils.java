package com.jyf.sixbookstore.common.utils;

import com.alibaba.fastjson.JSON;

import java.util.List;

/**
 * @author Mr.贾
 * @time 2019/6/22 22:49
 * @describe: 基于fastJson在封装的工具类
 */
public class JsonUtils {


    /**
     * json字符串 ---> javabean
     * @param jsonStr
     * @param clazz 需要传入java对象的类型
     * @param <T>
     * @return
     */
    public static <T> T stringToObject(String jsonStr,Class<T> clazz){
        return JSON.parseObject(jsonStr,clazz);
    }

    /**
     * 任意java对象 ---> json字符串
     * @param object 任意java对象
     * @return
     */
    public static String objectToString(Object object){
        return JSON.toJSONString(object);
    }

    /**
     * json字符串 ---> List集合
     * @param jsonStr
     * @param clazz List中元素的类型
     * @param <T>
     * @return
     */
    public static <T> List<T> stringToList(String jsonStr,Class<T> clazz){
        return JSON.parseArray(jsonStr,clazz);
    }




}
