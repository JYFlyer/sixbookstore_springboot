package com.jyf.sixbookstore.common.utils;


/**
 * @author Mr.贾
 * @time 2019/6/23 9:27
 * @describe: 控制台打印颜色字体的工具类
 */
public class ColorUtils {
    //白、红、绿、黄、蓝、紫、浅蓝、灰
    private static final String[] color = {"30","31","32","33","34","35","36","37"};
    //白、红、绿、黄、蓝、紫、浅蓝、灰
    private static final String[] bgColor={"40","41","42","43","44","45","46","47"};
    //无样式、加粗、下划线、反色
    private static final String[] style = {"0","1","4","7"};

    private static final String prefix = "\033[";

    private static final String suffix = "m";

    /*范围：样式后面的所有，所以最后要变成无样式，否则后面的所有都会被应用该样式
    * */

/*-----------------------------以下是换行的打印 pln-----------------------------------------*/
     //打印红色
     public static void plnRed(Object obj){
         System.out.println("\033[31;0m" + obj + "\033[0m");
     }
     //打印绿色
     public static void plnGreen(Object obj){
        System.out.println("\033[32;0m" + obj + "\033[0m");
     }
     //打印蓝色
     public static void plnBlue(Object obj){
        System.out.println("\033[34;0m" + obj + "\033[0m");
     }
     //打印紫色
     public static void plnPurple(Object obj){
        System.out.println("\033[35;0m" + obj + "\033[0m");
     }
     //打印加粗红色
     public static void plnRedB(Object obj){
        System.out.println("\033[31;1m" + obj + "\033[0m");
     }
     //打印加粗红色,背景色蓝色
     public static void plnRedBgbB(Object obj){
        System.out.println("\033[31;44;1m" + obj + "\033[0m");
     }

 /*-----------------------------以下是不换行的打印 p-----------------------------------------*/

    //打印红色
    public static void pRed(Object obj){
        System.out.print("\033[31;0m" + obj + "\033[0m");
    }
    //打印绿色
    public static void pGreen(Object obj){
        System.out.print("\033[32;0m" + obj + "\033[0m");
    }
    //打印蓝色
    public static void pBlue(Object obj){
        System.out.print("\033[34;0m" + obj + "\033[0m");
    }
    //打印紫色
    public static void pPurple(Object obj){
        System.out.print("\033[35;0m" + obj + "\033[0m");
    }
    //打印加粗红色
    public static void pRedB(Object obj){
        System.out.print("\033[31;1m" + obj + "\033[0m");
    }
    //打印加粗红色,背景色蓝色
    public static void pRedBgbB(Object obj){
        System.out.print("\033[31;44;1m" + obj + "\033[0m");
    }

}
