package com.jyf.sixbookstore.common.utils;

import com.jyf.sixbookstore.common.config.MyJedisPool;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import sun.rmi.runtime.Log;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Mr.贾
 * @time 2019/8/3 22:30
 * @describe:
 */
public class RedisUtils {

    //默认是0号库
    private static int baseId = 0;


    //除了该工具类提供的方法外，还可以在外面获取到jedis实例后，调用它原生的api来操作
    /**
     *
     * @return 返回jedis对象,并选择库
     */
    public static Jedis getJedis(int...index){
        Jedis jedis = MyJedisPool.getJedisPool().getResource();
        //默认是0号库，可传入1-16之间的数选择库存放数据
        if (index != null && index[0] > 0 && index[0] <= 16){
            //选择库时，提供的方法以后也会默认使用该库，除非改变选择库
            if (baseId != index[0]){
                baseId = index[0];
                jedis.select(baseId);
            }else {
                jedis.select(baseId);
            }
        }
        return jedis;
    }

   /*########################  key的操作  ################################*/

    /**
     * 返回当前库中所有的key
     * @param pattern
     * @return
     */
    public static Set<String> keys(String pattern){
        Jedis jedis = getJedis(baseId);
        Set<String> keys = jedis.keys(pattern);
        jedis.close();
        return keys;
    }

    /**
     * 删除一个或多个key
     * @param key
     * @return
     */
    public static Long del(String... key){
        Jedis jedis = getJedis(baseId);
        Long delNum = jedis.del(key);
        jedis.close();
        return delNum;
    }

    /**
     * 判断某个key是否还存在
     * @param key
     * @return
     */
    public static Boolean exists(String key){
        Jedis jedis = getJedis(baseId);
        Boolean existsFlag = jedis.exists(key);
        jedis.close();
        return existsFlag;
    }

    /**
     * 设置某个key的过期时间，单位秒
     * @param key
     * @param seconds
     */
    public static void expire(String key,int seconds){
        Jedis jedis = getJedis(baseId);
        jedis.expire(key,seconds);
        jedis.close();
    }

    /**
     * 查看某个key还有几秒过期，-1表示永不过期 ，-2表示已过期
     * @param key
     * @return
     */
    public static Long timeToLive(String key){
        Jedis jedis = getJedis(baseId);
        Long ttl = jedis.ttl(key);
        jedis.close();
        return ttl;
    }

    /**
     * 查看某个key对应的value的类型
     * @param key
     * @return
     */
    public static String type(String key){
        Jedis jedis = getJedis(baseId);
        String type = jedis.type(key);
        jedis.close();
        return type;
    }


   /*########################  string(字符串)的操作  ####################*/

    /**
     * 获取某个key的value，类型要对，只能value是string的才能获取
     * @param key
     * @return
     */
    public static String get(String key){
        Jedis jedis = getJedis(baseId);
        String value = jedis.get(key);
        //释放连接
        jedis.close();
        return value;
    }

    /**
     * 设置某个key的value
     * @param key
     * @param value
     */
    public static void set(String key,String value){
        Jedis jedis = getJedis(baseId);
        jedis.set(key,value);
        //释放连接
        jedis.close();
    }

    /**
     * 字符串后追加内容
     * @param key
     * @param appendContent
     */
    public static void append(String key,String appendContent){
        Jedis jedis = getJedis(baseId);
        jedis.append(key,appendContent);
        jedis.close();
    }

    /**
     * 返回key的value的长度
     * @param key
     * @return
     */
    public static Long strlen(String key){
        Jedis jedis = getJedis(baseId);
        Long strlen = jedis.strlen(key);
        jedis.close();
        return strlen;
    }

    /**
     * value 加1 必须是字符型数字
     * @param key
     * @return
     */
    public static Long incr(String key){
        Jedis jedis = getJedis(baseId);
        Long incrResult  = jedis.incr(key);
        jedis.close();
        return incrResult;
    }

    /**
     * value 减1   必须是字符型数字
     * @param key
     * @return
     */
    public static Long decr(String key){
        Jedis jedis = getJedis(baseId);
        Long decrResult = jedis.decr(key);
        jedis.close();
        return decrResult;
    }

    /**
     * value 加increment
     * @param key
     * @param increment
     * @return
     */
    public static Long incrby(String key,int increment){
        Jedis jedis = getJedis(baseId);
        Long incrByResult = jedis.incrBy(key, increment);
        jedis.close();
        return incrByResult;
    }

    /**
     * value 减increment
     * @param key
     * @param increment
     * @return
     */
    public static Long decrby(String key,int increment){
        Jedis jedis = getJedis(baseId);
        Long decrByResult = jedis.decrBy(key, increment);
        jedis.close();
        return decrByResult;
    }

    /**
     * 给某个key设置过期时间和value，成功返回OK
     * @param key
     * @param seconds
     * @param value
     * @return
     */
    public static String setex(String key,int seconds,String value){
        Jedis jedis = getJedis(baseId);
        String setex = jedis.setex(key, seconds, value);
        jedis.close();
        return setex;
    }


   /*########################  list(列表)的操作  #######################*/
    //lpush rpush lpop rpop lrange lindex llen lset

    /**
     * 从左边向列表中添加值
     * @param key
     * @param str
     */
    public static void lpush(String key,String str){
        Jedis jedis = getJedis(baseId);
        jedis.lpush(key, str);
        jedis.close();
    }

    /**
     * 从右边向列表中添加值
     * @param key
     * @param str
     */
    public static void rpush(String key,String str){
        Jedis jedis = getJedis(baseId);
        jedis.rpush(key, str);
        jedis.close();
    }

    /**
     * 从左边取出一个列表中的值
     * @param key
     * @return
     */
    public static String lpop(String key){
        Jedis jedis = getJedis(baseId);
        String lpop = jedis.lpop(key);
        jedis.close();
        return lpop;
    }

    /**
     * 从右边取出一个列表中的值
     * @param key
     * @return
     */
    public static String rpop(String key){
        Jedis jedis = getJedis(baseId);
        String lpop = jedis.rpop(key);
        jedis.close();
        return lpop;
    }

    /**
     * 取出列表中指定范围内的值，0 到 -1 表示全部
     * @param key
     * @param startIndex
     * @param endIndex
     * @return
     */
    public static List<String> lrange(String key,int startIndex,int endIndex){
        Jedis jedis = getJedis(baseId);
        List<String> lrange = jedis.lrange(key, startIndex, endIndex);
        jedis.close();
        return lrange;
    }

    /**
     * 返回某列表指定索引位置的值
     * @param key
     * @param index
     * @return
     */
    public static String lindex(String key,int index){
        Jedis jedis = getJedis(baseId);
        String lindex = jedis.lindex(key, index);
        jedis.close();
        return lindex;
    }

    /**
     * 返回某列表的长度
     * @param key
     * @return
     */
    public static Long llen(String key){
        Jedis jedis = getJedis(baseId);
        Long llen = jedis.llen(key);
        jedis.close();
        return llen;
    }

    /**
     * 给某列表指定位置设置为指定的值
     * @param key
     * @param index
     * @param str
     */
    public static void lset(String key,Long index,String str){
        Jedis jedis = getJedis(baseId);
        jedis.lset(key,index,str);
        jedis.close();
    }


   /*########################  hash(哈希表)的操作  #######################*/
     //hset hget hmset hmget hgetall hdel hkeys hvals hexists hincrby

    /**
     * 给某个hash表设置一个键值对
     * @param key
     * @param field
     * @param value
     */
    public static void hset(String key,String field,String value){
        Jedis jedis = getJedis(baseId);
        jedis.hset(key,field,value);
        jedis.close();
    }

    /**
     * 取出某个hash表中某个key对应的value
     * @param key
     * @param field
     * @return
     */
    public static String hget(String key,String field){
        Jedis jedis = getJedis(baseId);
        String hget = jedis.hget(key, field);
        jedis.close();
        return hget;
    }

    /**
     * 某个hash表设置一个或多个键值对
     * @param key
     * @param kvMap
     */
    public static void hmset(String key, Map<String,String> kvMap){
        Jedis jedis = getJedis(baseId);
        jedis.hmset(key,kvMap);
        jedis.close();
    }

    /**
     * 取出某个hash表中任意多个key对应的value的集合
     * @param key
     * @param fields
     * @return
     */
    public static List<String> hmget(String key,String...fields){
        Jedis jedis = getJedis(baseId);
        List<String> hmget = jedis.hmget(key, fields);
        jedis.close();
        return hmget;
    }

    /**
     * 取出某个hash表中所有的键值对
     * @param key
     * @return
     */
    public static Map<String, String> hgetall(String key){
        Jedis jedis = getJedis(baseId);
        Map<String, String> kvMap = jedis.hgetAll(key);
        jedis.close();
        return kvMap;
    }

    /**
     * 判断某个hash表中的某个key是否存在
     * @param key
     * @param field
     * @return
     */
    public static Boolean hexists(String key,String field){
        Jedis jedis = getJedis(baseId);
        Boolean hexists = jedis.hexists(key, field);
        jedis.close();
        return hexists;
    }

    /**
     * 返回某个hash表中所有的key
     * @param key
     * @return
     */
    public static Set<String> hkeys(String key){
        Jedis jedis = getJedis(baseId);
        Set<String> keys = jedis.hkeys(key);
        jedis.close();
        return keys;
    }

    /**
     * 返回某个hash表中所有的value
     * @param key
     * @return
     */
    public static List<String> hvals(String key){
        Jedis jedis = getJedis(baseId);
        List<String> hvals = jedis.hvals(key);
        jedis.close();
        return hvals;
    }

    /**
     * 删除某个hash表中的一个或多个键值对
     * @param key
     * @param fields
     */
    public static void hdel(String key,String... fields){
        Jedis jedis = getJedis(baseId);
        jedis.hdel(key, fields);
        jedis.close();
    }

    /**
     * 给某个hash表中的某个key的value增加多少
     * @param key hash表的key
     * @param field 表中的某个key
     * @param increment 增加多少
     * @return
     */
    public static Long hincrby(String key,String field,Long increment){
        Jedis jedis = getJedis(baseId);
        Long incrResult = jedis.hincrBy(key, field, increment);
        jedis.close();
        return incrResult;
    }









   /*########################  set(集合)的操作  #######################*/

   /*########################  zset(有序集合)的操作  #######################*/


    /**
     * 私有化构造器，不让实例化对象
      */
    private RedisUtils(){}


    public static void main(String[] args) {
        JedisPool jedisPool = MyJedisPool.getJedisPool();
        JedisPool jedisPool1 = MyJedisPool.getJedisPool();
        System.out.println(jedisPool == jedisPool1);//true

    }





}
