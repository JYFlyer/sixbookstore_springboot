package com.jyf.sixbookstore.common.utils;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 单文件上传下载
 * @author jyf
 */
public class MyFileUtils {

    private static Logger logger = LoggerFactory.getLogger(MyFileUtils.class);


    /**
     *  上传
     * @param bytesFile 文件的字节码数组
     * @param fileName 文件名
     * @param filePath 文件要存储的目录
     * @param DBFilePath 文件可访问的目录
     * @return  文件可访问的路径
     */
    public static String upload(byte[] bytesFile,String fileName, String filePath, String DBFilePath){

        createDir(filePath);
        //将请求的文件数据写出到服务器所在电脑的指定文件中
        boolean flag = bytesToFile(bytesFile, filePath + "/" + fileName);
        //成功后返回
        if (flag){
           return DBFilePath + "/"+ fileName;
        }
        return null;
    }

    /**
     * 删除某个文件
     * @param fileName 文件的绝对路径
     * @return
     */
    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 确保文件路径存在
     * @param path
     */
    private static void createDir(String path) {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    /**
     * 将文件的字节内容写出到文件
     * @param bytes
     * @param filePath
     * @return
     */
    private static boolean bytesToFile(byte[] bytes, final String filePath) {
        File file = new File(filePath);
        OutputStream os = null;
        BufferedOutputStream bos = null;
        try {
            os = new FileOutputStream(file);
            bos = new BufferedOutputStream(os);
            bos.write(bytes);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != bos) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != os) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;

    }

    /**
     * 文件重命名为时间+8位随机字符串格式
     * @param fileName 需要重命名的文件
     * @return
     */
    public static String renameFile(String fileName) {
        String timeName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String uuidName = UUID.randomUUID().toString().replace("-","").substring(0,8);
        return timeName + uuidName + "." +  fileName.substring(fileName.lastIndexOf(".") + 1);
    }



    /**
     * 文件上传
     * @param file 需要上传的文件
     * @param diskFilePath 文件的存储路径
     * @return 返回上传后的存储文件名
     */
    public static String uploadFile(MultipartFile file,String diskFilePath) {

        InputStream inputStream = null;

        try {
            // 获取带后缀的文件名
            String oldFileName = file.getOriginalFilename();
            String newFileName = UUID.randomUUID().toString().replace("-","")+oldFileName;
            //检查disk存储文件夹是否存在
            File realFile = new File(diskFilePath);
            if (!realFile.exists()) {
                realFile.mkdirs();
            }
            //完整的文件存储路径
            File newRealFile = new File(diskFilePath,"/"+newFileName);
            //获取上传文件的输入流
            inputStream = file.getInputStream();
            //将流写到文件中
            FileUtils.copyInputStreamToFile(inputStream,newRealFile);

            // 返回上传后的文件名
            return newFileName;

        } catch (IOException e1) {
            logger.error(e1.getMessage());
            return null;
        } finally {
            try {
                inputStream.close();
            } catch (Exception e2) {
                logger.error(e2.getMessage());
            }
        }
    }


    /**
     * 文件下载
     * @param response HttpServletResponse对象
     * @param inputStream 要下载的文件的输入流
     * @param showFileName 下载时展示的文件名
     * @throws UnsupportedEncodingException
     */
    public static void downLoadFile(HttpServletResponse response,InputStream inputStream,String showFileName) throws UnsupportedEncodingException {
        //防止文件名中文乱码
        showFileName = new String(showFileName.getBytes("utf-8"),"ISO8859-1");
        //设置响应头
        //设置强制下载不打开
        response.setContentType("application/force-download");
        response.setHeader("content-disposition", "attachment;filename="+showFileName);

        OutputStream outputStream = null;
        BufferedOutputStream bos = null;
        BufferedInputStream bis = null;
        try {
            outputStream = response.getOutputStream();

            bos = new BufferedOutputStream(outputStream);
            bis = new BufferedInputStream(inputStream);

            byte[] b = new byte[1024];
            int length = 0;
            while ((length = bis.read(b)) != -1){
                bos.write(b,0,length);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                bis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private static String[] fileTypeArray = {"jpg","png","jpeg","gif"};


    //验证文件大小和格式
    public static boolean checkFile(MultipartFile uploadHeadImg){
        String originalFilename = uploadHeadImg.getOriginalFilename();
        String suffix = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
        for (String fileType : fileTypeArray) {
            //如果格式正确
            if (fileType.equalsIgnoreCase(suffix)){
               //验证大小50MB
                long fileSize = uploadHeadImg.getSize();
                if (fileSize >0 && fileSize <= 1024 * 1024 * 50){
                    return true;
                }
            }
        }
        return false;
    }



}
