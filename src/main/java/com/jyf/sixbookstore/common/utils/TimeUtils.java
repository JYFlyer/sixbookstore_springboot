package com.jyf.sixbookstore.common.utils;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Mr.贾
 * @time 2019/6/22 10:42
 * @describe: 时间的工具类
 */
public class TimeUtils {


    /**
     * date 转 string
     * @return
     */
    public static String getFormatDate(Date date){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }

    /**
     * 返回如2019-06-06 06:06:06
     * @return
     */
    public static String getFormatNow(){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    /**
     * 返回如2019-06-06
     * @return
     */
    public static String getFormatNowDate(){
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    /**
     * 返回如06:06:06
     * @return
     */
    public static String getFormatNowTime(){
        return new SimpleDateFormat("HH:mm:ss").format(new Date());
    }

    /**
     * 返回如 星期六
     * @return
     */
    public static String getFormatNowWeek(){
        return new SimpleDateFormat("EE").format(new Date());
    }

    /**
     * 返回如2019-06-06 06:06:06 星期二
     * @return
     */
    public static String getFormatNowAndWeek(){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss EE").format(new Date());
    }


    /**
     * 返回包含当天的 00:00:00 和 24:00:00 两个时间的Date[]
     * @return
     */
    public static Date[] getCurrentDayStartAndEndTime(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY,0);//HOUR_OF_DAY是24小时制
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        Date startTime = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH,1);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        Date endTime = calendar.getTime();

        return new Date[]{startTime,endTime};
    }

    /**
     * 获取到当月的第一天​​​​​​ 
     * @return
     */
    public static Date getCurrentMonthFirstDay(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_MONTH,1);
        return calendar.getTime();
    }

    /**
     * 获取到当月的最后一天，注：当DAY_OF_MONTH的值为0时，默认获取上个月的最后一天
     * @return
     */
    public static Date getCurrentMonthLastDay(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH,1);
        calendar.set(Calendar.DAY_OF_MONTH,0);
        return calendar.getTime();
    }

    /**
     * LocalDate转Date
     * @param localDate
     * @return
     */
    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * LocalDateTime转Date
     * @param localDateTime
     * @return
     */
    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Date转LocalDate
     * @param date
     * @return
     */
    public static LocalDate asLocalDate(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    /**
     * Date转LocalDateTime
     * @param date
     * @return
     */
    public static LocalDateTime asLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }



}
