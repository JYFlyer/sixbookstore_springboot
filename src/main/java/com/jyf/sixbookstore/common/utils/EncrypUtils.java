package com.jyf.sixbookstore.common.utils;


import org.apache.commons.codec.binary.Hex;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.*;
import java.util.Base64;

/**
 * @author jyf
 * @time 2019/8/16 16:23
 * @describe: 加解密工具类
 */
public final class EncrypUtils {

    private static final String charset = "UTF-8";


    /**编码算法：Base64
     * 基于64个可打印的字符来表示二进制数据
     * (可逆)，常用于对图片进行编码后传输、对cookie的值编码(cookie不能有中文)
     */
    public static class Base64Util{

        public static String encry(String data) {
            try {
                return Base64.getEncoder().encodeToString(data.getBytes(charset));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return null;
        }

        public static String decry(String encryData){
            byte[] decode = Base64.getDecoder().decode(encryData);
            try {
                return new String(decode,charset);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return null;
        }

    }

    /**
     * 编码算法：URL编码
     * 专门对url进行编码解码的，防止url中文或其他非法字符的出现，导致服务器之间不识别
     */
    public static class URLUtil{

        public static String encry(String data){
            try {
                return URLEncoder.encode(data,charset);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return null;
        }
        public static String decry(String encryData){
            try {
                return URLDecoder.decode(encryData,charset);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return null;
        }

    }

    /**
     * 哈希算法(hash)又称摘要算法(digest)，不可逆。
     * 作用：对任意输入数据进行计算，得到一个固定长度的输出摘要。常用于验证数据是否被篡改。
     * 特性：相同的输入一定会得到相同的输出。
     *      不同的输入有极小的概率会得到相同的输出。(1/43亿)
     */

    /**哈希算法:MD5
     * (不可逆)，产生固定长度的字符串，常用于密码加密、文件加密
     */
    public static class MD5Util{

        public static String encry(String data){
            try {
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                md5.update(data.getBytes(charset));
                byte[] bytes = md5.digest();
                return new BigInteger(1, bytes).toString(16);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return null;
        }

        //MD5盐值加密
        public static String encryWithSalt(String data,String salt){
            try {
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                //加密
                md5.update((data + salt).getBytes());
                //得到加密结果
                byte[] bytes = md5.digest();
                //return new String(new Hex().encode(bytes));
                return bytesToHex(bytes);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            return null;
        }

    }

    /**哈希算法:sha1
     *
     */
    public static class sha1Util{

        public static String encry(String data) throws NoSuchAlgorithmException, UnsupportedEncodingException {
            MessageDigest sha1 = MessageDigest.getInstance("SHA1");
            byte[] digest1 = sha1.digest(data.getBytes(charset));
            return new String(digest1,charset);
        }
    }


    /**
     * DES 对称加密，加解密都需要同一个密钥才行
     */
    public static class DESUtil {

        private static final String KEY_ALGORITHM = "DES";

        private static final String CIPHER_ALGORITHM = "DES/ECB/NoPadding";
        /**
         *
         * 根据一个字符串生成对称密钥
         * @param keyStr 密钥字符串
         * @return 密钥对象
         * @throws InvalidKeyException
         * @throws NoSuchAlgorithmException
         * @throws Exception
         */
        private static SecretKey keyGenerator(String keyStr) throws Exception {
            byte input[] = HexToBytes(keyStr);
            DESKeySpec desKey = new DESKeySpec(input);
            // 创建一个密匙工厂，然后用它把DESKeySpec转换成
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(KEY_ALGORITHM);
            SecretKey securekey = keyFactory.generateSecret(desKey);
            return securekey;
        }


        /**
         * 加密数据
         * @param data 待加密数据
         * @param key 密钥
         * @return 加密后的数据
         */
        public static String encrypt(String data, String key) throws Exception {
            Key deskey = keyGenerator(key);
            // 实例化Cipher对象，它用于完成实际的加密操作
            Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            SecureRandom random = new SecureRandom();
            // 初始化Cipher对象，设置为加密模式
            cipher.init(Cipher.ENCRYPT_MODE, deskey, random);
            byte[] result = cipher.doFinal(data.getBytes());
            return new String(result);
        }

        /**
         * 解密数据
         * @param data 待解密数据
         * @param key 密钥
         * @return 解密后的数据
         */
        public static String decrypt(String data, String key) throws Exception {
            Key deskey = keyGenerator(key);
            Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            // 初始化Cipher对象，设置为解密模式
            cipher.init(Cipher.DECRYPT_MODE, deskey);
            // 执行解密操作
            return new String(cipher.doFinal(data.getBytes()));
        }
    }

    private static int parse(char c) {
        if (c >= 'a')
            return (c - 'a' + 10) & 0x0f;
        if (c >= 'A')
            return (c - 'A' + 10) & 0x0f;
        return (c - '0') & 0x0f;
    }

    /**
     * 二进制数，0和1组成，byte是由8bit，即8位二进制数组成。byte[]是由很多8位的二进制数组成。
     * 8进制数，0~7组成，逢8进1。
     * 10进制数，0~9组成，逢10进1.
     * 16进制数，0~9加上a~f组成，逢16进1。
     */

    /**
     * 十六进制数转字节数组
     * @param hexstr 16进制数
     * @return
     */
    private static byte[] HexToBytes(String hexstr) {
        byte[] b = new byte[hexstr.length() / 2];
        int j = 0;
        for (int i = 0; i < b.length; i++) {
            char c0 = hexstr.charAt(j++);
            char c1 = hexstr.charAt(j++);
            b[i] = (byte) ((parse(c0) << 4) | parse(c1));
        }
        return b;
    }

    /**
     * 字节数组转16进制数
     * @param bytes 需要转换的byte数组
     * @return  转换后的Hex字符串
     */
    private static String bytesToHex(byte[] bytes) {
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(bytes[i] & 0xFF);
            if(hex.length() < 2){
                sb.append(0);
            }
            sb.append(hex);
        }
        return sb.toString();
    }


    public static void main(String[] args) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String encry = MD5Util.encry("123456");
        String encry1 = MD5Util.encry("123456");
        System.out.println(encry);
        System.out.println(encry1);
        System.out.println(encry.equals(encry1));

        String withSalt1 = MD5Util.encryWithSalt("123456", "test");
        String withSalt2 = MD5Util.encryWithSalt("123456", "testtesttesttesttest");
        System.out.println(withSalt1);
        System.out.println(withSalt2);
        System.out.println(withSalt1.equals(withSalt2));
    }

}
