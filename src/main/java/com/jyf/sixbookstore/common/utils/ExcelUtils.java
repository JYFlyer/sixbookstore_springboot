package com.jyf.sixbookstore.common.utils;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 〈list/Excel〉
 *
 * @author cyh
 * @create 2019/3/20
 * @since 1.0.0
 */
public class ExcelUtils {

    //将Excel内容导入list
    public static List<Map<String,Object>> excelToList(MultipartFile file, String name) throws Exception{
        Workbook workbook= WorkbookFactory.create(file.getInputStream());
        Sheet sheet=workbook.getSheet(name);
        //行数
        int num=sheet.getLastRowNum();
        //列数
        int col=sheet.getRow(0).getLastCellNum();

        List<Map<String,Object>> list=new ArrayList<>();
        String[] colName=new String[col];

        //获取列名
        Row row=sheet.getRow(0);
        for (int i=0;i<col;i++){
            String[] s=row.getCell(i).getStringCellValue().split("-");
            colName[i]=s[0];
        }

        //将一行中每列数据放入一个map中,然后把map放入list
        for (int i=1;i<=num;i++){
            Map<String,Object> map=new HashMap<>();
            Row row1=sheet.getRow(i);
            if(row1!=null){
                for (int j=0;j<col;j++){
                    Cell cell=row1.getCell(j);
                    if(cell!=null) {
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        map.put(colName[j], cell.getStringCellValue());
                    }
                }
            }
            list.add(map);
        }
        return list;
    }

    //将数据库内容(list)导出到Excel，发送到前端
    public static void exportToExcel(HttpServletResponse response, Class cls, List<Map<String,Object>> list){
        try {
            //获取类名
            String clsName=cls.getSimpleName();
            //文件名称
            String fileName=clsName+".xls";

            HSSFWorkbook hssfWorkbook=new HSSFWorkbook();
            HSSFSheet hssfSheet=hssfWorkbook.createSheet(clsName);
            int rowNum=0;
            //新建行
            HSSFRow hssfRow=hssfSheet.createRow(rowNum++);
            //列
            int j=0;
            for (String i:list.get(0).keySet()
                 ) {
                hssfRow.createCell(j++).setCellValue(i);
            }
            //将数据放入表中
            for(int i=0;i<list.size();i++){
                HSSFRow row=hssfSheet.createRow(rowNum++);
                Map map=list.get(i);
                j=0;
                for (Object obj:map.values()) {
                    if(obj!=null)
                        row.createCell(j++).setCellValue(obj.toString());
                    else
                        row.createCell(j++);
                }
            }

            // 告诉浏览器用什么软件可以打开此文件
            response.setHeader("content-Type", "application/vnd.ms-excel");
            // 下载文件的默认名称
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName , "utf-8"));
            hssfWorkbook.write(response.getOutputStream());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
