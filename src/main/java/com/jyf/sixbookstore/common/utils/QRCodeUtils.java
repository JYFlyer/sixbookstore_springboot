package com.jyf.sixbookstore.common.utils;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Random;

/**
 * 二维码生成工具类
 * @author jyf
 * @Date 2019-12-13
 * @describe
 */
public class QRCodeUtils {

    private static final String CHARSET = "utf-8";
    private static final String imgFormat = "jpg";
    // 二维码尺寸    
    private static final int QRCODE_SIZE = 300;
    // LOGO宽度    
    private static final int WIDTH = 60;
    // LOGO高度    
    private static final int HEIGHT = 60;



    /**
     * 生成带logo的二维码(将二维码写出到一个输出流中)
     * @param content  内容
     * @param logoPath LOGO地址
     * @param outputStream 输出流
     * @param needCompress 是否压缩LOGO
     * @throws Exception
     */
    public static void createQRCodeWithLogoToOutputStream(String content, String logoPath,
                              OutputStream outputStream, boolean needCompress) throws Exception {
        BufferedImage image = QRCodeUtils.createImage(content, logoPath,
                needCompress);
        ImageIO.write(image, imgFormat, outputStream);
    }

    /**
     * 生成不带logo的二维码(将二维码写出到一个输出流中)
     * @param content 二维码内容
     * @param outputStream 输出流
     * @throws Exception
     */
    public static void createQRCodeToOutputStream(String content, OutputStream outputStream) throws Exception {
        QRCodeUtils.createQRCodeWithLogoToOutputStream(content, null, outputStream, false);
    }



    /**
     * 生成二维码带logo(将二维码写出到一个文件中)
     * @param content  内容
     * @param logoPath LOGO地址
     * @param destPath 存放目录
     * @param needCompress 是否压缩LOGO
     * @return 二维码文件名
     * @throws Exception
     */
    public static String createQRCodeWithLogoToFile(String content, String logoPath, String destPath,
                              boolean needCompress) throws Exception {
        BufferedImage image = QRCodeUtils.createImage(content, logoPath,
                needCompress);
        File file =new File(destPath);
        if (!file.exists() && !file.isDirectory()) {
            file.mkdirs();
        }
        String fileName = new Random().nextInt(99999999)+".jpg";
        ImageIO.write(image, imgFormat, new File(destPath+"/"+fileName));
        return fileName;
    }


    /**
     * 生成二维码不带logo(将二维码写出到一个文件中)
     * @param content 内容
     * @param destPath 存储地址
     * @return 二维码文件名
     * @throws Exception
     */
    public static String createQRCodeToFile(String content, String destPath) throws Exception {
        return QRCodeUtils.createQRCodeWithLogoToFile(content, null, destPath, false);
    }



    /**
     * 解析二维码
     * @param file 二维码图片文件
     * @return
     * @throws Exception
     */
    public static String decode(File file) throws Exception {
        BufferedImage image;
        image = ImageIO.read(file);
        if (image == null) {
            return null;
        }
        BufferedImageLuminanceSource source = new BufferedImageLuminanceSource(
                image);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        Result result;
        Hashtable<DecodeHintType, Object> hints = new Hashtable<>();
        hints.put(DecodeHintType.CHARACTER_SET, CHARSET);
        result = new MultiFormatReader().decode(bitmap, hints);
        return result.getText();
    }

     /*----------------------------以下是两个底层方法------------------------------------------*/

    /**
     * 给二维码插入LOGO
     * @param source 二维码图片
     * @param logoPath LOGO图片地址
     * @param needCompress 是否压缩
     * @throws Exception
     */
    private static void insertLogo(BufferedImage source, String logoPath,
                                    boolean needCompress) throws Exception {
        File file = new File(logoPath);
        if (!file.exists()) {
            System.err.println(""+logoPath+" 该文件不存在！");
            return;
        }
        Image src = ImageIO.read(new File(logoPath));
        int width = src.getWidth(null);
        int height = src.getHeight(null);
        if (needCompress) { // 压缩LOGO
            if (width > WIDTH) {
                width = WIDTH;
            }
            if (height > HEIGHT) {
                height = HEIGHT;
            }
            Image image = src.getScaledInstance(width, height,
                    Image.SCALE_SMOOTH);
            BufferedImage tag = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics g = tag.getGraphics();
            g.drawImage(image, 0, 0, null); // 绘制缩小后的图
            g.dispose();
            src = image;
        }
        // 插入LOGO
        Graphics2D graph = source.createGraphics();
        int x = (QRCODE_SIZE - width) / 2;
        int y = (QRCODE_SIZE - height) / 2;
        graph.drawImage(src, x, y, width, height, null);
        Shape shape = new RoundRectangle2D.Float(x, y, width, width, 6, 6);
        graph.setStroke(new BasicStroke(3f));
        graph.draw(shape);
        graph.dispose();
    }


    /**
     * 生成二维码图片
     * @param content 二维码内容
     * @param logoPath logo的文件路径
     * @param needCompress 是否压缩logo
     * @return BufferedImage
     * @throws Exception
     */
    private static BufferedImage createImage(String content, String logoPath,
                                             boolean needCompress) throws Exception {
        Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        hints.put(EncodeHintType.CHARACTER_SET, CHARSET);
        hints.put(EncodeHintType.MARGIN, 1);
        BitMatrix bitMatrix = new MultiFormatWriter().encode(content,
                BarcodeFormat.QR_CODE, QRCODE_SIZE, QRCODE_SIZE, hints);
        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, bitMatrix.get(x, y) ? 0xFF000000
                        : 0xFFFFFFFF);
            }
        }
        if (logoPath == null || "".equals(logoPath)) {
            return image;
        }
        // 插入图片
        QRCodeUtils.insertLogo(image, logoPath, needCompress);
        return image;
    }


    public static void main(String[] args) throws Exception {
        String text = "哈哈哈";  //这里设置自定义网站url
        String logoPath = QRCodeUtils.class.getClassLoader().getResource("static/img/009.png").toURI().getPath();
        String destPath = "C:\\Users\\admin\\Desktop";
        System.out.println(QRCodeUtils.createQRCodeWithLogoToFile(text,logoPath, destPath,true));
    }
}  