package com.jyf.sixbookstore.common.utils;

import java.util.UUID;

/**
 * @author Mr.贾
 * @time 2019/6/22 10:51
 * @describe:  生成UUID的工具类
 */
public class UUIDUtils {


    /**
     * 返回如 9e311633-76e8-4c66-848c-b3331a60077c
     * @return
     */
    public static String getOriginalUUID(){
        return UUID.randomUUID().toString();
    }

    /**
     * 返回如 9e31163376e84c66848cb3331a60077c
     * @return
     */
    public static String getFormatUUID(){
        return UUID.randomUUID().toString().replace("-","");
    }

    /**
     * 根据传入的数字截取固定长度的UUID
     * @param interceptNumber
     * @return
     */
    public static String getUUIDByNumber(int interceptNumber){
        if (interceptNumber >=32 || interceptNumber <=0 ){
            return UUID.randomUUID().toString().replace("-","");
        }
        return UUID.randomUUID().toString().replace("-","").substring(0,interceptNumber);
    }

}
