package com.jyf.sixbookstore.common.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.Properties;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

  
/** 
 * FTP服务器工具类 
 *  
 */  
public class FTPUtils {
    private static FTPUtils ftpUtils;
    private FTPClient ftpClient;
    private String ip ;//服务器地址
    private String port; // 服务器端口
    private String username; // 用户登录名
    private String password; // 用户登录密码
    private Integer timeout; // 超时时间
    private static String LOCAL_CHARSET = "GBK";

     
    private InputStream is; // 文件下载输入流
    private static final DecimalFormat DF = new DecimalFormat("#.##");//数字格式化
     
   /**
    * 私有构造方法 */

   private FTPUtils() {

   }
 
   /**
    * 获取FTPUtils对象实例 
    * @return 
    *      FTPUtils对象实例 */

   public synchronized static FTPUtils getInstance () {  
       if (null == ftpUtils) {
           ftpUtils = new FTPUtils();  
       }  
       return ftpUtils;  
   }




    /**
     * 新建文件夹
     * @param currentFilePath 当前文件路径
     * @param dirName 新建问价夹的名称
     * @return
     */
   public boolean makeDir(String currentFilePath,String dirName){
       try {
           //连接并切换目录
           boolean b = connectToTheServer(currentFilePath);
           if (b){
               //创建目录
               return ftpClient.makeDirectory(currentFilePath + dirName);
           }
       }catch (Exception e){
           e.printStackTrace();
       }finally {
           logout();
       }
       return false;

   }


    /**
     * 返回pathName目录下的所有文件
     * @param pathName pathName是从ftp服务根目录开始的
     * @return
     */
   public FTPFile[] getFTPFileList(String pathName) {
       try {
           boolean b = connectToTheServer(pathName);
           if (b){
               return ftpClient.listFiles();
           }
       }catch (Exception e){
           e.printStackTrace();
       }finally {
           logout();
       }
       return null;
   }


      
    /** 
     * 上传文件至FTP服务器 
     *
     *      服务器名称 
     * @param storePath 
     *      上传文件存储路径
     * @param fileName 
     *      上传文件存储名称 
     * @param is 
     *      上传文件输入流 
     * @return 
     *      <b>true</b>：上传成功 
     *      <br/> 
     *      <b>false</b>：上传失败 
     */  
    public boolean storeFile (String storePath, String fileName, InputStream is) {
        boolean result = false;  
        try {  
            // 连接至服务器  
            result = connectToTheServer(storePath);  
            // 判断服务器是否连接成功  
            if (result) {  
                // 上传文件  
                result = ftpClient.storeFile(fileName, is);  
            }  
            // 关闭输入流  
            is.close();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
            // 判断输入流是否存在  
            if (null != is) {  
                try {  
                    // 关闭输入流  
                    is.close();  
                } catch (IOException e) {  
                    e.printStackTrace();  
                }  
            }  
            // 登出服务器并断开连接  
            logout();
        }  
        return result;  
    }  
      
    /** 
     * 下载FTP服务器文件至本地<br/> 
     * 操作完成后需调用logout方法与服务器断开连接 
     *
     *      服务器名称 
     * @param remotePath 
     *      下载文件存储路径 
     * @param fileName 
     *      下载文件存储名称 
     * @return 
     *      <b>InputStream</b>：文件输入流 
     */  
    public byte[]  downloadFile (String remotePath, String fileName) {
        try {  
            boolean result = false;  
            // 连接至服务器  
            result = connectToTheServer(remotePath);  
            // 判断服务器是否连接成功  
            if (result) {  
                // 获取文件输入流  
            	ftpClient.enterLocalPassiveMode();
//            	if(ftpClient.listNames(fileName).length==0){
//            		throw new RuntimeException("该文件不存在");
//            	}else{
            		ByteArrayOutputStream os = new ByteArrayOutputStream();
	        		ftpClient.retrieveFile(fileName,os);
	        		
	        		//System.out.println("is.available()=="+is.available());
	        		byte[] bytes = os.toByteArray();
	            	
	                return bytes;
//                }  
            }  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return null;  
    }  
      
    /** 
     * 删除FTP服务器文件
     *      服务器名称 
     * @param remotePath 
     *      服务器文件所在目录
     * @param fileName 
     *      服务器文件存储名称
     * @return 
     *      <b>true</b>：删除成功
     *      <b>false</b>：删除失败 
     */  
    public boolean deleteFile (String remotePath, String fileName) {
        boolean result = false;  
        // 连接至服务器  
        result = connectToTheServer(remotePath);  
        // 判断服务器是否连接成功  
        if (result) {  
            try {  
                //删除文件
                result = ftpClient.deleteFile(fileName);  
            } catch (IOException e) {  
                e.printStackTrace();  
            } finally {  
                // 登出服务器并断开连接  
                logout();
            }  
        }  
        return result;  
    }


    /**
     * 删除文件夹
     * 注意：只能删除空的文件夹
     * @param pathName 要删除的文件夹路径 如 /aa/bb
     * @return
     */
    public boolean removeDir(String pathName){
        String parentPath = pathName.substring(0,pathName.lastIndexOf("/"));
        boolean flag =connectToTheServer(parentPath);
        if (flag){
            try {
                //删除目录
                ftpClient.removeDirectory(pathName);
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                // 登出服务器并断开连接
                logout();
            }
        }
        return flag;
    }

    /**
     * 文件重命名
     * @param currentFilePath 当前文件路径
     * @param oldFileName 老名字
     * @param newFileName 新名字
     * @return
     */
    public boolean renameFile(String currentFilePath,String oldFileName,String newFileName){
        boolean flag =connectToTheServer(currentFilePath);
        if (flag){
            try {
                ftpClient.rename(oldFileName,newFileName);
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                // 登出服务器并断开连接
                logout();
            }
        }
        return flag;
    }
      
    /** 
     * 检测FTP服务器文件是否存在 
     *
     *      服务器名称 
     * @param remotePath 
     *      检测文件存储路径 
     * @param fileName 
     *      检测文件存储名称 
     * @return 
     *      <b>true</b>：文件存在 
     *      <br/> 
     *      <b>false</b>：文件不存在 
     */  
    public boolean checkFile (String remotePath, String fileName) {
        boolean result = false;  
        try {  
            // 连接至服务器  
            result = connectToTheServer(remotePath);  
            // 判断服务器是否连接成功  
            if (result) {  
                // 默认文件不存在  
                result = false;  
                // 获取文件操作目录下所有文件名称  
                String[] remoteNames = ftpClient.listNames();  
                // 循环比对文件名称，判断是否含有当前要下载的文件名  
                for (String remoteName: remoteNames) {  
                    if (fileName.equals(remoteName)) {  
                        result = true;  
                    }  
                }  
            }  
        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
            // 登出服务器并断开连接  
            logout();
        }  
        return result;  
    }  
  
    /** 
     * 登出服务器并断开连接 
     *
     *      FTPClient对象实例 
      * @return 
     *      <b>true</b>：操作成功 
     *      <br/> 
     *      <b>false</b>：操作失败 
     */  
    private boolean logout () {
        boolean result = false;  
        if (null != is) {  
            try {  
                // 关闭输入流  
                is.close();  
            } catch (IOException e) {  
                e.printStackTrace();  
            }  
        }  
        if (null != ftpClient) {  
            try {  
                // 登出服务器  
                result = ftpClient.logout();  
            } catch (IOException e) {  
                e.printStackTrace();  
            } finally {  
                // 判断连接是否存在  
                if (ftpClient.isConnected()) {  
                    try {  
                        // 断开连接  
                        ftpClient.disconnect();  
                    } catch (IOException e) {  
                        e.printStackTrace();  
                    }  
                }  
            }  
        }  
        return result;  
    }


    /**
     * 初始化FtpClient对象
     */
    public void initConfig () {
        // 构造Properties对象
        Properties properties = new Properties();

        // 定义配置文件输入流
        InputStream is = null;
        try {
            // 获取配置文件输入流
            is = FTPUtils.class.getResourceAsStream("/application.properties");
            // 加载配置文件
            properties.load(is);
            // 读取配置文件
            ip = (String) properties.getProperty("ftp.ip");//设置地址
            port = (String) properties.get("ftp.port"); // 设置端口
            username = (String) properties.get("ftp.username"); // 设置用户名
            password = (String) properties.get("ftp.password"); // 设置密码
            timeout = Integer.parseInt((String) properties.get("ftp.timeout")); // 设置连接超时
            System.err.println("ip="+ip+",port="+port+",username="+username+",password="+password+",timeout="+timeout);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 判断输入流是否为空
            if (null != is) {
                try {
                    // 关闭输入流
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 连接（配置通用连接属性）至服务器
     *      服务器名称
     * @param remotePath
     *      当前访问目录
     * @return
     *      <b>true</b>：连接成功
     *      <br/>
     *      <b>false</b>：连接失败
     */
    private boolean connectToTheServer (String remotePath) {
        // 定义返回值
        boolean result = false;
        try {
            initConfig();
            try {
                 ftpClient = new FTPClient();
                 // 连接至服务器，端口默认为21时，可直接通过URL连接
                 ftpClient.connect(ip, Integer.parseInt(port));
                 // 登录服务器
                 ftpClient.login(username, password);
            }catch (Exception e){
                e.printStackTrace();
            }

            // 判断返回码是否合法
            if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
                // 不合法时断开连接
                ftpClient.disconnect();
                // 结束程序
                return result;
            }
            // 设置文件操作目录,如果不存在就创建
            result = ftpClient.changeWorkingDirectory(remotePath);
            System.out.println("changeWorkingDirectory结果："+result);
            /*if (!result){
               ftpClient.makeDirectory(remotePath);
               ftpClient.changeWorkingDirectory(remotePath);
            }*/
            // 设置文件类型，二进制
            result = ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            //设置连接超时
            ftpClient.setConnectTimeout(timeout);
            // 设置缓冲区大小
            ftpClient.setBufferSize(3072);
            // 设置服务器端字符编码
            ftpClient.setControlEncoding("iso-8859-1");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 格式化文件大小（B，KB，MB，GB）
     * @param size
     * @return
     */
    public String formatSize(long size){
        if(size<1024){
            return size + " B";
        }else if(size<1024*1024){
            return size/1024 + " KB";
        }else if(size<1024*1024*1024){
            return (size/(1024*1024)) + " MB";
        }else{
            double gb = size/(1024*1024*1024);
            return DF.format(gb)+" GB";
        }
    }


}