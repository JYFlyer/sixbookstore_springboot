package com.jyf.sixbookstore.common.utils;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author jyf
 * @time 2019/7/3 16:31
 * @describe: 封装的正则表达式验证工具
 */
public class RegExpUtils {


    //用户名（字母、数字、下划线、减号）6到20位
    public static final String username = "^[-_a-zA-Z0-9]{6,20}$";
    //密码（字母、数字、下划线、减号、点）至少6位
    public static final String password = "^[-._a-zA-Z0-9]{6,}$";
    //年龄1-150
    public static final String age = "^(?:[1-9][0-9]?|1[01][0-9]|150)$";
    //数字
    public static final String number = "^[0-9]*$";
    //保留两位有效数字
    public static final String number2 = "^[0-9]+(.[0-9]{2})?$";

    //手机号正则
    public static final String phoneNumber = "^(1[3-9])\\d{9}$";
    //身份证号正则
    public static final String IDNumber = "^\\d{17}[\\d|x]|\\d{15}$";
    //邮箱正则
    public static final String email = "\\w[-\\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\\.)+[A-Za-z]{2,14}";
    //全为中文字符
    public static final String Chinese = "^[\\u4e00-\\u9fa5]{0,}$";
    //中国邮政编码为6位数字
    public static final String postCode = "[1-9]\\d{5}(?!\\d)";
    //qq号
    public static final String QQ = "^[1-9][0-9]{4,10}$";
    //微信号
    public static final String weixin = "^[a-zA-Z]([-_a-zA-Z0-9]{5,19})+$";
    //车牌号
    public static final String carNumber = "^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9挂学警港澳]{1}$";



/*==============================直接根据方法名调用方法即可=====================================*/

    public boolean testPhoneNumber(String needTestStr){
        return test(phoneNumber,needTestStr);
    }

    public boolean testIDNumber(String needTestStr){
        return test(IDNumber,needTestStr);
    }

    public boolean testEmail(String needTestStr){
        return test(email,needTestStr);
    }

    public boolean testChinese(String needTestStr){
        return test(Chinese,needTestStr);
    }

    public boolean testPostCode(String needTestStr){
        return test(postCode,needTestStr);
    }
    public boolean testQQ(String needTestStr){
        return test(QQ,needTestStr);
    }
    public boolean testWeixin(String needTestStr){
        return test(weixin,needTestStr);
    }
    public boolean testCarNumber(String needTestStr){
        return test(carNumber,needTestStr);
    }

    public boolean testNumber(String needTestStr){
        return test(number,needTestStr);
    }

    public boolean testNumber2(String needTestStr){
        return test(number2,needTestStr);
    }

    public boolean testUsername(String needTestStr){
        return test(username,needTestStr);
    }

    public boolean testPassword(String needTestStr){
        return test(password,needTestStr);
    }

    public boolean testAge(String needTestStr){
        return test(age,needTestStr);
    }

    private boolean test(String regularExpression,String needTestStr){
        //编译正则表达式
        Pattern compile = Pattern.compile(regularExpression);
        //匹配字符串得到matcher对象
        Matcher matcher = compile.matcher(needTestStr);
        //返回匹配结果
        return matcher.matches();
    }






    @Test
    public void test(){
        boolean b = testChinese("不22行");
        System.out.println(b);//false

        boolean b1 = testPhoneNumber("17805426969");
        System.out.println(b1);//true

    }


}
