package com.jyf.sixbookstore.common.singleton;

import com.jyf.sixbookstore.bean.LoginReport;

/**
 * @author Mr.贾
 * @time 2019/11/3 11:33
 * @describe: 静态内部类的方式创建：登录记录实体类的单例
 */
public class LoginReportSingleton {


    private LoginReportSingleton(){
    }

    private static class InnerClass{
        private static final LoginReport LOGIN_REPORT = new LoginReport();
    }

    public static LoginReport getInstance(){
        return InnerClass.LOGIN_REPORT;
    }

}
