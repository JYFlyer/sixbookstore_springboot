package com.jyf.sixbookstore.common.aop;

import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.bean.LoginReport;
import com.jyf.sixbookstore.common.constant.Constant;
import com.jyf.sixbookstore.common.model.Result;
import com.jyf.sixbookstore.common.singleton.LoginReportSingleton;
import com.jyf.sixbookstore.common.utils.HttpContextUtils;
import com.jyf.sixbookstore.mapper.LoginReportMapper;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * @author Mr.贾
 * @time 2019/11/1 21:13
 * @describe: 登录记录的切面
 */
@Component
@Aspect
@Order(2)
public class LoginReportAspect {

    @Autowired
    private LoginReportMapper loginReportMapper;


    @Pointcut("execution(public com.jyf.sixbookstore.common.model.Result com.jyf.sixbookstore.controller.CustomerController.customerLogin(..))")
    public void loginReportMethod(){}

    @Pointcut("execution(public java.lang.String com.jyf.sixbookstore.controller.CustomerController.logout(..))")
    public void logoutReportMethod(){}


    /**
     * 在成功登陆并返回后记下登录记录
     * @param joinPoint 连接点
     * @param result 代表返回的对象
     * @throws UnsupportedEncodingException
     */
    @AfterReturning(returning = "result",value = "loginReportMethod()")
    public void makeLoginReport(JoinPoint joinPoint, Object result) throws UnsupportedEncodingException {

        Result result1 = (Result)result;
        Integer code = result1.getCode();
        //如果登陆成功了，再记录
        if (String.valueOf(code).equals(String.valueOf(Constant.SUCCESS.getCode()))){
            LoginReport loginReportInstance = getLoginReportInstance();
            loginReportInstance.setLoginType(Constant.LOGIN.getCode());
            //插入一条登录记录
            loginReportMapper.insert(loginReportInstance);
        }
    }


    /**
     * 用户退出系统时记录
     * @throws UnsupportedEncodingException
     */
    @Before("logoutReportMethod()")
    public void makeLogoutReport() throws UnsupportedEncodingException {

        LoginReport logoutReportInstance = getLoginReportInstance();
        logoutReportInstance.setLoginType(Constant.LOGOUT.getCode());

        //插入一条登出记录
        loginReportMapper.insert(logoutReportInstance);
    }


    private LoginReport getLoginReportInstance() throws UnsupportedEncodingException {
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        request.setCharacterEncoding("UTF-8");

        Customer customer= (Customer)request.getSession().getAttribute(Constant.sessionCustomerKey.getName());
        String realName = customer.getRealName();
        String phoneNumber = customer.getPhoneNumber();
        String ip = request.getRemoteAddr();

        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        Browser browser = userAgent.getBrowser();//获取浏览器信息
        OperatingSystem os = userAgent.getOperatingSystem(); //获取操作系统信息

        //获取单例的loginReport
        LoginReport loginReport = LoginReportSingleton.getInstance();
        loginReport.setCustomerName(realName);
        loginReport.setPhoneNumber(phoneNumber);
        loginReport.setLoginIp(ip);
        loginReport.setBrowser(browser.getName());
        loginReport.setOs(os.getName());
        loginReport.setCreateTime(new Date());

        return loginReport;
    }

}
