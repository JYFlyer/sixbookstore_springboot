package com.jyf.sixbookstore.common.aop;

import com.alibaba.fastjson.JSON;
import com.jyf.sixbookstore.common.exception.MyException;
import com.jyf.sixbookstore.common.utils.ColorUtils;
import com.jyf.sixbookstore.common.utils.HttpContextUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Mr.贾
 * @time 2019/6/15 9:44
 * @describe: 使用aop打印controller、service、mapper层的日志
 */
@Component
@Aspect
@Order(1)
public class MyWebLogAspect {

    //private Logger logger = LoggerFactory.getLogger(MyWebLogAspect.class);
    private static String canonicalPath;

    private ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    /**
     * controller层的可重用的切入点表达式
     * 匹配 com.jyf.sixbookstore.controller 该包下的所有public修饰的方法
     * 第一个*代表任意返回值，第二个代表任意类、接口，第三个代表任意方法名
     * ..代表任意个数和类型的入参
     */

    @Pointcut("execution(public * com.jyf.sixbookstore.controller.*.*(..))")
    public void controllerLog(){

    }

    /**
     * service层的可重用的切入点表达式
     */
    @Pointcut("execution(public * com.jyf.sixbookstore.service.*.*(..))")
    public void serviceLog(){

    }

    /**
     * mapper层的可重用的切入点表达式
     * mapper接口的所有方法都默认为public abstract 的
     */
    @Pointcut("execution(public * com.jyf.sixbookstore.mapper.*.*(..))")
    public void mapperLog(){

    }

    /**
     * target执行前执行该方法
     * 此处引用了上面的切入点表达式
     * RequestContextHolder是springMVC提供的可以在任何地方获取request和response对象
     * 切入点表达式可以用逻辑符号&&,||,!来描述
     * @param joinPoint
     */

    @Before("controllerLog()")
    public void doBefore(JoinPoint joinPoint) throws UnsupportedEncodingException {

        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        //防止post请求参数乱码
        request.setCharacterEncoding("utf-8");


        String strTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S E a").format(new Date());

        //String requestUrl = request.getRequestURL().toString();//请求全路径
        //String requestMethod = request.getMethod();//请求方式
        String requestArgs = JSON.toJSONString(request.getParameterMap());//请求参数的集合
        //String requestUserIp = request.getRemoteAddr();//请求人的ip
        //String requestUserHost = request.getRemoteHost();//主机名
        //String requestUserPort = String.valueOf(request.getRemotePort());//端口号
        String packageAndClassName = joinPoint.getSignature().getDeclaringTypeName();//包+类名
        String methodName = joinPoint.getSignature().getName();//方法名
        /*Object[] args = joinPoint.getArgs();
        Object[] args2 = new Object[args.length];
        //joinPoint.getArgs()中包含下面三个的对象，因为他们三个不能被序列化，如果不排除掉会报错。
        for (int i =0;i<args.length;i++) {
            if (args[i] instanceof ServletRequest || args[i] instanceof ServletResponse|| args[i] instanceof MultipartFile){
                continue;
            }
            args2[i] = args[i];
        }
        String methodArgs = JSON.toJSONString(args2);*/


        //String localIp = request.getLocalAddr();//本地ip
        //String localPort = String.valueOf(request.getLocalPort());//本地端口号
        //String localName = request.getLocalName();//本地主机名字

        String outPutStr = strTime+" "+packageAndClassName+"."+methodName+"--》请求参数："+requestArgs;
        System.out.println(outPutStr);


        threadLocal.set(System.currentTimeMillis());//设置开始时间
    }


    /**
     * target返回结果时执行该方法
     * @param result
     */
    @AfterReturning(returning="result",value = "controllerLog()")
    public void doAfter(JoinPoint joinPoint,Object result){
        String strTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S E a").format(new Date());
        Long startTime = threadLocal.get();
        Long spendTime = System.currentTimeMillis() - startTime;
        String packageAndClassName = joinPoint.getSignature().getDeclaringTypeName();//包+类名
        String methodName = joinPoint.getSignature().getName();

        System.out.println(strTime+" "+packageAndClassName+"."+methodName+"--》返回结果："+result.toString());
        System.out.println(strTime+" "+packageAndClassName+"."+methodName+"----》执行共花费了"+spendTime+"毫秒");
        

    }

    /**
     * target发生异常是执行该方法
     * @param joinPoint
     * @param exception
     */
    @AfterThrowing(throwing = "exception",value = "controllerLog()")
    public void doException(JoinPoint joinPoint,Exception exception) {
        String strTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S E a").format(new Date());
        String targetName = joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName();
        ColorUtils.pRed(strTime+" "+targetName+"---》发生了异常：");
        //此处可以利用反射，判断是Exception还是自定义的异常,打印不同的颜色
        Class<? extends Exception> aClass = exception.getClass();
        String simpleName = aClass.getSimpleName();//只获取类型名如ArithmeticException
        //如果是系统的异常就着重加红色，自定义的不加红
        if ("MyException".equals(simpleName) || exception instanceof MyException){
            ColorUtils.plnRed("异常为："+exception);
        }else {
            //打印堆栈信息
            exception.printStackTrace();
        }



    }

     //追加内容到文件中
     private void appendContentToFile(String outPutFilePath,String content){
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            //第二个参数为true表示在文件末尾追加内容，而不是覆盖
            fos = new FileOutputStream(outPutFilePath,true);
            bos = new BufferedOutputStream(fos);
            //写入内容
            bos.write(content.getBytes());
            bos.write("\r\n".getBytes());//换行
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            //一定要关闭流，否则会出现无法写出内容的情况
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

     }




}
