package com.jyf.sixbookstore.common.aop;

import com.jyf.sixbookstore.common.anotation.WebLogger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * @author Mr.贾
 * @time 2019/8/24 10:04
 * @describe:
 */
/*@Component
@Aspect
@Order*/ //Lower values have higher priority,值越小优先级越高，默认优先级最小
public class MyAnnotationAspectTest {

    //基于自定义注解的切入点表达式
    @Pointcut("@annotation(com.jyf.sixbookstore.common.anotation.WebLogger)")
    public void testAnnotationPointcut(){}


    @Before("testAnnotationPointcut()")
    public void testAnnotationBefore(JoinPoint joinPoint) throws UnsupportedEncodingException {
        String packetAndClassName = joinPoint.getSignature().getDeclaringTypeName();
        String methodName = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        ServletRequestAttributes servletRequestAttributes =(ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        //防止post请求参数乱码
        request.setCharacterEncoding("utf-8");
        Map<String, String[]> parameterMap = request.getParameterMap();
        //通过反射获取目标的注解
        WebLogger webLogger = joinPoint.getTarget().getClass().getAnnotation(WebLogger.class);
        System.out.println("方法："+packetAndClassName+methodName);
        System.out.println("入参："+args.toString());
        System.out.println("请求参数："+parameterMap.toString());
        if (webLogger != null){
            System.out.println("注解的值："+webLogger.content());
        }
    }



}
