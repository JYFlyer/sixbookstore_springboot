package com.jyf.sixbookstore.common.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author Mr.贾
 * @time 2019/9/28 13:57
 * @describe: 1、继承IService，在原来的基础上构建自定义的BaseService(当前采用的)
 *            2、或者模仿着IService，创建自定义的BaseService
 */
public interface BaseService<T> extends IService<T> {

    /**
     * 自定义方法名的查询list方法
     * @param wrapper
     * @return
     */
    List<T> selectList(Wrapper<T> wrapper);


}
