package com.jyf.sixbookstore.common.config;

import com.jyf.sixbookstore.common.interceptor.CustomerLoginInterceptor;
import com.jyf.sixbookstore.common.interceptor.CustomerNoLoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;
import java.io.IOException;


/**
 * @author Mr.贾
 * @time 2019/6/9 19:42
 * @describe: webmvc的配置
 */
@Configuration
public class MyWebMvcConfig implements WebMvcConfigurer {

    @Autowired
    CustomerLoginInterceptor customerLoginInterceptor;
    @Autowired
    CustomerNoLoginInterceptor customerNoLoginInterceptor;

    //前台服务需要登录拦截的路径数组
    private String[] customerPath={"/customer/perfectCustomerInfo","/customer/getCustomer","/customer/getHeadImg"
            ,"/frontSkip/toPay","/frontSkip/toShoppingCart","/frontSkip/toPayEdit"
            ,"/frontSkip/toPaySuccess","/frontSkip/toPersonalInfo","/frontSkip/toWebChat"};

    private String[] noLoginPath = {"/","/index.html","/homePage.html","/frontSkip/toHome","/frontSkip/toIndex"
                                    ,"/book/showBookDetails"};

    //访问这个路径时，显示的是这个路径，但实际上是去视图解析器解析的路径
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("redirect:/homePage.html");
        registry.addViewController("/index.html").setViewName("frontend/homePage");
        registry.addViewController("/homePage.html").setViewName("frontend/homePage");
        registry.addViewController("/frontSkip/toHome").setViewName("redirect:/homePage.html");
        registry.addViewController("/frontSkip/toIndex").setViewName("redirect:/index.html");
    }

    //注册拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(customerLoginInterceptor)
                .addPathPatterns(customerPath);
        registry.addInterceptor(customerNoLoginInterceptor)
                .addPathPatterns(noLoginPath);
    }

  /*  // 虚拟路径映射
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //用文件服务器上的资源就行了
        //获取文件的存储目录
        File file = new File(System.getProperty("user.dir") + "/../sixBookStoreResource");
        try {
            //判断是windows系统还是linux系统，做不同路径的映射
            String osName = System.getProperty("os.name").toLowerCase().substring(0,3);
            if (osName.equals("win")){
                registry.addResourceHandler("/resource/**")
                        .addResourceLocations("file:"+file.getCanonicalPath()+"\\");
            }else if (osName.equals("lin")){
                registry.addResourceHandler("/resource/**")
                        .addResourceLocations("file:"+file.getCanonicalPath()+"/");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/


    /*
    配合@JsonField注解使用
    @Bean
    public HttpMessageConverters httpMessageConverter(){
        FastJsonHttpMessageConverter fastJson = new FastJsonHttpMessageConverter();
        return new HttpMessageConverters(fastJson);
    }*/


}
