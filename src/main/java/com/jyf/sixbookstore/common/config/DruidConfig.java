package com.jyf.sixbookstore.common.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mr.贾
 * @time 2019/6/6 20:36
 * @describe:  Druid的配置类
 */
@Configuration
public class DruidConfig {

    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource dataSource(){
        return new DruidDataSource();
    }

    /**
     * 配置Druid的监控
     * 1、配置一个管理后台的Servlet
     */
    @Bean
    public ServletRegistrationBean statViewServlet(){
        ServletRegistrationBean srb = new ServletRegistrationBean();
        srb.setServlet(new StatViewServlet());
        srb.setUrlMappings(Arrays.asList("/druid/*"));

        Map<String,String> initParams = new HashMap<>();
        initParams.put("loginUsername","admin");
        initParams.put("loginPassword","admin");
        initParams.put("allow","127.0.0.1");//""为默认的允许所有访问
        //initParams.put("deny","192.168.1.101");//不让该IP访问
        srb.setInitParameters(initParams);
        return srb;
    }

    //2、配置一个web监控的filter，拦截sql等的使用状况
    @Bean
    public FilterRegistrationBean statFilter(){
        FilterRegistrationBean filter = new FilterRegistrationBean();
        filter.setFilter(new WebStatFilter());
        filter.setUrlPatterns(Arrays.asList("/*"));//设置拦截所有请求

        Map<String,String> initParams = new HashMap<>();
        //不拦截那些请求
        initParams.put("exclusions","/druid/*,*.html,*.js,*.css,*.jpg,*.png,*.ico");
        filter.setInitParameters(initParams);
        return filter;

    }


}
