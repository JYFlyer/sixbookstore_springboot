package com.jyf.sixbookstore.common.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Mr.贾
 * @time 2019/6/6 20:41
 * @describe:
 */
@Configuration
@MapperScan("com.jyf.sixbookstore.mapper")
public class MybatisPlusConfig {

    /**
     * MP内置分页插件
     * @return
     */
    @Bean
    public PaginationInterceptor paginationInterceptor(){
        return new PaginationInterceptor();
    }

    /**
     * MP内置sql性能分析插件
     * 用于输出每条 SQL 语句及其执行时间，只在开发和测试环境下使用
     * @return
     */
    //@Bean
    //@Profile({"dev","test"})
    public PerformanceInterceptor performanceInterceptor(){
        PerformanceInterceptor performanceInterceptor = new PerformanceInterceptor();
        //performanceInterceptor.setFormat(true);//sql格式化输出
        performanceInterceptor.setMaxTime(1000);//最大执行时长，超过自动停止执行，单位毫秒
        return performanceInterceptor;
    }

}
