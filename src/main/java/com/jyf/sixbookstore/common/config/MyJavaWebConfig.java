package com.jyf.sixbookstore.common.config;

import com.jyf.sixbookstore.common.listener.MyApplicationListener;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletContextListener;


/**
 * @author Mr.贾
 * @time 2019/6/9 11:06
 * @describe: 配置javaweb中的三大器
 */
@Configuration
public class MyJavaWebConfig {

     //注册listener
     @Bean
     public ServletListenerRegistrationBean<ServletContextListener> servletRegistrationBean(){
         ServletListenerRegistrationBean<ServletContextListener> sr = new ServletListenerRegistrationBean<>();
         sr.setListener(new MyApplicationListener());
         return sr;
     }



}
