package com.jyf.sixbookstore.common.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisException;

/**
 * @author Mr.贾
 * @time 2019/8/3 23:11
 * @describe: 懒汉式 初始化jedisPool
 */
@Component
public class MyJedisPool {

    private static final Logger logger = LoggerFactory.getLogger(MyJedisPool.class);

    /*
    * springBoot的@Value不支持静态的属性和方法，可以通过非静态的setter来给静态的属性赋值。
    * 且使用@Value的类，必须是IOC容器的一个组件。
    *  但可以放进去后不用，还是用原来的
    * */

    private static String redisHost;

    private static int redisPort;

    private static String auth;


    @Value("${redis.host}")
    public void setRedisHost(String redisHost) {
        MyJedisPool.redisHost = redisHost;
    }

    @Value("${redis.port}")
    public void setRedisPort(int redisPort) {
        MyJedisPool.redisPort = redisPort;
    }

    /*@Value("${redis.auth}")
    public void setAuth(String auth) {
        MyJedisPool.auth = auth;
    }*/



    private MyJedisPool(){}

    private static JedisPool jedisPool = null;


    //返回单例的jedisPool
    public static JedisPool getJedisPool() throws JedisException {
            if (null == jedisPool){//如果为null时才等待
            synchronized (logger){
                if (null == jedisPool){
                    //利用Propertise类读取配置文件内容来完成初始化
                    JedisPoolConfig poolConfig = new JedisPoolConfig();
                    //========= jedisPool的一些配置=============================
                    poolConfig.setMaxTotal(10000);
                    poolConfig.setMaxIdle(50);
                    poolConfig.setMaxWaitMillis(5 * 1000);
                    //timeout 连接超时
                    jedisPool = new JedisPool(poolConfig,redisHost, redisPort,2000);
                }
            }
        }
            return jedisPool;
    }
}
