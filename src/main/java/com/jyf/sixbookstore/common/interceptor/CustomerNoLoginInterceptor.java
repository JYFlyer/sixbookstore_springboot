package com.jyf.sixbookstore.common.interceptor;

import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.common.utils.EncrypUtils;
import com.jyf.sixbookstore.common.utils.JsonUtils;
import com.jyf.sixbookstore.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Mr.贾
 * @time 2019/8/17 15:02
 * @describe: 拦截不需要登录的页面，检查cookie，放到session中
 */
@Component
public class CustomerNoLoginInterceptor implements HandlerInterceptor {


    @Autowired
    private CustomerLoginInterceptor customerLoginInterceptor;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //去第二个不需要登录的页面时，如果session里面有customer，就不用再从cookie里面取，然后数据库比对了
        Customer customer = (Customer)request.getSession().getAttribute("customer");
        if (customer != null){
           return true;
        }else {
            customerLoginInterceptor.checkCustomerCookie(request);
            //无论有没有都放行
            return true;
        }


    }
}
