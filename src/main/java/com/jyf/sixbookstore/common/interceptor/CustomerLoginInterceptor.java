package com.jyf.sixbookstore.common.interceptor;

import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.common.constant.Constant;
import com.jyf.sixbookstore.common.utils.EncrypUtils;
import com.jyf.sixbookstore.common.utils.JsonUtils;
import com.jyf.sixbookstore.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;

/**
 * @author Mr.贾
 * @time 2019/6/23 17:09
 * @describe: 登录拦截
 */
//自动注入的bean所在的类一定要是IOC容器的组件，否则自动注入的bean将为null
@Component
public class CustomerLoginInterceptor implements HandlerInterceptor {

    private Logger logger = LoggerFactory.getLogger(CustomerLoginInterceptor.class);

    @Autowired
    private CustomerService customerService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Customer customer = (Customer)request.getSession().getAttribute("customer");
        //如果session中有customer，优先放行
        if (customer != null){
            return true;
        }else {
            //判断cookie中是否有customer信息
            boolean flag = checkCustomerCookie(request);
            if (flag){
               return true;
            }
            //如果cookie中没有，看你是ajax请求还是url请求，做对应的处理


            //若为ajax请求的话，直接跳转页面无效，必须有ajax的处理函数来处理结果
            //PS：ajax的请求头中 X-Requested-With 的值为 XMLHttpRequest
            String header = request.getHeader("x-requested-with");
            if ("XMLHttpRequest".equals(header)){
                logger.info("拦截了路径："+request.getRequestURL());
                response.getWriter().write("redirect");
            }else {
                logger.info("拦截了路径："+request.getRequestURL());
                response.sendRedirect("/frontSkip/toLogin");
            }
            return false;
        }
    }

     //如果cookie中有customer信息，就放到session中
     boolean checkCustomerCookie(HttpServletRequest request) throws UnsupportedEncodingException {
        Cookie[] cookies = request.getCookies();
         if (cookies != null) {
            for (Cookie cookie : cookies) {
                String cookieName = cookie.getName();
                //如果有，解密后拿cookie中的值和数据库中的值进行比较
                if (Constant.cookieCustomerKey.getName().equals(cookieName)){
                    String cookieValue = cookie.getValue();
                    String decry = EncrypUtils.Base64Util.decry(cookieValue);
                    Customer customer1 = JsonUtils.stringToObject(decry, Customer.class);
                    Customer customer2 = customerService.checkLogin(customer1.getPhoneNumber(), customer1.getPassword());
                    if (customer2 != null && customer2.getPassword().equals(customer1.getPassword())){
                        //放入到session中，放行
                        request.getSession().setAttribute("customer",customer2);
                        //自动登录时，更新用户的在线状态
                        Customer onlineCustomer = new Customer();
                        onlineCustomer.setId(customer2.getId());
                        onlineCustomer.setOnlineStatus(String.valueOf(Constant.ONLINESTATUS.getCode()));
                        customerService.updateById(onlineCustomer);
                        return true;
                    }
                }
            }
         }
        return false;
    }
}
