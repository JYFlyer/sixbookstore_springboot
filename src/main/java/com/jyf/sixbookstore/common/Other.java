package com.jyf.sixbookstore.common;

import com.jyf.sixbookstore.common.utils.SpringContextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.web.ServerProperties;

/**
 * @author Mr.贾
 * @time 2019/9/17 22:41
 * @describe: 提取一些公共的方法或属性
 */
public class Other {

    private static Logger logger = LoggerFactory.getLogger(Other.class);

    public static void printProjectInfo() {
        ServerProperties serverProperties = SpringContextUtils.getApplicationContext().getBean(ServerProperties.class);
        DataSourceProperties dataSourceProperties = SpringContextUtils.getApplicationContext().getBean(DataSourceProperties.class);
        logger.info("数据库：{}", dataSourceProperties.getUrl());
        logger.info("成功启动：http://localhost:{}", serverProperties.getPort());
    }
}
