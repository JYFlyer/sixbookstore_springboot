package com.jyf.sixbookstore.common.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.common.constant.Constant;
import com.jyf.sixbookstore.common.websocket.WebSocketServer;
import com.jyf.sixbookstore.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author Mr.贾
 * @time 2019/12/7 11:42
 * @describe: 统计在线人数的定时任务
 *            登录的为在线，没登录的为离线
 */
@Component
public class OnlineStatisticsTask {

    @Autowired
    private CustomerService customerService;

    //每三秒执行一次，异步执行
    @Async
    @Scheduled(cron = "0/3 * * * * *")
    public void statistics(){
        int totalCustomer = customerService.count(null);
        int onlineCustomer = customerService.count(new QueryWrapper<Customer>().eq("online_status", Constant.ONLINESTATUS.getCode()));

        //利用webSocket推送数据
        WebSocketServer.sendInfo(totalCustomer+ "," + onlineCustomer);
    }

}
