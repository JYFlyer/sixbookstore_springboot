package com.jyf.sixbookstore.common.model;

import com.jyf.sixbookstore.common.utils.TimeUtils;
import com.jyf.sixbookstore.common.utils.UUIDUtils;

/**
 * @author Mr.贾
 * @time 2019/8/10 11:55
 * @describe:
 */
public class MakeOrderNo {

    /**
     * 生成订单号
     * @return
     */
    public static String getOrderNo(){
        String date =TimeUtils.getFormatNowDate().replace("-","");
        String uuidByNumber = UUIDUtils.getUUIDByNumber(8);
        return date + uuidByNumber;
    }

    /**
     * 生成redis中订单编号的key
     * @param orderNo
     * @return
     */
    public static String getRedisOrderNo(String orderNo){
        return "order_"+orderNo;
    }

    /**
     * 生成customer在redis中所有未支付订单的key
     * @param customerId
     * @return
     */
    public static String getCustomerUnPaidOrderNoKey(Long customerId){
        return "customer_" +customerId +"_unpaidOrderNo";
    }
}
