package com.jyf.sixbookstore.common.anotation;

import java.lang.annotation.*;

/**
 * @author Mr.贾
 * @time 2019/8/24 9:40
 * @describe: JDK中的自定义注解，这里配合aop的@Pointcut(@anotation(xxx注解))使用
 *       1、用@interface为注解的类型
 *       2、元注解：1)@Target :指明自定义注解可修饰什么类型
 *                 2)@Retention ：指明该注解的生存周期，RUNTIME最长，运行时期依然存在
 *                 3)@Inherited ：表示该注解可以被继承
 *                 4)@Documented ：表示该注解可被生成javadoc文档
 *       3、属性：成员变量，即在注解的括号里可以填写的内容
 *               和普通的成员变量一样定义，不过变量名都要加上(),default关键字可用于指定默认值
 *               如果变量名定义成value()，则注解括号里可以不用指定变量名，否则需要指定
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.FIELD})
public @interface WebLogger {

    String content() default "";
}
