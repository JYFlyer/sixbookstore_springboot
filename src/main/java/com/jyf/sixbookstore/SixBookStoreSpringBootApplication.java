package com.jyf.sixbookstore;

import com.jyf.sixbookstore.common.Other;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling //开启定时任务
@EnableAsync      //开启异步任务
@SpringBootApplication
@ServletComponentScan //支持filter、listener、servlet注解
public class SixBookStoreSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SixBookStoreSpringBootApplication.class, args);
        System.out.println("--------------------------------------------------------------------------------------"
                + "6号书店项目启动了"
                         + "--------------------------------------------------------------------------------------");
        Other.printProjectInfo();
    }

}
