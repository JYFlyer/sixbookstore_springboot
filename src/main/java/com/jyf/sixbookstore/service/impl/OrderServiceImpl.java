package com.jyf.sixbookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jyf.sixbookstore.bean.Order;
import com.jyf.sixbookstore.mapper.OrderMapper;
import com.jyf.sixbookstore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Mr.贾
 * @time 2019/6/22 13:50
 * @describe:
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper,Order> implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public List<Order> getOrdersByOrderNo(String orderNo) {
        QueryWrapper<Order> queryMapper = new QueryWrapper<>();
        queryMapper.eq("order_no",orderNo);
        return orderMapper.selectList(queryMapper);
    }

    @Override
    public BigDecimal getTotalMoneyByOrderNo(String orderNo) {
        return orderMapper.getTotalMoneyByOrderNo(orderNo);
    }

    @Override
    public List<Integer> getBookNumberByOrderNoAndBookNos(String orderNo, List<String> bookNos) {
        return orderMapper.getBookNumberByOrderNoAndBookNos(orderNo,bookNos);
    }

    /*@Override
    public void updateBookStockAndStatusByOrderNoAndBookNos(String orderNo, List<Map<String, Object>> bookNosAndNumbers) {
        orderMapper.updateBookStockAndStatusByOrderNoAndBookNos(orderNo,bookNosAndNumbers);
    }*/

    @Override
    public void batchInsertOrder(List<Order> orders) {
        orderMapper.batchInsertOrders(orders);
    }

    @Override
    public Page<Order> orderPage(Page<Order> page, Map<String,Object> paramMap) {
        return page.setRecords(orderMapper.selectOrderPage(page,paramMap));
    }
}
