package com.jyf.sixbookstore.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jyf.sixbookstore.bean.ChatInfo;
import com.jyf.sixbookstore.mapper.WebChatMapper;
import com.jyf.sixbookstore.service.WebChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mr.贾
 * @time 2020/1/11 22:43
 * @describe:
 */
@Service
public class WebChatServiceImpl extends ServiceImpl<WebChatMapper,ChatInfo> implements WebChatService {


    @Autowired
    private WebChatMapper webChatMapper;

    @Override
    public List<ChatInfo> selectListByFromAndToId(Long fromCustomerId, Long toCustomerId) {
        return webChatMapper.selectListByFromAndToId(fromCustomerId,toCustomerId);
    }
}
