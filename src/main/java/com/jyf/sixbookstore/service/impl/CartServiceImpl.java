package com.jyf.sixbookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jyf.sixbookstore.bean.Cart;
import com.jyf.sixbookstore.mapper.CartMapper;
import com.jyf.sixbookstore.mapper.OrderMapper;
import com.jyf.sixbookstore.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CartServiceImpl implements CartService {
	
	@Autowired
    CartMapper cartMapper;

	@Autowired
    OrderMapper orderMapper;

    @Override
    public List<Integer> selectBookKindNumberByCustomerId(Long customerId) {
        return cartMapper.selectBookKindNumberByCustomerId(customerId);
    }

    @Override
    public Integer selectCartBookNumberByCustomerId(Long customerId) {
        return cartMapper.selectCartBookNumberByCustomerId(customerId);
    }


    @Override
    public List<Cart> getCartBookByBookNo(Long customerId, List<String> bookNos) {
        return cartMapper.getCartBookByBookNo(customerId, bookNos);
    }

    @Override
    public void cleanCart(Long customerId) {
        cartMapper.cleanCart(customerId);
    }

    @Override
    public void deleteBookByChecked(Long customerId, List<String> list) {
        cartMapper.deleteBookByChecked(customerId, list);
    }

    @Override
    public void deleteByCustomerIdAndBookNo(Long customerId, String bookNo) {
        UpdateWrapper<Cart> updateWrapper = new UpdateWrapper<>();
        //使用Map封装两个条件
        Map<String,Object> map = new HashMap<>();
        map.put("customer_id",customerId);
        map.put("book_no",bookNo);
        updateWrapper.allEq(map);
        cartMapper.delete(updateWrapper);
    }

    @Override
    public List<Cart> getCartBookByCustomerId(Long customerId) {
        return cartMapper.selectCartList(customerId);
    }

    /*@Override
    public int batchInsertOrder(List<Order> orders) {
        return cartMapper.batchInsertOrder(orders);
    }*/

    @Override
    public void updateBookNumberByCustomerIdAndBookNo(Long customerId, List<Map<String, Object>> bookNosAndNumbers) {
         cartMapper.updateBookNumberByCustomerIdAndBookNo(customerId,bookNosAndNumbers);
    }


}
