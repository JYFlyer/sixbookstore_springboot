package com.jyf.sixbookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jyf.sixbookstore.bean.Book;
import com.jyf.sixbookstore.bean.Cart;
import com.jyf.sixbookstore.bean.GuessLike;
import com.jyf.sixbookstore.mapper.BookMapper;
import com.jyf.sixbookstore.mapper.CartMapper;
import com.jyf.sixbookstore.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class BookServiceImpl implements BookService {
	
	@Autowired
    BookMapper bookMapper;

	@Autowired
	CartMapper cartMapper;




    @Override
    public void addToCart(Cart cart, Integer bookNumber, Long customerId, String bookNo) {
        List<String> bookNos=bookMapper.getCartBookNosByCustomerId(customerId);
        //如果购物车不等于null并且存在该书，就执行修改
        if(bookNos!=null&&bookNos.contains(bookNo)) {
            //修改cart的booknumber
            cartMapper.updateCartBookNumber(bookNumber, customerId, bookNo);
        }else {
			//否则就执行添加
			cartMapper.insert(cart);
		}

    }

    @Override
	public Book findBookByBookNo(String bookNo) {
		return bookMapper.selectOne(new QueryWrapper<Book>().eq("book_no",bookNo));
		
	}

	@Override
	public List<Book> getShowBookByBookKind(Integer bookKind) {
		QueryWrapper<Book> queryWrapper = new QueryWrapper<Book>().eq("book_kind", bookKind);
		List<Book> booksByBookKind = bookMapper.selectList(queryWrapper);
		return booksByBookKind;
	}


	@Override
	public List<Book> getLeftAndRightBook(List<Long> lrBookIds) {
		return bookMapper.getLeftAndRightBook(lrBookIds);
	}

    @Override
	public List<Book> getNewBookList(Integer bookKind){
		return bookMapper.getNewBookList(bookKind);
	}

	@Override
	public List<Book> getTabBookByBookKind(Integer bookKind) {
    	QueryWrapper<Book> queryWrapper = new QueryWrapper<>();
    	queryWrapper.eq("book_kind",bookKind);
		List<Book> TabBooksByBookKind = bookMapper.selectList(queryWrapper);
		return TabBooksByBookKind;
	}

    @Override
    public void updateBookStockByBookNos(List<Map<String, Object>> bookNosAndNumbers) {
        bookMapper.updateBookStockByBookNos(bookNosAndNumbers);
    }

    @Override
    public List<Long> getAllBookIds() {
        return bookMapper.getAllBookIds();
    }

    @Override
    public List<Book> getBooksByBookIds(List<Long> fourBookIds) {
        //根据id 的批量查询
        return bookMapper.selectBatchIds(fourBookIds);
    }

    @Override
    public Book findBookByBookId(Long randomId) {
        return bookMapper.selectById(randomId);
    }

    @Override
    public List<Integer> getBookStocksByBookNos(List<String> bookNos) {
        return bookMapper.getBookStocksByBookNos(bookNos);
    }


}
