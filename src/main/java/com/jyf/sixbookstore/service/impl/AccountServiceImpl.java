package com.jyf.sixbookstore.service.impl;

import com.jyf.sixbookstore.bean.Account;
import com.jyf.sixbookstore.common.constant.Constant;
import com.jyf.sixbookstore.mapper.AccountMapper;
import com.jyf.sixbookstore.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
    AccountMapper accountMapper;


    @Override
    public void updateCountMoney(Long customerId, BigDecimal totalMoney, Integer payMethod) {
        if(Constant.weixin.getCode() == payMethod) {
            accountMapper.updateWeiXinByCustomerId(customerId, totalMoney);
        }else {
            accountMapper.updateZhiFuBaoByCustomerId(customerId, totalMoney);
        }
    }

    @Override
    public BigDecimal getAccountMoney(Long customerId, Integer payMethod) {

        //0 就是weixin 1就是zhifubao
        if(Constant.weixin.getCode() == payMethod) {
            return accountMapper.getWeiXinByCustomerId(customerId);
        }else {
            return accountMapper.getZhiFuBaoByCustomerId(customerId);
        }
    }

    @Override
    public int insertAccount(Account account) {
        return accountMapper.insert(account);
    }

}
