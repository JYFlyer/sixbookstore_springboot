package com.jyf.sixbookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.bean.GuessLike;
import com.jyf.sixbookstore.common.anotation.WebLogger;
import com.jyf.sixbookstore.mapper.CustomerMapper;
import com.jyf.sixbookstore.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CustomerServiceImpl extends ServiceImpl<CustomerMapper,Customer> implements CustomerService {
	
	@Autowired
    CustomerMapper customerMapper;


    @Override
    public void updatePassword(String phoneNumber, String password) {
        customerMapper.updatePasswordByPhoneNumber(phoneNumber,password);
    }

    //注册customer
	public void zhuCeCustomer(Customer customer) {
		customerMapper.insert(customer);
	}
	
	public Customer checkPhoneNumber(String phoneNumber) {
		return customerMapper.selectOne(new QueryWrapper<Customer>().eq("phone_number",phoneNumber));
	}

	public Customer checkLogin(String phoneNumber,String password) {
		QueryWrapper<Customer> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("phone_number",phoneNumber).eq("password",password);
		return customerMapper.selectOne(queryWrapper);
		
	}

	@Override
	public Customer checkIdNumber(String idNumber) {
		QueryWrapper<Customer> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("id_number",idNumber);
		return customerMapper.selectOne(queryWrapper);
	}

	@Override
	public Customer checkByPhoneNumberAndIdNumber(String phoneNumber, String idNumber) {
		QueryWrapper<Customer> queryWrapper = new QueryWrapper<>();
		Map<String,Object> map = new HashMap<>();
		map.put("phone_number",phoneNumber);
		map.put("id_number",idNumber);
		queryWrapper.allEq(map);
		return customerMapper.selectOne(queryWrapper);
	}

    @Override
    public void perfectCustomerInfo(Customer customer) {
        customerMapper.updateById(customer);
    }

    @Override
    public Customer getCustomerById(Long customerId) {
        return customerMapper.selectById(customerId);
    }

    @Override
    public Customer updateHeadImg(Long customerId, String dbFilePath) {
        customerMapper.updateHeadImg(customerId,dbFilePath);
        return customerMapper.selectById(customerId);
    }


}
