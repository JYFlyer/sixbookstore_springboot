package com.jyf.sixbookstore.service.impl;

import com.jyf.sixbookstore.bean.Book;
import com.jyf.sixbookstore.bean.GuessLike;
import com.jyf.sixbookstore.mapper.GuessLikeMapper;
import com.jyf.sixbookstore.service.GuessLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author Mr.贾
 * @time 2019/6/29 10:51
 * @describe:
 */
@Service
public class GuessLikeServiceImpl implements GuessLikeService {

    @Autowired
    private GuessLikeMapper guessLikeMapper;


    @Override
    public int createRecord(GuessLike guessLike) {
        return guessLikeMapper.createRecord(guessLike);
    }

    @Override
    public void updateRecordTimes(Long customerId, String bookNo) {
        guessLikeMapper.updateRecordTimes(customerId,bookNo);
    }

    @Override
    public GuessLike getGuessLikeRecordByCustomerIdAndBookNo(Long customerId, String bookNo) {
        return guessLikeMapper.selectGKRecordByCIdAndBNo(customerId,bookNo);
    }

    @Override
    public List<Book> getGuessLikeBook(Long customerId, Date divisionTime){
        return guessLikeMapper.getGuessLikeBook(customerId,divisionTime);
    }
}
