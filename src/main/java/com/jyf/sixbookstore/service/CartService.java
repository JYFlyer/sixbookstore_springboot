package com.jyf.sixbookstore.service;

import com.jyf.sixbookstore.bean.Cart;
import com.jyf.sixbookstore.bean.Order;

import java.util.List;
import java.util.Map;

/**
 * @author Mr.贾
 * @time 2019/6/7 11:40
 * @describe:
 */
public interface CartService {

    List<Integer> selectBookKindNumberByCustomerId(Long customerId);

    Integer selectCartBookNumberByCustomerId(Long customerId);

    //根据customerId和bookNos
    List<Cart> getCartBookByBookNo(Long customerId, List<String> bookNos);

    void cleanCart(Long customerId);

    void deleteBookByChecked(Long customerId,List<String> list);

    void deleteByCustomerIdAndBookNo(Long customerId,String bookNo);

    List<Cart> getCartBookByCustomerId(Long customerId);

    //int batchInsertOrder(List<Order> orders);

    void updateBookNumberByCustomerIdAndBookNo(Long customerId, List<Map<String,Object>> bookNosAndNumbers);
}
