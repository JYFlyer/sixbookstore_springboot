package com.jyf.sixbookstore.service;

import com.jyf.sixbookstore.bean.Account;

import java.math.BigDecimal;

/**
 * @author Mr.贾
 * @time 2019/6/7 11:39
 * @describe:
 */
public interface AccountService {

     void updateCountMoney(Long customerId,BigDecimal totalMoney,Integer payMethod);

     BigDecimal getAccountMoney(Long customerId, Integer payMethod);

     //注册时添加账户
     int insertAccount(Account account);

}
