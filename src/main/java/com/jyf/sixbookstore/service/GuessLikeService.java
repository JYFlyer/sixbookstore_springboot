package com.jyf.sixbookstore.service;

import com.jyf.sixbookstore.bean.Book;
import com.jyf.sixbookstore.bean.GuessLike;

import java.util.Date;
import java.util.List;

/**
 * @author Mr.贾
 * @time 2019/6/29 10:50
 * @describe:
 */
public interface GuessLikeService {

    //添加一条猜你喜欢的记录
    int createRecord(GuessLike guessLike);
    //次数加一
    void updateRecordTimes(Long customerId, String bookNo);

    //根据customerId和bookNo查询猜你喜欢记录
    GuessLike getGuessLikeRecordByCustomerIdAndBookNo(Long customerId, String bookNo);

    //获取猜你喜欢列表
    List<Book> getGuessLikeBook(Long customerId, Date divisionTime);
}
