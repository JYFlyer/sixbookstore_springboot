package com.jyf.sixbookstore.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jyf.sixbookstore.bean.Order;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Mr.贾
 * @time 2019/6/22 13:44
 * @describe:
 */
public interface OrderService extends IService<Order> {



    //根据orderNo得到订单信息
    List<Order> getOrdersByOrderNo(String orderNo);
    //根据orderNo得到总价格
    BigDecimal getTotalMoneyByOrderNo(String orderNo);

    List<Integer> getBookNumberByOrderNoAndBookNos(String orderNo, List<String> bookNos);
    //修改order库存
    //void updateBookStockAndStatusByOrderNoAndBookNos(String orderNo, List<Map<String, Object>> bookNosAndNumbers);

    //同步redis的订单到mysql
    void batchInsertOrder(List<Order> orders);

    Page<Order> orderPage(Page<Order> page, Map<String,Object> paramMap);
}
