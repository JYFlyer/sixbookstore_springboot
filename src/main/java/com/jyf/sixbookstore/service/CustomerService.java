package com.jyf.sixbookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jyf.sixbookstore.bean.Customer;
import com.jyf.sixbookstore.bean.GuessLike;

/**
 * @author Mr.贾
 * @time 2019/6/7 11:39
 * @describe:
 */
public interface CustomerService extends IService<Customer> {

    void updatePassword(String phoneNumber,String password);

    void zhuCeCustomer(Customer customer);

    Customer checkPhoneNumber(String phoneNumber);

    Customer checkLogin(String phoneNumber,String password);

    Customer checkIdNumber(String idNumber);

    Customer checkByPhoneNumberAndIdNumber(String phoneNumber, String idNumber);

    void perfectCustomerInfo(Customer customer);

    Customer getCustomerById(Long customerId);

    //修改用户头像
    Customer updateHeadImg(Long customerId, String dbFilePath);
}
