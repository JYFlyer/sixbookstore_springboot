package com.jyf.sixbookstore.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jyf.sixbookstore.bean.ChatInfo;

import java.util.List;

/**
 * @author Mr.贾
 * @time 2020/1/11 22:42
 * @describe:
 */
public interface WebChatService extends IService<ChatInfo> {

    /**
     * 查询两个人之间的聊天记录
     * @param fromCustomerId
     * @param toCustomerId
     * @return
     */
    List<ChatInfo> selectListByFromAndToId(Long fromCustomerId, Long toCustomerId);
}
