package com.jyf.sixbookstore.service;

import com.jyf.sixbookstore.bean.Book;
import com.jyf.sixbookstore.bean.Cart;
import com.jyf.sixbookstore.bean.GuessLike;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Mr.贾
 * @time 2019/6/7 11:40
 * @describe:
 */
public interface BookService {


     void addToCart(Cart cart,Integer bookNumber,Long customerId ,String bookBianHao);


     Book findBookByBookNo(String bookNo);

     List<Book> getShowBookByBookKind(Integer bookKind);

     List<Book> getLeftAndRightBook(List<Long> lrBookIds);

     List<Book> getNewBookList(Integer bookKind);

     List<Book> getTabBookByBookKind(Integer bookKind);

     void updateBookStockByBookNos(List<Map<String,Object>>bookNosAndNumbers);
     //得到所有的bookId
     List<Long> getAllBookIds();
     //随机取出四本书
     List<Book> getBooksByBookIds(List<Long> fourBookIds);

     Book findBookByBookId(Long randomId);

     //根据书的编号查询出书的库存
     List<Integer> getBookStocksByBookNos(List<String> bookNos);
}
