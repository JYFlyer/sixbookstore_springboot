# 六号书店springBoot版

#### 介绍
六号书店的springBoot版，单模块maven版

#### 软件架构
1、后台
* 整体解耦：springboot
* 模板引擎：thymeleaf
* 日志：self4j+logback
* 数据层：mybatis和mybatisPlus
* 数据库：mysql
* 缓存：redis
* 数据源：druid
* 项目管理：maven  

2、前端页面：

html+css+js+jQuery1.8+layer+bootstrap3+ajax纯手写搭建
      
      

#### 使用说明

1.  创建sixbookstore数据库，导入sixbootstore.sql文件，application.yml中配置mysql连接信息
2.  application.properties中配置redis服务
3.  SixBookStoreSpringBootApplication的run()方法启动项目

说明：关于上传资源的访问，我是在本地额外启动了一个tomcat（用于做文件资源虚拟路径的映射和文件访问的跨域问题），
大家可以改变一下文件的存储路径和访问路径。






#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
